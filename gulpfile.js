const dotenv = require("dotenv").config();
const settings = require("./cfg/gulp");

const gulp = require("gulp");
const print = require("gulp-print");
const bro = require("gulp-bro");
const watch = require("gulp-watch");
const csso = require("gulp-csso");
const cssimport = require("gulp-cssimport");
const nodemon = require("gulp-nodemon");
const browsersync = require("browser-sync");
const sass = require("gulp-sass");
const plumber = require("gulp-plumber");
const uglify = require("gulp-uglify-es").default;
const rename = require("gulp-rename");
const envify = require("envify");

gulp.task("nodemon", () => nodemon(settings.nodemonSettings));
gulp.task("browsersync", () => browsersync.init(settings.browserSyncSettings));
gulp.task("build", ["js", "css"], () => {
    return gulp.src(["public/js/app.js", "public/js/vendor.js"])
        .pipe(uglify())
        .pipe(rename({ suffix: ".min" }))
        .pipe(gulp.dest("public/js"))
        .pipe(print(filepath => `minified: ${filepath}`));
});

gulp.task("default", ["build"], () => {
    gulp.start(["nodemon", "browsersync", "watch"])
});

gulp.task("js", () => {
    return gulp.src("assets/js/*.js")
        .pipe(plumber())
        .pipe(bro({
            transform: [
                "envify",
                "vueify"
            ]
        }))
        .pipe(gulp.dest("public/js"))
        .pipe(print(filepath => `built: ${filepath}`));
});

gulp.task("css", () => {
    return gulp.src("assets/sass/*.sass")
        .pipe(plumber())
        .pipe(sass())
        .pipe(cssimport())
        .pipe(csso())
        .pipe(gulp.dest("public/css"))
        .pipe(print(filepath => `built: ${filepath}`));
});

gulp.task("watch", () => {
    watch(["assets/js/*.js", "assets/js/views/*", "assets/js/components/*", "assets/js/controllers/*"]).on("change", () => {
        gulp.src("assets/js/app.js")
            .pipe(plumber())
            .pipe(bro(settings.broSettings))
            .pipe(gulp.dest("public/js"))
            .pipe(print(filepath => `built: ${filepath}`));
    });

    watch("assets/sass/*.sass")
        .pipe(plumber())
        .pipe(sass())
        .pipe(cssimport())
        .pipe(csso())
        .pipe(gulp.dest("public/css"))
        .pipe(print(filepath => `built: ${filepath}`));
});

module.exports = gulp;