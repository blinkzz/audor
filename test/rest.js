let chai = require("chai");
let chaiHttp = require("chai-http");
let assert = chai.assert;
let host = "http://localhost:8080";

chai.use(chaiHttp);

describe("rest", function () {
    let cookies = "";

    describe("authorization", function () {
        before(function (done) {
            chai.request(host)
                .post("/login")
                .send({login: "admin", password: "admin"})
                .end(function (error, response) {
                    cookies = response.headers["set-cookie"];
                    done();
                });
        });

        describe("check authorization", function () {
            it("should be authorized", function (done) {
                chai.request(host)
                    .get("/rest/users/this")
                    .set("Cookie", cookies)
                    .end((error, response) => {
                        assert.exists(response.body._id);
                        done();
                    })
            })
        })
    });

    describe("users", function () {
        let thisUser = "";

        describe("GET /this", function () {
            let err, res = null;

            before(function (done) {
                chai.request(host)
                    .get("/rest/users/this")
                    .set("Cookie", cookies)
                    .end((error, response) => {
                        err = error;
                        res = response;
                        thisUser = res.body;
                        done();
                    })
            });

            it("has _id", () => assert.exists(res.body._id));
            it("has login", () => assert.exists(res.body.login));
        });

        describe("GET /:id", function () {
            let err, res = null;

            before(function (done) {
                chai.request(host)
                    .get(`/rest/users/${thisUser._id}`)
                    .set("Cookie", cookies)
                    .end((error, response) => {
                        err = error;
                        res = response;
                        done();
                    })
            });

            it("has _id", () => assert.exists(res.body._id));
            it("has login", () => assert.exists(res.body.login));

            it("_id matching", () => assert.equal(res.body._id, thisUser._id));
            it("name matching", () => assert.equal(res.body.name, thisUser.name));
        })
    });
});

describe("rest (without auth)", function () {
    describe("boards", function () {
        describe("GET /boards", function () {
            let err, res = null;

            before(function (done) {
                chai.request(host)
                    .get("/rest/boards")
                    .end((error, response) => {
                        err = error;
                        res = response;
                        done();
                    });
            });

            it("should return error", () => {
                assert.exists(res.body.error);
                assert.exists(res.body.errorCode);
                assert.equal(res.body.error, "NOT_AUTHORIZED");
                assert.equal(res.body.errorCode, 0);
            });
        });
    });
});

