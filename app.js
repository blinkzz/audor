const dotenv = require('dotenv').config();
const express = require("express");
const path = require("path");
const favicon = require("serve-favicon");
const logger = require("morgan");
const cookieParser = require("cookie-parser");
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);
const bodyParser = require("body-parser");
const settings = require("./cfg/main.js");
const app = express();
const http = require('http');
const fs = require('fs');
const compression = require("compression");
const httpsRedirect = require("express-http-to-https").redirectToHTTPS;

logger.token("login", (req, res) => {
   if (req.session.isAuthorized) return req.session.userData.login;
   return "guest";
});

/* Database Connection */
global.mongoose = require("mongoose");
global.ObjectId = mongoose.Schema.ObjectId;

mongoose.connection.on("reconnecting", () => console.log("Trying to reconnect MongoDB connection..."));
mongoose.connection.on("connecting", () => console.log("Trying to establish MongoDB connection..."));
mongoose.connection.on("connected", () => console.log("MongoDB connection established"));
mongoose.connection.on("reconnected", () => console.log("MongoDB reconnected"));
mongoose.connection.on("disconnected", () => console.log("Unable to connect to a MongoDB server"));

mongoose.connect(`mongodb://${process.env.MONGO_HOST}/${process.env.MONGO_DB}`, {
    useMongoClient: true,
    autoReconnect: true,
    reconnectTries: Number.MAX_VALUE,
    reconnectInterval: 200
});

mongoose.Promise = global.Promise;

global.to = function (promise) {
    return promise.then(data => {
        return [null, data];
    }).catch(err => [err]);
};

global.__root = __dirname;

const roles = require("./models/role.js");

app.use((req, res, next) => {
    if (req.hostname.indexOf("www.") === 0) return res.redirect(301, req.protocol + '://' + req.host.slice(4) + req.url);
    next();
});

app.use(httpsRedirect([/localhost:(\d{4})/]));

app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");
app.set('view cache', true);

app.use(compression());

app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger(settings.loggerFormat));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(session({
    secret: 'totallynotasecret',
    store: new MongoStore({
        db: "session",
        mongooseConnection: mongoose.connection
    }),
    resave: false,
    saveUninitialized: true
}));

app.use(express.static(path.join(__dirname, "public")));
app.use("/content", express.static(path.join(__dirname, "content")));

app.use(require("./routes/init"));
app.use(require("./routes/session"));
app.use("/rest", require("./routes/rest"));
app.use("/settings", require("./routes/settings"));

app.use("*", (req, res) => {
    let avaliableRoutes = ["/login", "/register", "", "/"];
    if (avaliableRoutes.indexOf(req.baseUrl) >= 0) return res.render("app");
    if (!req.session.isAuthorized) return res.redirect("/");
    res.render("app");
});

const httpsOptions = {
    cert: fs.readFileSync("ssl/public.pem"),
    key: fs.readFileSync("ssl/private.pem"),
    maxSockets: 1000,
    spdy: {
        maxStreams: 1000
    }
};

const spdy = require("spdy");
http.createServer(app).listen(80);
http.createServer(app).listen(8080);
spdy.createServer(httpsOptions, app).listen(443);

if (process.platform === "win32") {
    const consoleReader = require("readline").createInterface({
        input: process.stdin,
        output: process.stdout
    });

    consoleReader.on("SIGINT", function () {
        process.emit("SIGINT");
    });
}

process.on("SIGINT", function () {
    process.exit();
});

module.exports = app;