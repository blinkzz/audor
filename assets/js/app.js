const Boards = require("./views/Boards.vue");
const Board = require("./views/Board.vue");
const User = require("./views/User.vue");
const Settings = require("./views/Settings.vue");
const Login = require("./views/Login.vue");
const Admin = require("./views/Admin.vue");
const Index = require("./views/Index.vue");
const Register = require("./views/Register.vue");
const Logout = require("./controllers/Logout.vue");

const router = new VueRouter({
    mode: 'history',
    routes: [
        {name: "Index", path: "/", component: Index},
        {name: "Admin", path: "/admin", component: Admin},
        {name: "Boards", path: "/boards", component: Boards},
        {name: "Board", path: "/boards/:id", component: Board},
        {name: "User", path: "/users/:id", component: User},
        {name: "Settings", path: "/settings", component: Settings},
        {name: "Login", path: "/login", component: Login},
        {name: "Register", path: "/register", component: Register},
        {name: "Logout", path: "/logout", component: Logout}
    ]
});

global.eventBus = new Vue();

global.vueInstance = new Vue({
    el: "#app",
    data: {
        currentBoard: {},
        loadComplete: false,
        boards: [],
        user: {},
        cardModalVisible: false,
        cardMoving: false,
        isMobilePhone: false
    },
    router,
    computed: {
        userRole() {
            if (!this.currentBoard._id) return {};

            for (let user of this.currentBoard.users) {
                if (this.user._id === user.user._id) {
                    return user.role;
                }
            }
            return {};
        },
        appStyle() {
            let appClasses = [];
            this.user.prefs && this.user.prefs.theme && this.user.prefs.theme === "dark" && appClasses.push("app-dark");
            this.currentBoard.background && appClasses.push("bg-none");

            if (this.$route.name === "Board") appClasses.push("lock-height");

            let excludedRoutes = ["Login", "Register"];
            excludedRoutes.includes(this.$route.name) && appClasses.push("bg-primary-gradient");

            return appClasses;
        },
        navbarClasses() {
            let navClasses = ["mb-3"];
            navClasses.push(this.currentBoard.background ? "navbar-transparent" : "bg-primary");
            return navClasses;
        },
        hasNavbar() {
            let excludedList = ["Login", "Register", "Index"];
            return !excludedList.includes(this.$route.name);
        }
    },
    methods: {
        removeBoard(board) {
            this.boardModalVisible = false;
            this.$http.delete(`/rest/boards/${board._id}`).then(() => {
                this.getBoards();
                this.$router.push("/boards");
            })
        },
        loadBoards() {
            this.$http.get("/rest/boards/").then(response => {
                this.boards = response.data;
                this.loadComplete = true;
            }).catch(error => {});
        }
    },
    created() {
        this.isMobilePhone = (typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf('IEMobile') !== -1);
        this.$http.get("/rest/users/this")
            .then(response => {
                if (response.data.error) return;
                this.user = response.data;
                this.loadBoards();
                if (this.$route.path === "/") this.$router.push("/boards");
            })
            .catch(error => {});
        eventBus.$on("board-remove", this.removeBoard);
    }
});

/* Disabled till rework
if('serviceWorker' in navigator) {
    navigator.serviceWorker.register('/sw.js', { scope: "/" })
        .then(function() {
            console.log('Service Worker Registered');
        });
};
*/

