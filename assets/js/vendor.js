if (process.env.NODE_ENV === "development") global.Vue = require("vue/dist/vue.js");
else global.Vue = require("vue/dist/vue.min.js");
global.Sortable = require("sortablejs");
global.VueRouter = require("vue-router");
global.moment = require("moment/min/moment-with-locales.min");
moment.locale('ru');

const Vuelidate = require("vuelidate");
const Bootstrap = require("bootstrap-vue/dist/bootstrap-vue.min");
const VueResource = require("vue-resource");

Vue.use(Vuelidate.default);
Vue.use(VueResource);
Vue.use(VueRouter);
Vue.use(Bootstrap);
