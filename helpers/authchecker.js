const errors = require("./errors");

module.exports = {
    hasAuth(req, res, next) {
        if (!req.session.userData || !req.session.userData._id) {
            let error = errors.NOT_AUTHORIZED;
            return sendError(res, error);
        }

        next();
    },
    hasAdminAuth(req, res, next) {
        if (!req.session.userData || !req.session.userData._id) {
            let error = errors.NOT_AUTHORIZED;
            return sendError(res, error);
        }

        if (!req.session.userData.admin !== true) {
            let error = errors.NOT_AN_ADMIN;
            return sendError(res, error);
        }

        next();
    }
};

function sendError(res, error) {
    return res.json({
        error: error.name,
        errorCode: error.code
    });
}