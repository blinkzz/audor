module.exports = {
    NOT_AUTHORIZED: {
        name: "NOT_AUTHORIZED",
        code: 0
    },
    NOT_AN_ADMIN: {
        name: "NOT_AN_ADMIN",
        code: 1
    }
};