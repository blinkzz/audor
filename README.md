# Deployment
Перед началом работы, стоит убедиться, что система не имеет конфликтующих пакетов (apache2 и т.д.), 
либо они запущенны на отдельных портах

1. Убедимся, что система обновлена и готова к работе

    `apt-get update && apt-get upgrade`
    
2. Установим необходимые утилиты

    `apt-get install nano git curl`
    
3. Установим свежий Node.js
    
    `curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -`  
    `apt-get install -y nodejs`  
    
4. Установим MongoDB

    `apt-get install mongodb`
    
5. Перейдем в директорию для проекта и клонируем туда его

    `cd /var/www`
    `git clone https://blinkzz@bitbucket.org/blinkzz/projectaudor.git`
    
6. Установим все необходимые зависимости

    `npm install`
    
7. Установим pm2 (менеджер процессов)

    `npm install -g pm2`
    
8. Добавим наше приложение в список процессов

    `pm2 start app.js`
    
9. Добавим pm2 в автозагрузку

    `pm2 startup systemd`