const express = require("express");
const router = express.Router();
const user = require("../models/user");
const multer = require("multer");
const upload = multer({ dest: "uploads/" });
const {check, validationResult} = require('express-validator/check');

router.post("/", upload.single("photo"), async (req, res) => {
    let userData = await user.findById(req.session.userData._id);
    
    userData.name = req.body.name || userData.name;
    userData.about = req.body.about || userData.about;
    userData.website = req.body.website || userData.website;
    userData.email = req.body.email || userData.email;
    userData.prefs = "prefs" in req.body ? req.body.prefs : userData.prefs;

    if (req.file) await userData.uploadPhoto(req.file);

    userData.save();
    req.session.userData = userData;
    res.json(userData);
});

router.post("/password", async (req, res) => {

    let [err, userData] = await to(user.findById(req.session.userData._id));

    if (userData.password !== user.getPasswordHash(req.body.old_password)) {
        res.render("settings", {
            errorMessage: res.locals.strings.old_password_mismatch
        });
        return;
    }

    userData.password = user.getPasswordHash(req.body.new_password)
    userData.save();

    res.render("settings", {
        successMessage: res.locals.strings.password_changed
    });
});

module.exports = router;