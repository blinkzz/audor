const express = require("express");
const router = express.Router();
const list = require("../../models/list");
const card = require("../../models/card");

router.post("/:id", async (req, res) => {
    let selectedList = await list.findOne({_id: req.params.id});
    selectedList.name = req.body.name || selectedList.name;
    selectedList.order = req.body.order;
    selectedList.save();
    res.json(selectedList);
});

router.delete("/:id", async (req, res) => {
    let removedCards = await card.remove({list: req.params.id});
    let removedList = await list.remove({_id: req.params.id});
    res.json(removedList);
});

module.exports = router;