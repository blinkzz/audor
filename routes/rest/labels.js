const express = require("express");
const router = express.Router();
const labelColors = require("../../models/label_colors");

router.get("/colors", (req, res) => {
    res.json(labelColors);
});

module.exports = router;