const express = require("express");
const router = express.Router();
const user = require("../../models/user");
const authChecker = require("../../helpers/authchecker");

router.get("/this", async (req, res) => {
   res.json(await user.findOne({_id: req.session.userData._id}));
});

router.get("/all", authChecker.hasAdminAuth, async (req, res) => {
    let offset = parseInt(req.query.offset) || 0;
    let usersData = await user.find().select(["-admin"]).skip(offset).limit(50);
    res.json(usersData);
});

router.get("/all/count", authChecker.hasAdminAuth, async (req, res) => {
    let usersCount = await user.count();
    res.json(usersCount);
});

router.get("/:id", async (req, res) => {
    let [err, userData] = await to(user.findOne({_id: req.params.id}));

    if (err) {
        res.json({});
        return;
    }

    res.json(userData);
});

router.post("/:id", async (req, res) => {
    if (req.params.id !== req.session.userData._id) res.json()
});

module.exports = router;