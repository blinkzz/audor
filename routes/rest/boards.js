const express = require("express");
const router = express.Router();
const multiparty = require("multiparty");
const card = require("../../models/card");
const user = require("../../models/user");
const board = require("../../models/board");
const list = require("../../models/list");
const role = require("../../models/role");

router.get("/", async (req, res) => {
    let avaliableBoards = await board.find({"users.user": req.session.userData._id}).populate("users.user");
    res.json(avaliableBoards);
});

router.get("/:id", async (req, res) => {
    let selectedBoard = await board.findOne({_id: req.params.id})
        .populate("users.user")
        .populate("users.role");

    res.json(selectedBoard);
});

router.get("/:id/lists", async (req, res) => {
    let lists = await list.find({board: req.params.id}).populate("cards");
    res.json(lists);
});

router.post("/:id/lists", async (req, res) => {
    res.json(await list.create({
        name: req.body.name,
        board: req.params.id,
        owner: req.session.userData._id,
        order: req.body.order
    }));
});

router.post("/:id/lists/order", (req, res) => {
    //TODO: Replace with something more clever
    res.send("ok");
    let lists = req.body;

    lists.forEach(async (item) => {
        let listData = await list.findOne({_id: item.id});
        listData.order = item.order;
        listData.save();
    });
});

router.post("/:id/cards/order", async (req, res) => {
    //TODO: Replace with something more clever
    res.send("ok");

    let lists = req.body;
    lists.forEach(function (listItem) {
        listItem.cards.forEach(async function (cardItem) {
            let cardEntry = await card.findOne({_id: cardItem.id});
            cardEntry.list = listItem.id;
            cardEntry.order = cardItem.order;
            cardEntry.save();
        })
    });
});

router.post("/:id/cards", async (req, res) => {
    let newCard = await card.create({
        name: req.body.name,
        board: req.params.id,
        owner: req.session.userData._id
    });

    res.json(newCard);
});

router.delete("/:id/cards", async (req, res) => {
    let data = await card.remove({_id: req.body.id});
    res.json(data);
});

router.delete("/:id/cards/all", async (req, res) => {
    let cards = await card.find({board: req.params.id});
    cards.forEach(function (card) {
        card.remove();
    });

    res.json({status: "Done"});
});

router.post("/", async (req, res) => {
    let freshBoard = await board.create({name: req.body.name});
    let ownerRole = await role.findOne({ name: "owner"});

    freshBoard.users.push({ user: req.session.userData._id, role: ownerRole._id});
    freshBoard.save();

    res.json(freshBoard);
});

router.delete("/:id", async (req, res) => {
    await board.remove({_id: req.params.id});
    res.json({ok: true});
});

router.post("/:id", async (req, res) => {
    let selectedBoard = await board.findOne({_id: req.params.id});

    selectedBoard.name = ("name" in req.body) ? req.body.name : selectedBoard.name;
    selectedBoard.prefs = ("prefs" in req.body) ? req.body.prefs : selectedBoard.prefs;
    selectedBoard.save();

    res.json(selectedBoard);
});

router.delete("/:id/users/:user", async(req, res) => {
    let selectedBoard = await board.findOne({
        _id: req.params.id
    });

    selectedBoard.users.id(req.params.user).remove();
    selectedBoard.save();

    res.json({"ok" : true});
});

router.post("/:id/users", async (req, res) => {
    let selectedUser = await user.findOne({ login: req.body.login });
    let selectedBoard = await board.findOne({ _id: req.params.id });
    let selectedRole = await role.findOne({ name: req.body.role });

    let isUnique = true;
    for (let boardUser of selectedBoard.users) {
        if (boardUser.user.equals(selectedUser._id)) {
            isUnique = false;
            break;
        }
    }

    if (!isUnique) {
        res.json( { error: "Пользователь уже добавлен" });
        return;
    }

    selectedBoard.users.push({ user: selectedUser._id, role: selectedRole._id });
    selectedBoard.save();
    selectedBoard = await board.populate(selectedBoard, "users.user");
    selectedBoard = await board.populate(selectedBoard, "users.role");

    res.json(selectedBoard.users);
});

router.delete("/:id/background", async (req, res) => {
    let boardData = await board.findById(req.params.id);
    await boardData.deleteBackground();
    boardData.save();
    res.json(boardData);
});

// TODO: Rework
router.post("/:id/background", async (req, res) => {
    let formData = new multiparty.Form();
    formData.parse(req, async (err, fields, files) => {
        let boardData = await board.findById(req.params.id);

        if (!err) if (files.background) await boardData.uploadBackground(files.background[0]);

        boardData.save();
        res.send(JSON.stringify(boardData));
    });
});

module.exports = router;
