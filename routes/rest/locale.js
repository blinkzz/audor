const express = require('express');
const fs = require('fs');
const router = express.Router();

router.get("/:locale", function (req, res) {
    let requestedLocale = "locale/" + req.params.locale + ".json";

    if (fs.existsSync(requestedLocale)) {
        let locale = fs.readFileSync(requestedLocale).toString();
        res.send(locale);
    }
    else res.json({ "error": "There is no such locale" });  
});

module.exports = router;