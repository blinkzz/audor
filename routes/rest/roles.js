const express = require("express");
const router = express.Router();
const role = require("../../models/role");

router.get("/", async (req, res) => {
    let roles = await role.find({ custom: false });
    res.json(roles);
});

router.post("/:id", async (req, res) => {
   let selectedRole = await role.findOne({ _id: req.params.id });
   selectedRole.name = "name" in req.body ? req.body.name : selectedRole.name;
   selectedRole.permissions = "permissions" in req.body ? req.body.permissions : selectedRole.permissions;
   selectedRole.save();
   res.json(selectedRole);
});

router.post("/", async (req, res) => {
   if (Array.isArray(req.body)) {
       req.body.forEach(async updatedRole => {
            let selectedRole = await role.findOne({ _id: updatedRole._id });
            selectedRole.permissions = updatedRole.permissions;
            selectedRole.save();
       })
   }
   res.json(res.body);
});


module.exports = router;