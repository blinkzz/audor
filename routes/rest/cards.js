const express = require("express");
const router = express.Router();
const card = require("../../models/card");
const multer = require("multer");
const upload = multer({ dest: "uploads/" });

router.get("/", async (req, res) =>{
    let offset = req.query.offset || 0;
    let cards = await card.find().skip(offset).limit(50);
    res.json(cards);
});

router.post("/", async (req, res) => {
    let createdCard = await card.create({
        name: req.body.name,
        list: req.body.list,
        board: req.body.board,
        owner: req.session.userData._id
    });

    res.json(createdCard);
});

router.post("/:id", upload.single("image"), async (req, res) => {
    let selectedCard = await card.findOne({_id: req.params.id});

    selectedCard.name = req.body.name || selectedCard.name;
    selectedCard.description = ("description" in req.body) ? req.body.description : selectedCard.description;
    selectedCard.labels = req.body.labels || selectedCard.labels;
    selectedCard.progressBars = ("progressBars" in req.body) ? req.body.progressBars : selectedCard.progressBars;

    if (req.file) await selectedCard.uploadImage(req.file);
    selectedCard.save();
    res.json(selectedCard);
});

router.delete("/:id/image", async (req, res) => {
    let selectedCard = await card.findOne({_id: req.params.id});
    await selectedCard.removeImage();
    res.send("ok!");
});

router.get("/:id", async (req, res) => {
    let [err, cardData] = await to(card.findOne({_id: req.params.id}));

    if (err) {
        res.send(JSON.stringify({"error": "There is no such card"}));
        return;
    }

    res.json(cardData);
});


router.delete("/:id", async (req, res) => {
    let selectedCard = await card.findOne({_id: req.params.id});
    await selectedCard.removeImage();
    selectedCard.remove();
    res.send("ok!");
});

module.exports = router;