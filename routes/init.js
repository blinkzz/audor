module.exports = function (req, res, next) {
    delete require.cache[require.resolve("../locale/ru_RU.json")];
    res.locals.strings = require("../locale/ru_RU.json");
    res.locals.title = res.locals.strings.title;
    res.locals.env = process.env;

    res.locals.preloads = [
        {link: "/fonts/fa-solid-900.woff2", as: "font"},
        {link: "/fonts/Roboto-Regular.ttf", as: "font", type: "font/ttf"},
        {link: "/fonts/Roboto-Light.ttf", as: "font", type: "font/ttf"},
        {link: "/fonts/Montserrat-Light.ttf", as: "font", type: "font/ttf"},
        {link: "/fonts/Montserrat-Regular.ttf", as: "font", type: "font/ttf"},
        {link: "/images/laptop-mockup.png", as: "image", type: "image/png"},
        {link: "/images/theme-light.png", as: "image", type: "image/png"},
        {link: "/images/theme-dark.png", as: "image", type: "image/png"},
        {link: "/images/slide-icon.svg", as: "image", type: "image/svg+xml"},
        {link: "/images/audor-logo-white.svg", as: "image", type: "image/svg+xml"},
    ];

    next()
};