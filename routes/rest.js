const express = require('express');
const router = express.Router();
const hasAuth = require("../helpers/authchecker").hasAuth;

router.use(hasAuth);
router.use("/lists", require("./rest/lists"));
router.use("/boards", require("./rest/boards"));
router.use("/locale", require("./rest/locale"));
router.use("/cards", require("./rest/cards"));
router.use("/users", require("./rest/users"));
router.use("/labels", require("./rest/labels"));
router.use("/roles", require("./rest/roles"));

router.get("/", (req, res) => {
    res.status(418);
    res.send("Method not specified");
});

module.exports = router;