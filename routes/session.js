const express = require("express");
const router = express.Router();
const user = require("../models/user");
const {check, validationResult} = require('express-validator/check');
const RateLimit = require("express-rate-limit");

const authorizationRateLimit = new RateLimit({
    windowMs: 60 * 1000,
    max: 10,
    delayMs: 0
});

const loginFormValidation = [
    check("login")
        .isLength({min: 1}).withMessage("Логин не может быть пустым")
        .isAlphanumeric().withMessage("Логин может содержать только латинские символы")
        .trim(),
    check("password")
        .isLength({min: 1}).withMessage("Пароль не может быть пустым")
        .trim()
];

const registerFormValidation = [
    check("login")
        .isLength({ min: 1 }).withMessage("Логин не может быть пустым")
        .isAlpha().withMessage("Логин может содержать только латинские символы")
        .trim(),
    check("password")
        .isLength({ min: 1 }).withMessage("Пароль не может быть пустым")
        .trim()
];

router.get(["/login", "/register"], (req, res) => {
    if (req.session.isAuthorized) return res.redirect("/boards");
    return res.render("app");
});

router.post("/login", authorizationRateLimit, loginFormValidation, async (req, res) => {
    let errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.json({error: "Произошла ошибка серверной валидации"})
    }

    let [err, data] = await to(user.findOne({
        login: req.body.login,
        password: user.getPasswordHash(req.body.password)
    }));

    if (data === null) {
        res.json({error: "Аккаунт с таким логином / паролем не найден"});
        return;
    }

    insertSessionData(req, data);
    res.json(data);
});

router.get("/logout", function (req, res) {
    delete req.session.userData;
    req.session.isAuthorized = false;
    res.redirect("/");
});

router.post("/register", authorizationRateLimit, registerFormValidation, async (req, res) => {
    let [err, newUser] = await to(user.create({
        login: req.body.login,
        password: user.getPasswordHash(req.body.password)
    }));

    if (err) {
        if (err.code === 11000) res.json({ error: "Пользователь с таким логином уже существует"} );
        return;
    }

    insertSessionData(req, newUser);
    res.json(newUser);
});

function insertSessionData(req, data) {
    req.session.isAuthorized = true;
    req.session.userData = data;
}

module.exports = router;
