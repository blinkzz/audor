module.exports = {
    babelSettings: {
        presets: [
            "es2015"
        ]
    },
    watchSettings: {
        ignoreInitial: false
    },
    nodemonSettings: {
        script: "app.js",
        ignore: [
            "gulpfile.js",
            "public/*",
            "assets/*"
        ],
        legacyWatch: true
    },
    browserSyncSettings: {
        proxy: "localhost:8080",
        files: [
            "public",
            "views"
        ],
        port: "7000",
    }
};