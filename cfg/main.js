module.exports = {
    loggerFormat: "[:date] (:remote-addr) (:login) :method :url :status :response-time ms",
};