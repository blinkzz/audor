(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
(function (global){
const Boards = require("./views/Boards.vue");
const Board = require("./views/Board.vue");
const User = require("./views/User.vue");
const Settings = require("./views/Settings.vue");
const Login = require("./views/Login.vue");
const Admin = require("./views/Admin.vue");
const Index = require("./views/Index.vue");
const Register = require("./views/Register.vue");
const Logout = require("./controllers/Logout.vue");

const router = new VueRouter({
    mode: 'history',
    routes: [
        {name: "Index", path: "/", component: Index},
        {name: "Admin", path: "/admin", component: Admin},
        {name: "Boards", path: "/boards", component: Boards},
        {name: "Board", path: "/boards/:id", component: Board},
        {name: "User", path: "/users/:id", component: User},
        {name: "Settings", path: "/settings", component: Settings},
        {name: "Login", path: "/login", component: Login},
        {name: "Register", path: "/register", component: Register},
        {name: "Logout", path: "/logout", component: Logout}
    ]
});

global.eventBus = new Vue();

global.vueInstance = new Vue({
    el: "#app",
    data: {
        currentBoard: {},
        loadComplete: false,
        boards: [],
        user: {},
        cardModalVisible: false,
        cardMoving: false,
        isMobilePhone: false
    },
    router,
    computed: {
        userRole() {
            if (!this.currentBoard._id) return {};

            for (let user of this.currentBoard.users) {
                if (this.user._id === user.user._id) {
                    return user.role;
                }
            }
            return {};
        },
        appStyle() {
            let appClasses = [];
            this.user.prefs && this.user.prefs.theme && this.user.prefs.theme === "dark" && appClasses.push("app-dark");
            this.currentBoard.background && appClasses.push("bg-none");

            if (this.$route.name === "Board") appClasses.push("lock-height");

            let excludedRoutes = ["Login", "Register"];
            excludedRoutes.includes(this.$route.name) && appClasses.push("bg-primary-gradient");

            return appClasses;
        },
        navbarClasses() {
            let navClasses = ["mb-3"];
            navClasses.push(this.currentBoard.background ? "navbar-transparent" : "bg-primary");
            return navClasses;
        },
        hasNavbar() {
            let excludedList = ["Login", "Register", "Index"];
            return !excludedList.includes(this.$route.name);
        }
    },
    methods: {
        removeBoard(board) {
            this.boardModalVisible = false;
            this.$http.delete(`/rest/boards/${board._id}`).then(() => {
                this.getBoards();
                this.$router.push("/boards");
            })
        },
        loadBoards() {
            this.$http.get("/rest/boards/").then(response => {
                this.boards = response.data;
                this.loadComplete = true;
            }).catch(error => {});
        }
    },
    created() {
        this.isMobilePhone = (typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf('IEMobile') !== -1);
        this.$http.get("/rest/users/this")
            .then(response => {
                if (response.data.error) return;
                this.user = response.data;
                this.loadBoards();
                if (this.$route.path === "/") this.$router.push("/boards");
            })
            .catch(error => {});
        eventBus.$on("board-remove", this.removeBoard);
    }
});

/* Disabled till rework
if('serviceWorker' in navigator) {
    navigator.serviceWorker.register('/sw.js', { scope: "/" })
        .then(function() {
            console.log('Service Worker Registered');
        });
};
*/


}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"./controllers/Logout.vue":13,"./views/Admin.vue":14,"./views/Board.vue":15,"./views/Boards.vue":16,"./views/Index.vue":17,"./views/Login.vue":18,"./views/Register.vue":19,"./views/Settings.vue":20,"./views/User.vue":21}],2:[function(require,module,exports){
;(function(){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getIterator2 = require("babel-runtime/core-js/get-iterator");

var _getIterator3 = _interopRequireDefault(_getIterator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    name: "AudorCard",
    components: {
        "CardProgressBar": require("./CardProgressBar.vue")
    },
    props: {
        card: Object
    },
    computed: {
        board: function board() {
            return this.$root.currentBoard;
        },
        hasIcons: function hasIcons() {
            var iconFields = ["description", "progressBars"];
            var _iteratorNormalCompletion = true;
            var _didIteratorError = false;
            var _iteratorError = undefined;

            try {
                for (var _iterator = (0, _getIterator3.default)(iconFields), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                    var field = _step.value;
                    if (this.card[field] && this.card[field].length > 0) return true;
                }
            } catch (err) {
                _didIteratorError = true;
                _iteratorError = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion && _iterator.return) {
                        _iterator.return();
                    }
                } finally {
                    if (_didIteratorError) {
                        throw _iteratorError;
                    }
                }
            }

            return false;
        },
        isOptionsAvaliable: function isOptionsAvaliable() {
            return this.$root.userRole.permissions.card.update || this.$root.userRole.permissions.card.remove;
        }
    },
    methods: {
        cardModalToggle: function cardModalToggle() {
            eventBus.$emit('card-modal-toggle', this.card);
        },
        cardClickHandler: function cardClickHandler(event) {
            var clickableElements = ["audor-card", "audor-card-heading", "card-labels-list", "card-info-icons", "card-image"];
            var _iteratorNormalCompletion2 = true;
            var _didIteratorError2 = false;
            var _iteratorError2 = undefined;

            try {
                for (var _iterator2 = (0, _getIterator3.default)(clickableElements), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                    var elem = _step2.value;

                    if (event.target.className.indexOf(elem) >= 0) {
                        this.cardModalToggle();
                        return;
                    }
                }
            } catch (err) {
                _didIteratorError2 = true;
                _iteratorError2 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion2 && _iterator2.return) {
                        _iterator2.return();
                    }
                } finally {
                    if (_didIteratorError2) {
                        throw _iteratorError2;
                    }
                }
            }
        }
    }
};
})()
if (module.exports.__esModule) module.exports = module.exports.default
var __vue__options__ = (typeof module.exports === "function"? module.exports.options: module.exports)
__vue__options__.render = function render () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return (_vm.board.prefs)?_c('div',{staticClass:"audor-card",on:{"click":_vm.cardClickHandler}},[(_vm.board.prefs.showImagesOnCards)?_c('img',{staticClass:"card-image",attrs:{"src":_vm.card.image}}):_vm._e(),_c('div',{staticClass:"audor-card-info"},[(_vm.isOptionsAvaliable)?_c('b-dropdown',{staticClass:"audor-options-btn",attrs:{"no-caret":"no-caret","boundary":"window","variant":"link"}},[(_vm.$root.userRole.permissions.card.update)?_c('b-dropdown-item',{on:{"click":_vm.cardModalToggle}},[_vm._v("Изменить")]):_vm._e(),(_vm.$root.userRole.permissions.card.remove)?_c('b-dropdown-item',{on:{"click":function($event){_vm.$emit('card-remove')}}},[_vm._v("Удалить")]):_vm._e(),_c('template',{slot:"button-content"},[_c('i',{staticClass:"fa fa-fw fa-ellipsis-h"})])],2):_vm._e(),(_vm.board.prefs.showLabelsOnCards && _vm.card.labels !== undefined && _vm.card.labels.length !== 0)?_c('div',{staticClass:"card-labels-list"},_vm._l((_vm.card.labels),function(label,key){return _c('div',{staticClass:"card-label-small",style:({background: label})})})):_vm._e(),_c('div',{staticClass:"audor-card-heading"},[_vm._v(_vm._s(_vm.card.name))]),(_vm.hasIcons)?_c('div',{staticClass:"card-info-icons"},[(_vm.card.description)?_c('i',{directives:[{name:"b-tooltip",rawName:"v-b-tooltip",value:(_vm.v-_vm.b-_vm.tooltip),expression:"v-b-tooltip"}],staticClass:"fa fa-fw fa-file-alt",attrs:{"title":"Карточка имеет описание"}}):_vm._e(),(!_vm.board.prefs.showProgressBarsOnCards && _vm.card.progressBars && _vm.card.progressBars.length > 0)?_c('i',{directives:[{name:"b-tooltip",rawName:"v-b-tooltip",value:(_vm.v-_vm.b-_vm.tooltip),expression:"v-b-tooltip"}],staticClass:"fa fa-fw fa-bars",attrs:{"title":"Карточка имеет индикаторы прогресса"}}):_vm._e(),(!_vm.board.prefs.showImagesOnCards && _vm.card.image)?_c('i',{directives:[{name:"b-tooltip",rawName:"v-b-tooltip",value:(_vm.v-_vm.b-_vm.tooltip),expression:"v-b-tooltip"}],staticClass:"fa fa-fw fa-image",attrs:{"title":"Карточка имеет картинку"}}):_vm._e()]):_vm._e(),(_vm.board.prefs.showProgressBarsOnCards)?_c('div',{staticClass:"card-progress"},_vm._l((_vm.card.progressBars),function(item,index){return _c('CardProgressBar',{attrs:{"progressBar":item,"removable":true,"compact":true},on:{"remove":function($event){_vm.removeProgressBar(index)}}})})):_vm._e()],1)]):_vm._e()}
__vue__options__.staticRenderFns = []

},{"./CardProgressBar.vue":10,"babel-runtime/core-js/get-iterator":22}],3:[function(require,module,exports){
;(function(){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _require = require("vuelidate/lib/validators"),
    required = _require.required;

exports.default = {
    name: "AudorCardForm",
    data: function data() {
        return {
            formVisible: false,
            name: ""
        };
    },
    validations: {
        name: {
            required: required
        }
    },
    methods: {
        showForm: function showForm() {
            this.formVisible = true;
            this.$v.$reset();
        },
        hideForm: function hideForm() {
            this.name = "";
            this.formVisible = false;
            this.$v.$reset();
        },
        getFieldState: function getFieldState(form, field) {
            if (!this.$v) return null;

            if (!field) {
                if (!this.$v[form].$dirty) return null;
                return !this.$v[form].$invalid;
            }

            if (!this.$v[form][field].$dirty) return null;
            return !this.$v[form][field].$invalid;
        },

        createNewCard: function createNewCard() {
            this.$emit("create-card", this.name);
            this.hideForm();
        }
    }
};
})()
if (module.exports.__esModule) module.exports = module.exports.default
var __vue__options__ = (typeof module.exports === "function"? module.exports.options: module.exports)
__vue__options__.render = function render () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"audor-card-form"},[(_vm.formVisible)?_c('form',{staticClass:"new-card-form",on:{"submit":function($event){$event.preventDefault();return _vm.createNewCard($event)}}},[_c('b-form-group',[_c('b-form-textarea',{attrs:{"name":"name","state":_vm.getFieldState('name')},on:{"input":function($event){_vm.$v.name.$touch()},"keyup":function($event){if(!('button' in $event)&&_vm._k($event.keyCode,"enter",13,$event.key,"Enter")){ return null; }return _vm.createNewCard($event)}},model:{value:(_vm.name),callback:function ($$v) {_vm.name=$$v},expression:"name"}}),_c('b-form-invalid-feedback',[_vm._v("Название карточки не может быть пустым")])],1),_c('b-form-group',[_c('b-button-group',[_c('b-button',{attrs:{"variant":"success","disabled":_vm.$v.name.$invalid},on:{"click":_vm.createNewCard}},[_vm._v("Создать")]),_c('b-button',{attrs:{"variant":"default"},on:{"click":function($event){$event.preventDefault();return _vm.hideForm($event)}}},[_c('span',{staticClass:"fa fa-fw fa-times"})])],1)],1)],1):_c('a',{staticClass:"grey-link new-card-prompt",attrs:{"href":"#"},on:{"click":function($event){$event.preventDefault();return _vm.showForm($event)}}},[_vm._v("Добавить карточку")])])}
__vue__options__.staticRenderFns = []

},{"vuelidate/lib/validators":91}],4:[function(require,module,exports){
(function (global){
;(function(){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    name: "AudorList",
    components: {
        AudorCard: require("./AudorCard.vue"),
        AudorCardForm: require("./AudorCardForm.vue"),
        Draggable: require("vuedraggable")
    },
    computed: {
        cardDragState: function cardDragState() {
            if (!this.$root.userRole.permissions) return true;
            return !this.$root.userRole.permissions.card.update;
        },
        containerStyles: function containerStyles() {
            var containerStyles = ["audor-card-inner-container"];
            if (this.$root.cardMoving) containerStyles.push("card-placeholder");
            return containerStyles;
        },
        hasCards: function hasCards() {
            return this.list.cards.length > 0;
        },
        listStyles: function listStyles() {
            if (this.$root.cardMoving) return "";
            if (!this.hasCards) return "empty";
        }
    },
    data: function data() {
        return {
            headingEdit: false,
            cardDraggableOptions: {
                group: "card",
                animation: 300,
                disabled: this.$root.isMobilePhone || this.cardDragState
            }
        };
    },
    props: {
        list: Object
    },
    methods: {
        toggleEdit: function toggleEdit() {
            if (this.$root.userRole.permissions.list.update) this.headingEdit = true;
        },
        listNameEdit: function listNameEdit() {
            this.headingEdit = false;
            this.$http.post("/rest/lists/" + this.list._id, this.list);
        },
        createCard: function createCard(name) {
            var _this = this;

            var data = { name: name, list: this.list, board: global.board };

            this.$http.post("/rest/cards/", data).then(function (response) {
                _this.list.cards.push(response.data);
            });
        },
        removeCard: function removeCard(card, index) {
            var _this2 = this;

            this.$http.delete("/rest/cards/" + card._id).then(function (response) {
                _this2.list.cards.splice(index, 1);
            });
        },
        cardMoveStart: function cardMoveStart() {
            this.$root.cardMoving = true;
        },
        cardMove: function cardMove() {
            eventBus.$emit("card-move");
            this.$root.cardMoving = false;
        }
    }
};
})()
if (module.exports.__esModule) module.exports = module.exports.default
var __vue__options__ = (typeof module.exports === "function"? module.exports.options: module.exports)
__vue__options__.render = function render () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"audor-list",class:_vm.listStyles},[_c('div',{staticClass:"audor-list-head"},[(!_vm.headingEdit && (_vm.$root.userRole.permissions.list.update|| _vm.$root.userRole.permissions.list.remove))?_c('b-dropdown',{staticClass:"audor-options-btn",attrs:{"right":"right","no-caret":"no-caret","boundary":"window","variant":"link"}},[_c('template',{slot:"button-content"},[_c('i',{staticClass:"fa fa-fw fa-ellipsis-h"})]),(_vm.$root.userRole.permissions.list.update)?_c('b-dropdown-item',{on:{"click":function($event){_vm.headingEdit = true}}},[_vm._v("Изменить")]):_vm._e(),(_vm.$root.userRole.permissions.list.remove)?_c('b-dropdown-item',{on:{"click":function($event){_vm.$emit('list-remove')}}},[_vm._v("Удалить")]):_vm._e()],2):_vm._e(),(!_vm.headingEdit)?_c('div',{staticClass:"audor-list-heading",on:{"click":_vm.toggleEdit}},[_vm._v(_vm._s(_vm.list.name))]):_c('b-form-input',{attrs:{"type":"text"},nativeOn:{"keyup":function($event){if(!('button' in $event)&&_vm._k($event.keyCode,"enter",13,$event.key,"Enter")){ return null; }return _vm.listNameEdit($event)}},model:{value:(_vm.list.name),callback:function ($$v) {_vm.$set(_vm.list, "name", $$v)},expression:"list.name"}})],1),_c('div',{staticClass:"audor-card-container"},[_c('draggable',{class:_vm.containerStyles,attrs:{"options":_vm.cardDraggableOptions},on:{"start":_vm.cardMoveStart,"end":_vm.cardMove},model:{value:(_vm.list.cards),callback:function ($$v) {_vm.$set(_vm.list, "cards", $$v)},expression:"list.cards"}},_vm._l((_vm.list.cards),function(card,index){return _c('AudorCard',{key:card._id,attrs:{"card":card},on:{"card-remove":function($event){_vm.removeCard(card, index)}}})}))],1),(_vm.$root.userRole.permissions.card.create)?_c('AudorCardForm',{on:{"create-card":_vm.createCard}}):_vm._e()],1)}
__vue__options__.staticRenderFns = []

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"./AudorCard.vue":2,"./AudorCardForm.vue":3,"vuedraggable":83}],5:[function(require,module,exports){
;(function(){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _require = require("vuelidate/lib/validators"),
    required = _require.required;

exports.default = {
    data: function data() {
        return {
            formVisible: false,
            name: ""
        };
    },
    validations: {
        name: {
            required: required
        }
    },
    methods: {
        showForm: function showForm() {
            this.formVisible = true;
            this.$v.$reset();
        },
        hideForm: function hideForm() {
            this.name = "";
            this.formVisible = false;
            this.$v.$reset();
        },
        getFieldState: function getFieldState(form, field) {
            if (!this.$v) return null;

            if (!field) {
                if (!this.$v[form].$dirty) return null;
                return !this.$v[form].$invalid;
            }

            if (!this.$v[form][field].$dirty) return null;
            return !this.$v[form][field].$invalid;
        },

        createList: function createList(event) {
            this.formVisible = false;
            this.$emit("list-create", this.name);
            this.name = "";
        }
    }
};
})()
if (module.exports.__esModule) module.exports = module.exports.default
var __vue__options__ = (typeof module.exports === "function"? module.exports.options: module.exports)
__vue__options__.render = function render () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"audor-list-form"},[(_vm.formVisible)?_c('form',{on:{"submit":function($event){$event.preventDefault();return _vm.createList($event)}}},[_c('b-form-group',[_c('b-form-input',{attrs:{"type":"text","state":_vm.getFieldState('name')},on:{"input":function($event){_vm.$v.name.$touch()}},model:{value:(_vm.name),callback:function ($$v) {_vm.name=$$v},expression:"name"}}),_c('b-form-invalid-feedback',[_vm._v("Название списка не может быть пустым")])],1),_c('b-form-group',[_c('b-button-group',[_c('b-button',{attrs:{"variant":"success","disabled":_vm.$v.name.$invalid},on:{"click":_vm.createList}},[_vm._v("Создать")]),_c('b-button',{attrs:{"variant":"default"},on:{"click":_vm.hideForm}},[_c('span',{staticClass:"fa fa-fw fa-times"})])],1)],1)],1):_c('a',{staticClass:"grey-link",attrs:{"href":"#"},on:{"click":function($event){$event.preventDefault();return _vm.showForm($event)}}},[_vm._v("Создать список")])])}
__vue__options__.staticRenderFns = []

},{"vuelidate/lib/validators":91}],6:[function(require,module,exports){
;(function(){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    name: "BeforeAfter",
    props: {
        before: String,
        after: String,
        disabled: Boolean
    },
    data: function data() {
        return {
            containerSize: {},
            containerStyle: {},
            offsetX: Number
        };
    },

    computed: {
        beforeClip: function beforeClip() {
            return {
                clip: "rect(0, " + this.offsetX + "px, " + this.containerSize.height + "px, 0)"
            };
        }
    },
    methods: {
        setDimensions: function setDimensions() {
            var targetElement = this.$el.querySelector(".after-img");
            var targetRect = targetElement.getBoundingClientRect();

            this.containerSize.height = targetRect.height;
            this.containerSize.width = targetRect.width;

            this.offsetX = this.containerSize.width / 2;

            this.containerStyle = { height: this.containerSize.height + "px", width: this.containerSize.width + "px" };
        },
        mouseHandler: function mouseHandler(event) {
            if (this.disabled) return;
            var targetRect = event.target.getBoundingClientRect();
            this.offsetX = event.touches ? event.touches[0].pageX - targetRect.left : event.offsetX;
        },
        resize: function resize() {
            var _this = this;

            this.containerSize = {};
            this.containerStyle = {};
            this.$nextTick(function () {
                return _this.setDimensions();
            });
        }
    },
    created: function created() {
        window.addEventListener("resize", this.resize);
    }
};
})()
if (module.exports.__esModule) module.exports = module.exports.default
var __vue__options__ = (typeof module.exports === "function"? module.exports.options: module.exports)
__vue__options__.render = function render () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"before-after-container",style:(_vm.containerStyle),on:{"mousemove":_vm.mouseHandler,"touchmove":_vm.mouseHandler}},[_c('img',{staticClass:"before-img",style:(_vm.beforeClip),attrs:{"src":_vm.before}}),_c('img',{staticClass:"after-img",attrs:{"src":_vm.after},on:{"load":_vm.setDimensions}})])}
__vue__options__.staticRenderFns = []

},{}],7:[function(require,module,exports){
;(function(){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _require = require("vuelidate/lib/validators"),
    required = _require.required,
    maxLength = _require.maxLength;

exports.default = {
    data: function data() {
        return {
            userInviteForm: {
                login: "",
                role: "",
                error: ""
            },
            userRoles: [{ value: "admin", text: "Администратор" }, { value: "user", text: "Пользователь" }, { value: "guest", text: "Гость" }]
        };
    },

    computed: {
        board: function board() {
            return this.$parent.board;
        }
    },
    validations: {
        board: {
            name: {
                required: required,
                maxLength: maxLength(30)
            }
        },
        userInviteForm: {
            login: {
                required: required
            },
            role: {
                required: required
            }
        }
    },
    methods: {
        getFieldState: function getFieldState(form, field) {
            if (!this.$v) return null;

            if (!field) {
                if (!this.$v[form].$dirty) return null;
                return !this.$v[form].$invalid;
            }

            if (!this.$v[form][field].$dirty) return null;
            return !this.$v[form][field].$invalid;
        },
        removeBackground: function removeBackground() {
            var _this = this;

            this.$http.delete("/rest/boards/" + this.board._id + "/background").then(function () {
                _this.board.background = undefined;
            });
        },
        changeBackground: function changeBackground(event) {
            var _this2 = this;

            var formData = new FormData(event.target);
            this.$http.post("/rest/boards/" + this.board._id + "/background", formData).then(function (response) {
                _this2.$set(_this2.board, "background", response.data.background);
            });
        },
        updateBoard: function updateBoard() {
            var _this3 = this;

            this.$http.post("/rest/boards/" + this.board._id, this.board).then(function () {
                _this3.$parent.boardModalVisible = false;
            });
        },
        removeBoard: function removeBoard() {
            eventBus.$emit("board-remove", this.board);
        },
        removeUser: function removeUser(index) {
            var _this4 = this;

            var removedUser = this.board.users[index];
            this.$http.delete("/rest/boards/" + this.board._id + "/users/" + removedUser._id).then(function (response) {
                _this4.board.users.splice(index, 1);
            });
        },
        addUser: function addUser() {
            var _this5 = this;

            this.userInviteForm.error = "";

            this.$http.post("/rest/boards/" + this.board._id + "/users", this.userInviteForm).then(function (response) {
                console.log(response.data);
                if (response.data.error) {
                    _this5.userInviteForm.error = response.data.error;
                    return;
                }

                _this5.board.users = response.data;
            });
        }
    }
};
})()
if (module.exports.__esModule) module.exports = module.exports.default
var __vue__options__ = (typeof module.exports === "function"? module.exports.options: module.exports)
__vue__options__.render = function render () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return (_vm.board)?_c('b-modal',{attrs:{"title":"Настройки доски"},model:{value:(_vm.$parent.boardModalVisible),callback:function ($$v) {_vm.$set(_vm.$parent, "boardModalVisible", $$v)},expression:"$parent.boardModalVisible"}},[_c('b-tabs',{attrs:{"pills":"pills","nav-class":"mb-3"}},[_c('b-tab',{attrs:{"title":"Основные"}},[_c('b-form-group',[_c('label',[_vm._v("Название")]),_c('b-form-input',{attrs:{"type":"text","state":_vm.getFieldState('board', 'name'),"placeholder":"Название"},on:{"input":function($event){_vm.$v.board.name.$touch()}},model:{value:(_vm.board.name),callback:function ($$v) {_vm.$set(_vm.board, "name", $$v)},expression:"board.name"}}),(!_vm.$v.board.name.required)?_c('b-form-invalid-feedback',[_vm._v("Название доски не может быть пустым")]):_vm._e(),(!_vm.$v.board.name.maxLength)?_c('b-form-invalid-feedback',[_vm._v("Название доски не должно превышать 30 символов")]):_vm._e()],1),(_vm.board.prefs)?_c('div',{staticClass:"board-prefs-block"},[_c('div',{staticClass:"form-check"},[_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.board.prefs.showImagesOnCards),expression:"board.prefs.showImagesOnCards"}],staticClass:"form-check-input",attrs:{"type":"checkbox"},domProps:{"checked":Array.isArray(_vm.board.prefs.showImagesOnCards)?_vm._i(_vm.board.prefs.showImagesOnCards,null)>-1:(_vm.board.prefs.showImagesOnCards)},on:{"change":function($event){var $$a=_vm.board.prefs.showImagesOnCards,$$el=$event.target,$$c=$$el.checked?(true):(false);if(Array.isArray($$a)){var $$v=null,$$i=_vm._i($$a,$$v);if($$el.checked){$$i<0&&(_vm.$set(_vm.board.prefs, "showImagesOnCards", $$a.concat([$$v])))}else{$$i>-1&&(_vm.$set(_vm.board.prefs, "showImagesOnCards", $$a.slice(0,$$i).concat($$a.slice($$i+1))))}}else{_vm.$set(_vm.board.prefs, "showImagesOnCards", $$c)}}}}),_c('label',{staticClass:"form-check-label"},[_vm._v("Показывать изображения на карточках")])]),_c('div',{staticClass:"form-check"},[_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.board.prefs.showProgressBarsOnCards),expression:"board.prefs.showProgressBarsOnCards"}],staticClass:"form-check-input",attrs:{"type":"checkbox"},domProps:{"checked":Array.isArray(_vm.board.prefs.showProgressBarsOnCards)?_vm._i(_vm.board.prefs.showProgressBarsOnCards,null)>-1:(_vm.board.prefs.showProgressBarsOnCards)},on:{"change":function($event){var $$a=_vm.board.prefs.showProgressBarsOnCards,$$el=$event.target,$$c=$$el.checked?(true):(false);if(Array.isArray($$a)){var $$v=null,$$i=_vm._i($$a,$$v);if($$el.checked){$$i<0&&(_vm.$set(_vm.board.prefs, "showProgressBarsOnCards", $$a.concat([$$v])))}else{$$i>-1&&(_vm.$set(_vm.board.prefs, "showProgressBarsOnCards", $$a.slice(0,$$i).concat($$a.slice($$i+1))))}}else{_vm.$set(_vm.board.prefs, "showProgressBarsOnCards", $$c)}}}}),_c('label',{staticClass:"form-check-label"},[_vm._v("Показывать индикаторы на карточках")])]),_c('div',{staticClass:"form-check"},[_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.board.prefs.showLabelsOnCards),expression:"board.prefs.showLabelsOnCards"}],staticClass:"form-check-input",attrs:{"type":"checkbox"},domProps:{"checked":Array.isArray(_vm.board.prefs.showLabelsOnCards)?_vm._i(_vm.board.prefs.showLabelsOnCards,null)>-1:(_vm.board.prefs.showLabelsOnCards)},on:{"change":function($event){var $$a=_vm.board.prefs.showLabelsOnCards,$$el=$event.target,$$c=$$el.checked?(true):(false);if(Array.isArray($$a)){var $$v=null,$$i=_vm._i($$a,$$v);if($$el.checked){$$i<0&&(_vm.$set(_vm.board.prefs, "showLabelsOnCards", $$a.concat([$$v])))}else{$$i>-1&&(_vm.$set(_vm.board.prefs, "showLabelsOnCards", $$a.slice(0,$$i).concat($$a.slice($$i+1))))}}else{_vm.$set(_vm.board.prefs, "showLabelsOnCards", $$c)}}}}),_c('label',{staticClass:"form-check-label"},[_vm._v("Показывать метки на карточках")])])]):_vm._e()],1),_c('b-tab',{attrs:{"title":"Фоновое изображение"}},[(_vm.board.background)?_c('img',{staticClass:"img-fluid mb-3",attrs:{"src":_vm.board.background}}):_vm._e(),_c('form',{on:{"submit":function($event){$event.preventDefault();return _vm.changeBackground($event)}}},[_c('div',{staticClass:"form-group"},[_c('input',{staticClass:"form-control",attrs:{"type":"file","name":"background"}})]),_c('div',{staticClass:"form-group"},[_c('input',{staticClass:"btn btn-success mr-2",attrs:{"type":"submit","value":"Загрузить"}}),(_vm.board.background)?_c('input',{staticClass:"btn btn-danger",attrs:{"type":"button","value":"Удалить"},on:{"click":_vm.removeBackground}}):_vm._e()])])]),_c('b-tab',{attrs:{"title":"Пользователи"}},[_c('table',{staticClass:"table table-striped table-bordered"},[_c('thead',[_c('tr',[_c('th',[_vm._v("Имя пользователя")]),_c('th',[_vm._v("Группа")]),_c('th',[_vm._v("Действия")])])]),_c('tbody',_vm._l((_vm.board.users),function(entry,index){return _c('tr',[_c('td',[_vm._v(_vm._s(entry.user.login))]),_c('td',[_vm._v(_vm._s(entry.role.name))]),_c('td',[_c('b-button',{attrs:{"variant":"danger"},on:{"click":function($event){_vm.removeUser(index)}}},[_vm._v("Удалить")])],1)])}))]),(_vm.userInviteForm.error)?_c('b-alert',{attrs:{"variant":"danger","show":_vm.userInviteForm.error}},[_vm._v(_vm._s(_vm.userInviteForm.error))]):_vm._e(),_c('b-form-group',[_c('label',[_vm._v("Логин")]),_c('b-form-input',{attrs:{"type":"text","state":_vm.getFieldState('userInviteForm', 'login')},on:{"input":function($event){_vm.$v.userInviteForm.login.$touch()}},model:{value:(_vm.userInviteForm.login),callback:function ($$v) {_vm.$set(_vm.userInviteForm, "login", $$v)},expression:"userInviteForm.login"}}),_c('b-form-invalid-feedback',[_vm._v("Логин пользователя не может быть пустым")])],1),_c('b-form-group',[_c('label',[_vm._v("Группа")]),_c('b-form-select',{attrs:{"state":_vm.getFieldState('userInviteForm', 'role'),"options":_vm.userRoles},on:{"input":function($event){_vm.$v.userInviteForm.role.$touch()}},model:{value:(_vm.userInviteForm.role),callback:function ($$v) {_vm.$set(_vm.userInviteForm, "role", $$v)},expression:"userInviteForm.role"}}),_c('b-form-invalid-feedback',[_vm._v("Выберите группу пользователя")])],1),_c('b-form-group',[_c('b-button',{attrs:{"variant":"success","disabled":_vm.$v.userInviteForm.$invalid},on:{"click":_vm.addUser}},[_vm._v("Добавить")])],1)],1),_c('b-tab',{attrs:{"title":"Удалить доску"}},[_c('span',[_vm._v("Вы точно уверены что хотите удалить доску")]),_c('div',{staticClass:"form-group mt-1"},[_c('button',{staticClass:"btn btn-danger",on:{"click":_vm.removeBoard}},[_vm._v("Удалить")])])])],1),_c('template',{slot:"modal-footer"},[_c('b-btn',{attrs:{"variant":"success","disabled":_vm.$v.board.$invalid},on:{"click":_vm.updateBoard}},[_vm._v("Сохранить")]),_c('button',{staticClass:"btn btn-secondary",on:{"click":function($event){_vm.$parent.boardModalVisible = false}}},[_vm._v("Закрыть")])],1)],2):_vm._e()}
__vue__options__.staticRenderFns = []

},{"vuelidate/lib/validators":91}],8:[function(require,module,exports){
;(function(){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    name: "BoardTile",
    props: {
        board: Object
    },
    computed: {
        backgroundImage: function backgroundImage() {
            return this.board.background ? { backgroundImage: "url(" + this.board.background + ")" } : "";
        }
    }
};
})()
if (module.exports.__esModule) module.exports = module.exports.default
var __vue__options__ = (typeof module.exports === "function"? module.exports.options: module.exports)
__vue__options__.render = function render () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('router-link',{staticClass:"col-md-6 col-lg-3 mb-3",attrs:{"to":'/boards/' + _vm.board._id}},[_c('div',{staticClass:"col-md-12 board-tile",style:(_vm.backgroundImage)},[_c('div',{staticClass:"board-tile-name"},[_vm._v(_vm._s(_vm.board.name))])])])}
__vue__options__.staticRenderFns = []

},{}],9:[function(require,module,exports){
;(function(){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _assign = require("babel-runtime/core-js/object/assign");

var _assign2 = _interopRequireDefault(_assign);

var _getIterator2 = require("babel-runtime/core-js/get-iterator");

var _getIterator3 = _interopRequireDefault(_getIterator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    name: "CardModal",
    components: {
        "CardProgressBar": require("./CardProgressBar.vue")
    },
    props: {
        card: {},
        list: {},
        labelColors: Array
    },
    computed: {
        createdAt: function createdAt() {
            if (!this.card.createdAt) return "";
            return moment(this.card.createdAt).fromNow();
        },

        avaliableColors: function avaliableColors() {
            var avaliableLabels = this.labelColors.slice();
            if (typeof this.card.labels === "undefined") return avaliableLabels;
            var _iteratorNormalCompletion = true;
            var _didIteratorError = false;
            var _iteratorError = undefined;

            try {
                for (var _iterator = (0, _getIterator3.default)(this.card.labels), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                    var label = _step.value;
                    avaliableLabels.splice(avaliableLabels.indexOf(label), 1);
                }
            } catch (err) {
                _didIteratorError = true;
                _iteratorError = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion && _iterator.return) {
                        _iterator.return();
                    }
                } finally {
                    if (_didIteratorError) {
                        throw _iteratorError;
                    }
                }
            }

            return avaliableLabels;
        }
    },
    data: function data() {
        return {
            editMode: false,
            progressBarForm: {
                name: "",
                value: 50,
                color: "bg-success"
            }
        };
    },
    methods: {
        attachLabel: function attachLabel(index) {
            if (typeof this.card.labels === "undefined") this.card.labels = [];
            this.card.labels.push(this.avaliableColors[index]);
        },
        removeLabel: function removeLabel(index) {
            this.card.labels.splice(index, 1);
        },
        updateCard: function updateCard() {
            this.$http.post("/rest/cards/" + this.card._id, this.card);
            this.editMode = false;
        },
        uploadImage: function uploadImage(event) {
            var _this = this;

            event.preventDefault();

            var formData = new FormData(event.target);

            this.$http.post("/rest/cards/" + this.card._id, formData).then(function (response) {
                _this.$set(_this.card, "image", response.body.image);
            });
        },
        removeImage: function removeImage() {
            var _this2 = this;

            this.$http.delete("/rest/cards/" + this.card._id + "/image").then(function () {
                _this2.card.image = undefined;
            });
        },
        addProgressBar: function addProgressBar() {
            if (this.card.progressBars === undefined) this.card.progressBars = [];
            this.card.progressBars.push((0, _assign2.default)({}, this.progressBarForm));
        },
        removeProgressBar: function removeProgressBar(index) {
            this.card.progressBars.splice(index, 1);
            if (this.card.progressBars.length === 0) this.card.progressBars = undefined;
        }
    }
};
})()
if (module.exports.__esModule) module.exports = module.exports.default
var __vue__options__ = (typeof module.exports === "function"? module.exports.options: module.exports)
__vue__options__.render = function render () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return (_vm.card)?_c('b-modal',{model:{value:(_vm.$root.cardModalVisible),callback:function ($$v) {_vm.$set(_vm.$root, "cardModalVisible", $$v)},expression:"$root.cardModalVisible"}},[_c('template',{staticClass:"d-block",slot:"modal-title"},[_c('h4',{staticClass:"modal-title d-block"},[_vm._v(_vm._s(_vm.card.name))]),_c('small',{staticClass:"font-weight-light"},[_vm._v("в списке "+_vm._s(_vm.list.name))])]),(_vm.editMode)?_c('b-tabs',{attrs:{"pills":"pills","nav-class":"mb-3"}},[_c('b-tab',{attrs:{"active":"active","title":"Основные"}},[_c('div',{staticClass:"form-group"},[_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.card.name),expression:"card.name"}],staticClass:"form-control",attrs:{"type":"text"},domProps:{"value":(_vm.card.name)},on:{"input":function($event){if($event.target.composing){ return; }_vm.$set(_vm.card, "name", $event.target.value)}}})]),_c('div',{staticClass:"form-group"},[_c('textarea',{directives:[{name:"model",rawName:"v-model",value:(_vm.card.description),expression:"card.description"}],staticClass:"form-control",attrs:{"placeholder":"Описание карточки"},domProps:{"value":(_vm.card.description)},on:{"input":function($event){if($event.target.composing){ return; }_vm.$set(_vm.card, "description", $event.target.value)}}})])]),_c('b-tab',{attrs:{"title":"Изображение"}},[_c('form',{on:{"submit":_vm.uploadImage}},[_c('div',{staticClass:"form-group"},[(_vm.card.image)?_c('img',{staticClass:"img-fluid",attrs:{"src":_vm.card.image}}):_vm._e()]),_c('div',{staticClass:"form-group"},[_c('input',{staticClass:"form-control",attrs:{"type":"file","name":"image"}})]),_c('div',{staticClass:"form-group"},[_c('input',{staticClass:"btn btn-success mr-2",attrs:{"type":"submit","value":"Загрузить"}}),(_vm.card.image)?_c('a',{staticClass:"btn btn-danger",attrs:{"href":"#"},on:{"click":function($event){$event.preventDefault();return _vm.removeImage($event)}}},[_vm._v("Удалить")]):_vm._e()])])]),_c('b-tab',{attrs:{"title":"Метки"}},[_c('div',{staticClass:"card-labels-list"},[_c('p',[_vm._v("Доступные метки:")]),_vm._l((_vm.avaliableColors),function(label,key){return _c('div',{staticClass:"card-label-big",style:({background: label}),attrs:{"data-index":key},on:{"click":function($event){_vm.attachLabel(key)}}})})],2),_c('div',{staticClass:"card-labels-list"},[_c('p',[_vm._v("Выбранные метки:")]),_vm._l((_vm.card.labels),function(label,key){return _c('div',{staticClass:"card-label-big",style:({background: label}),attrs:{"data-index":key},on:{"click":function($event){_vm.removeLabel(key)}}})})],2)]),_c('b-tab',{attrs:{"title":"Индикаторы"}},[_vm._l((_vm.card.progressBars),function(item,index){return _c('CardProgressBar',{attrs:{"progressBar":item,"removable":true},on:{"remove":function($event){_vm.removeProgressBar(index)}}})}),_c('form',{staticClass:"border rounded p-3 mt-3",on:{"submit":function($event){$event.preventDefault();return _vm.addProgressBar($event)}}},[_c('div',{staticClass:"form-group"},[_c('label',[_vm._v("Название")]),_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.progressBarForm.name),expression:"progressBarForm.name"}],staticClass:"form-control",attrs:{"type":"text"},domProps:{"value":(_vm.progressBarForm.name)},on:{"input":function($event){if($event.target.composing){ return; }_vm.$set(_vm.progressBarForm, "name", $event.target.value)}}})]),_c('div',{staticClass:"form-group"},[_c('label',[_vm._v("Значение")]),_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.progressBarForm.value),expression:"progressBarForm.value"}],staticClass:"form-control",attrs:{"type":"number","min":"0","max":"100"},domProps:{"value":(_vm.progressBarForm.value)},on:{"input":function($event){if($event.target.composing){ return; }_vm.$set(_vm.progressBarForm, "value", $event.target.value)}}})]),_c('div',{staticClass:"form-group"},[_c('label',[_vm._v("Цвет")]),_c('select',{directives:[{name:"model",rawName:"v-model",value:(_vm.progressBarForm.color),expression:"progressBarForm.color"}],staticClass:"form-control",on:{"change":function($event){var $$selectedVal = Array.prototype.filter.call($event.target.options,function(o){return o.selected}).map(function(o){var val = "_value" in o ? o._value : o.value;return val}); _vm.$set(_vm.progressBarForm, "color", $event.target.multiple ? $$selectedVal : $$selectedVal[0])}}},[_c('option',{attrs:{"value":"bg-success","selected":"selected"}},[_vm._v("Зеленый")]),_c('option',{attrs:{"value":"bg-warning"}},[_vm._v("Желтый")]),_c('option',{attrs:{"value":"bg-info"}},[_vm._v("Синий")]),_c('option',{attrs:{"value":"bg-danger"}},[_vm._v("Красный")])])]),_c('div',{staticClass:"form-group"},[_c('input',{staticClass:"btn btn-success",attrs:{"type":"submit","value":"Добавить"}})])])],2)],1):[(_vm.card.owner)?_c('div',{staticClass:"card-owner-block mb-3"},[_c('span',{staticClass:"fa fa-fw fa-user mb-1 mr-1"}),_c('span',{staticClass:"font-weight-bold"},[_vm._v("Создатель")]),_c('a',{staticClass:"d-block",attrs:{"href":'/users/' + _vm.card.owner._id}},[_vm._v(_vm._s(_vm.card.owner.name))])]):_vm._e(),_c('div',{staticClass:"card-dates-block mb-3"},[_c('span',{staticClass:"fa fa-fw fa-clock mb-1 mr-1"}),_c('span',{staticClass:"font-weight-bold"},[_vm._v("Даты")]),_c('div',{staticClass:"d-block"},[_vm._v("Дата создания: "+_vm._s(_vm.createdAt))])]),(_vm.card.image)?_c('div',{staticClass:"card-image-block mb-3"},[_c('span',{staticClass:"fa fa-fw fa-image mr-1"}),_c('span',{staticClass:"font-weight-bold"},[_vm._v("Изображение")]),_c('img',{staticClass:"img-fluid mt-2",attrs:{"src":_vm.card.image}})]):_vm._e(),(_vm.card.description)?_c('div',{staticClass:"card-description-block mb-3"},[_c('span',{staticClass:"fa fa-fw fa-file-alt mb-1 mr-1"}),_c('span',{staticClass:"font-weight-bold"},[_vm._v("Описание")]),_c('div',{staticClass:"d-block card-description"},[_vm._v(_vm._s(_vm.card.description))])]):_vm._e(),(_vm.card.progressBars !== undefined && _vm.card.progressBars.length !== 0)?_c('div',{staticClass:"card-progresbar-list mb-3"},[_c('span',{staticClass:"fa fa-fw fa-bars mr-1"}),_c('span',{staticClass:"font-weight-bold"},[_vm._v("Индикаторы")]),_vm._l((_vm.card.progressBars),function(progressBar){return _c('CardProgressBar',{attrs:{"progressBar":progressBar}})})],2):_vm._e(),(_vm.card.labels !== undefined && _vm.card.labels.length !== 0)?_c('div',{staticClass:"card-labels-list mb-3"},[_c('div',{staticClass:"d-block mb-2"},[_c('span',{staticClass:"fa fa-fw fa-tag mr-1"}),_c('span',{staticClass:"font-weight-bold"},[_vm._v("Метки")])]),_vm._l((_vm.card.labels),function(label,key){return _c('div',{staticClass:"card-label-big",style:({background: label})})})],2):_vm._e()],_c('template',{slot:"modal-footer"},[(_vm.$root.userRole.permissions.card.update)?_c('a',{attrs:{"href":"#"},on:{"click":function($event){$event.preventDefault();_vm.editMode = !_vm.editMode}}},[_vm._v(_vm._s(_vm.editMode ? "Режим просмотра" : "Режим редактирования"))]):_vm._e(),(_vm.editMode)?_c('button',{staticClass:"btn btn-success",on:{"click":_vm.updateCard}},[_vm._v("Сохранить")]):_vm._e(),_c('button',{staticClass:"btn btn-secondary",attrs:{"data-dismiss":"modal"},on:{"click":function($event){_vm.editMode = false}}},[_vm._v("Закрыть")])])],2):_vm._e()}
__vue__options__.staticRenderFns = []

},{"./CardProgressBar.vue":10,"babel-runtime/core-js/get-iterator":22,"babel-runtime/core-js/object/assign":23}],10:[function(require,module,exports){
;(function(){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    name: "CardProgressBar",
    props: {
        progressBar: Object,
        removable: false,
        compact: false
    }
};
})()
if (module.exports.__esModule) module.exports = module.exports.default
var __vue__options__ = (typeof module.exports === "function"? module.exports.options: module.exports)
__vue__options__.render = function render () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return (!_vm.compact)?_c('div',{staticClass:"card-progress mt-3 border rounded p-3"},[_c('span',[_vm._v(_vm._s(_vm.progressBar.name))]),_c('div',{staticClass:"progress mt-1"},[_c('div',{class:['progress-bar', _vm.progressBar.color],style:("width: " + _vm.progressBar.value + "%")})]),(_vm.removable)?_c('a',{staticClass:"mt-3 btn btn-danger",attrs:{"href":"#"},on:{"click":function($event){$event.preventDefault();_vm.$emit('remove')}}},[_vm._v("Удалить")]):_vm._e()]):_c('div',{staticClass:"card-progress mt-1"},[_c('div',{staticClass:"progress"},[_c('div',{class:['progress-bar', _vm.progressBar.color],style:("width: " + _vm.progressBar.value + "%")})])])}
__vue__options__.staticRenderFns = []

},{}],11:[function(require,module,exports){
;(function(){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    name: "CircleButton",
    props: {
        icon: String
    },
    computed: {
        buttonIcon: function buttonIcon() {
            return "fa-" + this.icon;
        }
    }
};
})()
if (module.exports.__esModule) module.exports = module.exports.default
var __vue__options__ = (typeof module.exports === "function"? module.exports.options: module.exports)
__vue__options__.render = function render () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"circle-button"},[_c('div',{staticClass:"fa",class:_vm.buttonIcon})])}
__vue__options__.staticRenderFns = []

},{}],12:[function(require,module,exports){
;(function(){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    name: "Spinner"
};
})()
if (module.exports.__esModule) module.exports = module.exports.default
var __vue__options__ = (typeof module.exports === "function"? module.exports.options: module.exports)
__vue__options__.render = function render () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"spinner"})}
__vue__options__.staticRenderFns = []

},{}],13:[function(require,module,exports){
;(function(){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    name: "Logout",
    beforeRouteEnter: function beforeRouteEnter(to, from, next) {
        next(function (vm) {
            vm.$http.get("/logout").then(function () {
                vm.$root.user = {};
                vm.$root.boards = {};
                vm.$root.loadComplete = false;
                vm.$router.push("/");
            });
        });
    }
};
})()
if (module.exports.__esModule) module.exports = module.exports.default
var __vue__options__ = (typeof module.exports === "function"? module.exports.options: module.exports)
__vue__options__.render = function render () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c("div")}
__vue__options__.staticRenderFns = []

},{}],14:[function(require,module,exports){
;(function(){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    name: "Admin",
    data: function data() {
        return {
            roles: Object,
            actions: {
                create: "Создавать",
                update: "Изменять",
                remove: "Удалять"
            },
            groups: {
                card: "Карточки",
                list: "Списки",
                board: "Доска"
            },
            roleNames: {
                owner: "Владелец",
                admin: "Администратор",
                user: "Пользователь",
                guest: "Гость"
            }
        };
    },

    computed: {
        user: function user() {
            return this.$root.user;
        }
    },
    methods: {
        reloadComponent: function reloadComponent() {
            var _this = this;

            this.$http.get("/rest/roles").then(function (response) {
                return _this.roles = response.data;
            });
        },
        saveRoles: function saveRoles() {
            this.$http.post("/rest/roles", this.roles);
        }
    },
    beforeRouteUpdate: function beforeRouteUpdate(to, from, next) {
        this.reloadComponent();
        next();
    },
    created: function created() {
        this.reloadComponent();
    }
};
})()
if (module.exports.__esModule) module.exports = module.exports.default
var __vue__options__ = (typeof module.exports === "function"? module.exports.options: module.exports)
__vue__options__.render = function render () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"container"},[_c('b-tabs',{attrs:{"pills":"pills","navClass":"mb-3"}},[_c('b-tab',{attrs:{"title":"Редактор ролей"}},[_vm._l((_vm.roles),function(role,roleIndex){return _c('b-card',{staticClass:"mb-3"},[_c('h2',[_vm._v(_vm._s(_vm.roleNames[role.name]))]),_vm._l((role.permissions),function(permission,permissionIndex){return _c('div',{staticClass:"permission-object"},[_c('div',{staticClass:"font-weight-bold mb-1"},[_vm._v(_vm._s(_vm.groups[permissionIndex]))]),_vm._l((permission),function(group,groupIndex){return _c('b-form-checkbox',{model:{value:(_vm.roles[roleIndex].permissions[permissionIndex][groupIndex]),callback:function ($$v) {_vm.$set(_vm.roles[roleIndex].permissions[permissionIndex], groupIndex, $$v)},expression:"roles[roleIndex].permissions[permissionIndex][groupIndex]"}},[_vm._v(_vm._s(_vm.actions[groupIndex]))])})],2)})],2)}),_c('b-btn',{staticClass:"mb-5",attrs:{"variant":"success"},on:{"click":_vm.saveRoles}},[_vm._v("Сохранить")])],2)],1)],1)}
__vue__options__.staticRenderFns = []

},{}],15:[function(require,module,exports){
;(function(){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    name: "board",
    components: {
        "CardModal": require("../components/CardModal.vue"),
        "BoardModal": require("../components/BoardModal.vue"),
        "AudorList": require("../components/AudorList.vue"),
        "AudorListForm": require("../components/AudorListForm.vue"),
        "Draggable": require("vuedraggable"),
        "Spinner": require("../components/Spinner.vue"),
        "CircleButton": require("../components/CircleButton.vue")
    },
    computed: {
        listDragState: function listDragState() {
            if (!this.$root.userRole.permissions) return true;
            return !this.$root.userRole.permissions.list.update;
        }
    },
    data: function data() {
        return {
            board: {},
            lists: [],
            labelColors: [],
            selectedCard: {},
            selectedList: {},
            listDraggableOptions: {
                group: "lists",
                animation: 300,
                filter: ".audor-list-form, input",
                preventOnFilter: true,
                disabled: this.$root.isMobilePhone || this.listDragState
            },
            loadComplete: false,
            boardModalVisible: false
        };
    },
    methods: {
        initialSort: function initialSort() {
            var _this = this;

            this.lists.sort(this.sortByOrder);
            this.lists.forEach(function (list) {
                return list.cards.sort(_this.sortByOrder);
            });
        },
        sortByOrder: function sortByOrder(left, right) {
            if (left.order > right.order) return 1;
            if (left.order < right.order) return -1;
            return 0;
        },
        createList: function createList(name) {
            var _this2 = this;

            var data = { name: name, order: this.lists.length };

            this.$http.post("/rest/boards/" + this.board._id + "/lists", data).then(function (response) {
                response.data.cards = [];
                _this2.lists.push(response.data);
            });
        },
        removeList: function removeList(index) {
            var _this3 = this;

            this.$http.delete("/rest/lists/" + this.lists[index]._id).then(function () {
                _this3.lists.splice(index, 1);
            });
        },
        updateCardsOrder: function updateCardsOrder() {
            this.lists.forEach(function (list) {
                list.cards = list.cards.map(function (card, index) {
                    card.order = index;
                    return card;
                });
            });

            var cardsOrder = this.lists.slice().map(function (list) {
                var cards = list.cards.map(function (card) {
                    return { id: card._id, order: card.order };
                });
                return { id: list.id, cards: cards };
            });

            this.$http.post("/rest/boards/" + this.board._id + "/cards/order", cardsOrder);
        },
        updateListsOrder: function updateListsOrder(event) {
            this.lists = this.lists.map(function (list, index) {
                list.order = index;
                return list;
            });

            var listsOrder = this.lists.map(function (list) {
                return { id: list.id, order: list.order };
            });

            this.$http.post("/rest/boards/" + this.board._id + "/lists/order", listsOrder);
        },
        showCardForm: function showCardForm(card) {
            var _this4 = this;

            this.selectedCard = card;

            this.lists.forEach(function (list) {
                if (list._id === card.list) _this4.selectedList = list;
            });

            this.$root.cardModalVisible = true;
        },
        reloadComponent: function reloadComponent(board) {
            var _this5 = this;

            this.loadComplete = false;
            this.$http.get("/rest/boards/" + board).then(function (response) {
                _this5.board = response.data;
                _this5.$root.currentBoard = _this5.board;
            });

            this.$http.get("/rest/boards/" + board + "/lists").then(function (response) {
                _this5.lists = response.data;
                _this5.initialSort();
                _this5.loadComplete = true;
            });
            this.$http.get("/rest/labels/colors").then(function (response) {
                return _this5.labelColors = response.data;
            });
        },

        updateBoard: function updateBoard() {
            this.$http.post("/rest/boards/" + this.board._id, this.board);
        },
        showBoardModal: function showBoardModal() {
            this.boardModalVisible = true;
        },
        dragstart: function dragstart(event) {
            console.log(event);
        },
        dragend: function dragend(event) {
            console.log(event);
        }
    },
    created: function created() {
        this.reloadComponent(this.$route.params.id);
        eventBus.$on("card-modal-toggle", this.showCardForm);
        eventBus.$on("card-move", this.updateCardsOrder);
    },
    beforeRouteUpdate: function beforeRouteUpdate(to, from, next) {
        this.reloadComponent(to.params.id);
        next();
    },
    beforeRouteLeave: function beforeRouteLeave(to, from, next) {
        this.$root.currentBoard = {};
        next();
    }
};
})()
if (module.exports.__esModule) module.exports = module.exports.default
var __vue__options__ = (typeof module.exports === "function"? module.exports.options: module.exports)
__vue__options__.render = function render () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return (_vm.$root.userRole.permissions)?_c('div',{staticClass:"board-page"},[(_vm.board.background != null)?_c('div',{staticClass:"background",style:({'background-image': 'url(' + _vm.board.background + ')'})},[_c('div',{staticClass:"blur-effect"})]):_vm._e(),(!_vm.loadComplete)?_c('Spinner'):_c('div',{ref:"board",staticClass:"board-page-wrapper"},[_c('div',{staticClass:"board-controls container-fluid"},[_c('div',{staticClass:"board-name",class:{'text-white': _vm.board.background}},[_vm._v(_vm._s(_vm.board.name))]),(_vm.$root.userRole.permissions.board.update)?_c('CircleButton',{attrs:{"icon":"cog"},nativeOn:{"click":function($event){return _vm.showBoardModal($event)}}}):_vm._e()],1),_c('div',{staticClass:"board-contents"},[_c('draggable',{staticClass:"board-contents-draggable",attrs:{"options":_vm.listDraggableOptions},on:{"end":[_vm.updateListsOrder,_vm.dragend],"start":_vm.dragstart},model:{value:(_vm.lists),callback:function ($$v) {_vm.lists=$$v},expression:"lists"}},_vm._l((_vm.lists),function(list,index){return _c('AudorList',{key:list._id,attrs:{"list":list},on:{"list-remove":function($event){_vm.removeList(index)},"card-edit":_vm.showCardForm}})})),(_vm.$root.userRole.permissions.list.create)?_c('AudorListForm',{on:{"list-create":_vm.createList}}):_vm._e()],1)]),_c('CardModal',{attrs:{"list":_vm.selectedList,"card":_vm.selectedCard,"label-colors":_vm.labelColors}}),_c('BoardModal')],1):_vm._e()}
__vue__options__.staticRenderFns = []

},{"../components/AudorList.vue":4,"../components/AudorListForm.vue":5,"../components/BoardModal.vue":7,"../components/CardModal.vue":9,"../components/CircleButton.vue":11,"../components/Spinner.vue":12,"vuedraggable":83}],16:[function(require,module,exports){
;(function(){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _require = require("vuelidate/lib/validators"),
    required = _require.required;

exports.default = {
    name: "boards",
    data: function data() {
        return {
            boardCreationForm: {
                name: ""
            }
        };
    },
    components: {
        "BoardTile": require("../components/BoardTile.vue"),
        "Spinner": require("../components/Spinner.vue")
    },
    validations: {
        boardCreationForm: {
            name: {
                required: required
            }
        }
    },
    computed: {
        loadComplete: function loadComplete() {
            return this.$root.loadComplete;
        },
        boards: function boards() {
            return this.$root.boards;
        }
    },
    methods: {
        getFieldState: function getFieldState(form, field) {
            if (!this.$v) return null;
            if (!this.$v[form][field].$dirty) return null;
            return !this.$v[form][field].$invalid;
        },
        boardCreate: function boardCreate() {
            var _this = this;

            this.$http.post("/rest/boards/", this.boardCreationForm).then(function (response) {
                _this.$root.boards.push(response.data);
            });
        },
        reloadComponent: function reloadComponent() {
            this.$root.loadBoards();
        }
    },
    created: function created() {
        this.reloadComponent();
    },
    beforeRouteUpdate: function beforeRouteUpdate(to, from, next) {
        this.reloadComponent();
        next();
    }
};
})()
if (module.exports.__esModule) module.exports = module.exports.default
var __vue__options__ = (typeof module.exports === "function"? module.exports.options: module.exports)
__vue__options__.render = function render () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"container"},[(!_vm.loadComplete)?_c('Spinner'):[_c('b-dropdown',{staticClass:"mb-3",attrs:{"variant":"info","text":"Создать","no-caret":"no-caret"}},[_c('template',{slot:"button-content"},[_c('i',{staticClass:"fa fa-plus"})]),_c('div',{staticClass:"dropdown-form",on:{"keydown":function($event){if(!('button' in $event)&&_vm._k($event.keyCode,"enter",13,$event.key,"Enter")){ return null; }return _vm.boardCreate($event)}}},[_c('b-form-group',[_c('b-form-input',{attrs:{"type":"text","state":_vm.getFieldState('boardCreationForm', 'name'),"placeholder":"Название доски"},on:{"input":function($event){_vm.$v.boardCreationForm.name.$touch()}},model:{value:(_vm.boardCreationForm.name),callback:function ($$v) {_vm.$set(_vm.boardCreationForm, "name", $$v)},expression:"boardCreationForm.name"}}),_c('b-form-invalid-feedback',[_vm._v("Название доски не может быть пустым")])],1),_c('b-form-group',[_c('b-btn',{attrs:{"block":"block","variant":"info","disabled":_vm.$v.boardCreationForm.$invalid},on:{"click":_vm.boardCreate}},[_vm._v("Создать")])],1)],1)],2),_c('div',{staticClass:"row"},_vm._l((_vm.boards),function(board){return _c('BoardTile',{key:board._id,attrs:{"board":board}})}))]],2)}
__vue__options__.staticRenderFns = []

},{"../components/BoardTile.vue":8,"../components/Spinner.vue":12,"vuelidate/lib/validators":91}],17:[function(require,module,exports){
;(function(){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});


var BeforeAfter = require("../components/BeforeAfter.vue");

var _require = require("vue-carousel-3d"),
    Carousel3d = _require.Carousel3d,
    Slide = _require.Slide;

var Spinner = require("../components/Spinner.vue");

exports.default = {
    name: "index",
    components: {
        BeforeAfter: BeforeAfter,
        Carousel3d: Carousel3d,
        Slide: Slide,
        Spinner: Spinner
    }
};
})()
if (module.exports.__esModule) module.exports = module.exports.default
var __vue__options__ = (typeof module.exports === "function"? module.exports.options: module.exports)
__vue__options__.render = function render () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"index-page"},[_c('section',{staticClass:"home",attrs:{"id":"home"}},[_c('b-navbar',{attrs:{"toggleable":"md","type":"dark"}},[_c('div',{staticClass:"container"},[_c('b-navbar-brand',[_c('img',{attrs:{"src":"/images/audor-logo-white.svg","alt":"audor-logo","height":"35"}})]),_c('b-navbar-toggle',{staticClass:"ml-auto",attrs:{"target":"nav_collapse"}}),_c('b-collapse',{attrs:{"is-nav":"is-nav","id":"nav_collapse"}},[_c('b-navbar-nav',{staticClass:"ml-auto"},[_c('b-nav-item',{attrs:{"to":"/login"}},[_vm._v("Войти")]),_c('b-nav-item',{attrs:{"to":"/register"}},[_vm._v("Зарегистрироваться")])],1)],1)],1)]),_c('div',{staticClass:"container home-contents"},[_c('div',{staticClass:"row"},[_c('div',{staticClass:"col-md-12 col-xl-7 text-center text-xl-left"},[_c('h1',{staticClass:"mb-default"},[_vm._v("Что такое Project Audor")]),_c('div',{staticClass:"lead mb-default"},[_vm._v("Это революционное решение для визуализации рабочего процесса,которое может подстраиваться под ваши нужды, и будет\nполностью соответствовать вашим желаниям")]),_c('b-btn',{staticClass:"btn-outline-light",attrs:{"to":"/register"}},[_vm._v("Попробовать")])],1),_vm._m(0)])])],1),_vm._m(1),_c('section',{staticClass:"themes",attrs:{"id":"themes"}},[_c('div',{staticClass:"container d-flex align-items-center flex-column"},[_c('h2',[_vm._v("Две стороны")]),_c('div',{staticClass:"text-muted text-center"},[_vm._v("Выбери свою")]),_c('img',{staticClass:"slide-icon",attrs:{"src":"/images/slide-icon.svg","alt":"slide-icon"}}),_c('BeforeAfter',{attrs:{"before":"/images/theme-light.png","after":"/images/theme-dark.png"}}),_c('p',[_vm._v("Светлая, а также темная тема, для тех кто любит поработать ночью ;)")])],1)]),_c('section',{staticClass:"pricing",attrs:{"id":"pricing"}},[_c('div',{staticClass:"container"},[_c('h2',[_vm._v("Цены")]),_c('div',{staticClass:"text-muted text-center"},[_vm._v("На любой вкус")]),_c('div',{staticClass:"d-flex justify-content-center"},[_c('div',{staticClass:"pricing-card"},[_c('div',{staticClass:"pricing-header"},[_vm._v("Бесплатно")]),_vm._m(2),_c('div',{staticClass:"price-description text-muted"},[_vm._v("Да, да, именно так. Абсолютно и полностью бесплатно")]),_c('div',{staticClass:"btn-wrapper"},[_c('b-btn',{staticClass:"btn-outline-light",attrs:{"to":"/register"}},[_vm._v("Попробовать")])],1)])])])]),_vm._m(3),_c('section',{staticClass:"gallery"},[_c('div',{staticClass:"container"},[_c('h2',[_vm._v("Галерея")]),_c('div',{staticClass:"text-muted text-center"},[_vm._v("Крутые примеры")]),_c('carousel-3d',{attrs:{"height":640,"perspective":0}},[_c('slide',{attrs:{"index":0}},[_c('img',{attrs:{"src":"/images/slides/1.png"}})]),_c('slide',{attrs:{"index":1}},[_c('img',{attrs:{"src":"/images/slides/2.png"}})]),_c('slide',{attrs:{"index":2}},[_c('img',{attrs:{"src":"/images/slides/3.png"}})])],1)],1)]),_vm._m(4)])}
__vue__options__.staticRenderFns = [function render () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"col-md-12 col-xl-5 laptop-mockup"},[_c('img',{staticClass:"img-fluid",attrs:{"src":"/images/laptop-mockup.png","alt":"laptop-mockup"}})])},function render () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('section',{staticClass:"features",attrs:{"id":"features"}},[_c('div',{staticClass:"container"},[_c('h2',[_vm._v("Особенности")]),_c('div',{staticClass:"row"},[_c('div',{staticClass:"feature-block col-lg-4 col-md-6"},[_c('div',{staticClass:"feature-icon"},[_c('div',{staticClass:"fa fa-users"})]),_c('div',{staticClass:"feature-header"},[_vm._v("Работа в команде")]),_c('div',{staticClass:"feature-description"},[_vm._v("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tempus porttitor erat, eget dictum tellus pellentesque id. Cras ac dolor nibh. Aliquam non gravida risus.")])]),_c('div',{staticClass:"feature-block col-lg-4 col-md-6"},[_c('div',{staticClass:"feature-icon"},[_c('div',{staticClass:"fa fa-mobile-alt"})]),_c('div',{staticClass:"feature-header"},[_vm._v("Адаптивный дизайн")]),_c('div',{staticClass:"feature-description"},[_vm._v("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tempus porttitor erat, eget dictum tellus pellentesque id. Cras ac dolor nibh. Aliquam non gravida risus.")])]),_c('div',{staticClass:"feature-block col-lg-4 col-md-6"},[_c('div',{staticClass:"feature-icon"},[_c('div',{staticClass:"fa fa-upload"})]),_c('div',{staticClass:"feature-header"},[_vm._v("Возможность загрузки файлов")]),_c('div',{staticClass:"feature-description"},[_vm._v("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tempus porttitor erat, eget dictum tellus pellentesque id. Cras ac dolor nibh. Aliquam non gravida risus.")])]),_c('div',{staticClass:"feature-block col-lg-4 col-md-6"},[_c('div',{staticClass:"feature-icon"},[_c('div',{staticClass:"fa fa-bell"})]),_c('div',{staticClass:"feature-header"},[_vm._v("Оповещения")]),_c('div',{staticClass:"feature-description"},[_vm._v("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tempus porttitor erat, eget dictum tellus pellentesque id. Cras ac dolor nibh. Aliquam non gravida risus.")])]),_c('div',{staticClass:"feature-block col-lg-4 col-md-6"},[_c('div',{staticClass:"feature-icon"},[_c('div',{staticClass:"fa fa-cogs"})]),_c('div',{staticClass:"feature-header"},[_vm._v("Персонализация")]),_c('div',{staticClass:"feature-description"},[_vm._v("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tempus porttitor erat, eget dictum tellus pellentesque id. Cras ac dolor nibh. Aliquam non gravida risus.")])]),_c('div',{staticClass:"feature-block col-lg-4 col-md-6"},[_c('div',{staticClass:"feature-icon"},[_c('div',{staticClass:"fa fa-clock"})]),_c('div',{staticClass:"feature-header"},[_vm._v("Отслеживание времени")]),_c('div',{staticClass:"feature-description"},[_vm._v("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tempus porttitor erat, eget dictum tellus pellentesque id. Cras ac dolor nibh. Aliquam non gravida risus.")])]),_c('div',{staticClass:"feature-block col-lg-4 col-md-6"},[_c('div',{staticClass:"feature-icon"},[_c('div',{staticClass:"fa fa-chart-bar"})]),_c('div',{staticClass:"feature-header"},[_vm._v("Статистика и аналитка")]),_c('div',{staticClass:"feature-description"},[_vm._v("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tempus porttitor erat, eget dictum tellus pellentesque id. Cras ac dolor nibh. Aliquam non gravida risus.")])]),_c('div',{staticClass:"feature-block col-lg-4 col-md-6"},[_c('div',{staticClass:"feature-icon"},[_c('div',{staticClass:"fa fa-wrench"})]),_c('div',{staticClass:"feature-header"},[_vm._v("Наличие API для разработчиков")]),_c('div',{staticClass:"feature-description"},[_vm._v("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tempus porttitor erat, eget dictum tellus pellentesque id. Cras ac dolor nibh. Aliquam non gravida risus.")])]),_c('div',{staticClass:"feature-block col-lg-4 col-md-6"},[_c('div',{staticClass:"feature-icon"},[_c('div',{staticClass:"fa fa-hand-holding-usd"})]),_c('div',{staticClass:"feature-header"},[_vm._v("Абсолютно бесплатно")]),_c('div',{staticClass:"feature-description"},[_vm._v("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tempus porttitor erat, eget dictum tellus pellentesque id. Cras ac dolor nibh. Aliquam non gravida risus.")])])])])])},function render () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"price"},[_vm._v("0$"),_c('span',{staticClass:"price-addon"},[_vm._v(" месяц")])])},function render () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('section',{staticClass:"tech",attrs:{"id":"tech"}},[_c('div',{staticClass:"container"},[_c('h2',[_vm._v("Технологии")]),_c('div',{staticClass:"text-muted text-center"},[_vm._v("На кромке лезвия")]),_c('div',{staticClass:"images-container"},[_c('img',{attrs:{"src":"/images/tech/vuejs.svg","height":"75"}}),_c('img',{attrs:{"src":"/images/tech/nodejs.svg","height":"75"}}),_c('img',{attrs:{"src":"/images/tech/mongodb.svg","height":"75"}}),_c('img',{attrs:{"src":"/images/tech/bootstrap.svg","height":"75"}}),_c('img',{attrs:{"src":"/images/tech/express.svg","height":"75"}})])])])},function render () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('section',{staticClass:"footer"},[_c('div',{staticClass:"container"},[_c('p',[_vm._v("Project Audor 2017")]),_c('p',[_vm._v("by Vlad Maidov")])])])}]

},{"../components/BeforeAfter.vue":6,"../components/Spinner.vue":12,"vue-carousel-3d":82}],18:[function(require,module,exports){
;(function(){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _require = require("vuelidate/lib/validators"),
    required = _require.required;

exports.default = {
    name: "login",
    data: function data() {
        return {
            login: "",
            password: "",
            error: ""
        };
    },

    validations: {
        login: {
            required: required
        },
        password: {
            required: required
        }
    },
    methods: {
        getFieldState: function getFieldState(field) {
            if (!this.$v) return null;
            if (!this.$v[field].$dirty) return null;
            return !this.$v[field].$invalid;
        },
        attemptLogin: function attemptLogin() {
            var _this = this;

            if (this.$v.$invalid) return;
            this.error = "";

            this.$http.post("/login", this.$data).then(function (response) {
                if (response.data.error) {
                    _this.error = response.data.error;
                    return;
                }

                _this.$root.user = response.data;
                _this.$router.push("/boards");
            });
        }
    },
    beforeRouteUpdate: function beforeRouteUpdate(to, from, next) {
        console.log("test");
        if (this.$root.user.login) return next("/boards");
        next();
    }
};
})()
if (module.exports.__esModule) module.exports = module.exports.default
var __vue__options__ = (typeof module.exports === "function"? module.exports.options: module.exports)
__vue__options__.render = function render () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"auth-page container d-flex flex-column justify-content-center"},[_c('div',{staticClass:"auth-form"},[_c('form',[(_vm.error)?_c('b-alert',{attrs:{"variant":"danger","show":"show"}},[_vm._v(_vm._s(_vm.error))]):_vm._e(),_c('b-form-group',[_c('b-input-group',[_c('b-input-group-text',{staticClass:"d-none d-md-block",attrs:{"slot":"prepend"},slot:"prepend"},[_c('span',{staticClass:"fa fa-fw fa-user"})]),_c('b-form-input',{attrs:{"type":"text","state":_vm.getFieldState('login'),"placeholder":"Логин"},on:{"input":function($event){_vm.$v.login.$touch()}},model:{value:(_vm.login),callback:function ($$v) {_vm.login=$$v},expression:"login"}}),_c('b-form-invalid-feedback',[_vm._v("Поле не может быть пустым")])],1)],1),_c('b-form-group',[_c('b-input-group',[_c('b-input-group-text',{staticClass:"d-none d-md-block",attrs:{"slot":"prepend"},slot:"prepend"},[_c('span',{staticClass:"fa fa-fw fa-key"})]),_c('b-form-input',{attrs:{"type":"password","state":_vm.getFieldState('password'),"placeholder":"Пароль"},on:{"input":function($event){_vm.$v.password.$touch()}},model:{value:(_vm.password),callback:function ($$v) {_vm.password=$$v},expression:"password"}}),_c('b-form-invalid-feedback',[_vm._v("Поле не может быть пустым")])],1)],1),_c('b-form-group',[_c('b-btn',{staticClass:"form-control",attrs:{"block":"block","variant":"success","disabled":_vm.$v.$invalid},on:{"click":_vm.attemptLogin}},[_vm._v("Войти")])],1),_c('hr'),_c('b-form-group',{staticClass:"text-center"},[_c('b-link',{attrs:{"to":"/register"}},[_vm._v("Зарегистрироваться")])],1)],1)])])}
__vue__options__.staticRenderFns = []

},{"vuelidate/lib/validators":91}],19:[function(require,module,exports){
;(function(){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _require = require("vuelidate/lib/validators"),
    required = _require.required,
    sameAs = _require.sameAs,
    alphaNum = _require.alphaNum;

exports.default = {
    name: "register",
    data: function data() {
        return {
            login: "",
            password: "",
            password_repeat: "",
            error: ""
        };
    },

    validations: {
        login: {
            required: required,
            alphaNum: alphaNum
        },
        password: {
            required: required
        },
        password_repeat: {
            required: required,
            sameAs: sameAs("password")
        }
    },
    methods: {
        getFieldState: function getFieldState(field) {
            if (!this.$v) return null;
            if (!this.$v[field].$dirty) return null;
            return !this.$v[field].$invalid;
        },
        attemptRegister: function attemptRegister() {
            var _this = this;

            if (this.$v.$invalid) return;
            this.error = "";
            this.$http.post("/register", this.$data).then(function (response) {
                if (response.data.error) {
                    _this.error = response.data.error;
                    return;
                }

                _this.$root.user = response.data;
                _this.$router.push("/boards");
            });
        }
    },
    beforeRouteUpdate: function beforeRouteUpdate(to, from, next) {
        if (this.$root.user.login) return next("/boards");
        return next();
    }
};
})()
if (module.exports.__esModule) module.exports = module.exports.default
var __vue__options__ = (typeof module.exports === "function"? module.exports.options: module.exports)
__vue__options__.render = function render () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"auth-page container d-flex flex-column justify-content-center"},[_c('form',{staticClass:"auth-form"},[(_vm.error)?_c('b-alert',{attrs:{"variant":"danger","show":"show"}},[_vm._v(_vm._s(_vm.error))]):_vm._e(),_c('b-form-group',[_c('b-input-group',[_c('b-input-group-text',{staticClass:"d-none d-md-block",attrs:{"slot":"prepend"},slot:"prepend"},[_c('span',{staticClass:"fa fa-fw fa-user"})]),_c('b-form-input',{attrs:{"type":"text","state":_vm.getFieldState('login'),"placeholder":"Логин"},on:{"input":function($event){_vm.$v.login.$touch()}},model:{value:(_vm.login),callback:function ($$v) {_vm.login=$$v},expression:"login"}}),(!_vm.$v.login.required)?_c('b-form-invalid-feedback',[_vm._v("Поле не может быть пустым")]):_vm._e(),(!_vm.$v.login.alphaNum)?_c('b-form-invalid-feedback',[_vm._v("Поле может содержать только латиницу и цифры")]):_vm._e()],1)],1),_c('b-form-group',[_c('b-input-group',[_c('b-input-group-text',{staticClass:"d-none d-md-block",attrs:{"slot":"prepend"},slot:"prepend"},[_c('span',{staticClass:"fa fa-fw fa-key"})]),_c('b-form-input',{attrs:{"type":"password","state":_vm.getFieldState('password'),"placeholder":"Пароль"},on:{"input":function($event){_vm.$v.password.$touch()}},model:{value:(_vm.password),callback:function ($$v) {_vm.password=$$v},expression:"password"}}),_c('b-form-invalid-feedback',[_vm._v("Поле не может быть пустым")])],1)],1),_c('b-form-group',[_c('b-input-group',[_c('b-input-group-text',{staticClass:"d-none d-md-block",attrs:{"slot":"prepend"},slot:"prepend"},[_c('span',{staticClass:"fa fa-fw fa-key"})]),_c('b-form-input',{attrs:{"type":"password","state":_vm.getFieldState('password_repeat'),"placeholder":"Пароль (еще раз)"},on:{"input":function($event){_vm.$v.password_repeat.$touch()}},model:{value:(_vm.password_repeat),callback:function ($$v) {_vm.password_repeat=$$v},expression:"password_repeat"}}),(!_vm.$v.password_repeat.required)?_c('b-form-invalid-feedback',[_vm._v("Поле не может быть пустым")]):_vm._e(),(!_vm.$v.password_repeat.sameAs)?_c('b-form-invalid-feedback',[_vm._v("Проверочный пароль не совпадает")]):_vm._e()],1)],1),_c('b-form-group',[_c('b-btn',{attrs:{"block":"block","variant":"success","disabled":_vm.$v.$invalid},on:{"click":_vm.attemptRegister}},[_vm._v("Зарегистрироваться")])],1),_c('hr'),_c('b-form-group',{staticClass:"text-center"},[_c('b-link',{attrs:{"to":"/login"}},[_vm._v("Войти")])],1)],1)])}
__vue__options__.staticRenderFns = []

},{"vuelidate/lib/validators":91}],20:[function(require,module,exports){
;(function(){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _require = require("vuelidate/lib/validators"),
    required = _require.required,
    sameAs = _require.sameAs;

exports.default = {
    name: "settings",
    data: function data() {
        return {
            currentTab: 0,
            passwordChangeForm: {}
        };
    },

    validations: {
        passwordChangeForm: {
            old_password: {
                required: required
            },
            new_password: {
                required: required
            },
            new_password_repeat: {
                required: required,
                sameAs: sameAs("new_password")
            }
        }
    },
    computed: {
        settings: function settings() {
            return this.$parent.user;
        }
    },
    methods: {
        getFieldState: function getFieldState(form, field) {
            if (!this.$v) return null;

            if (!field) {
                if (!this.$v[form].$dirty) return null;
                return !this.$v[form].$invalid;
            }

            if (!this.$v[form][field].$dirty) return null;
            return !this.$v[form][field].$invalid;
        },

        updateSettings: function updateSettings() {
            this.$http.post("/settings", this.settings);
        },
        uploadPhoto: function uploadPhoto(event) {
            var _this = this;

            var formData = new FormData(event.target);
            this.$http.post("/settings", formData).then(function (response) {
                _this.$set(_this.settings, "photo", response.data.photo);
            });
        },
        changePassword: function changePassword() {
            this.$http.post("/settings/password", this.passwordChangeForm).then(function (response) {});
        }
    }
};
})()
if (module.exports.__esModule) module.exports = module.exports.default
var __vue__options__ = (typeof module.exports === "function"? module.exports.options: module.exports)
__vue__options__.render = function render () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"container mt-3 mb-3"},[_c('b-tabs',{attrs:{"pills":"pills","vertical":"vertical","nav-wrapper-class":"mb-3 col-12 col-lg-3","content-class":"col-12 col-sm-12 col-lg-5"}},[_c('b-tab',{attrs:{"title":"Основные"}},[_c('form',{on:{"submit":function($event){$event.preventDefault();return _vm.updateSettings($event)}}},[_c('div',{staticClass:"form-group"},[_c('label',[_vm._v("Имя (псевдоним)")]),_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.settings.name),expression:"settings.name"}],staticClass:"form-control",attrs:{"type":"text","name":"name"},domProps:{"value":(_vm.settings.name)},on:{"input":function($event){if($event.target.composing){ return; }_vm.$set(_vm.settings, "name", $event.target.value)}}})]),_c('div',{staticClass:"form-group"},[_c('label',[_vm._v("О себе")]),_c('textarea',{directives:[{name:"model",rawName:"v-model",value:(_vm.settings.about),expression:"settings.about"}],staticClass:"form-control",attrs:{"name":"about"},domProps:{"value":(_vm.settings.about)},on:{"input":function($event){if($event.target.composing){ return; }_vm.$set(_vm.settings, "about", $event.target.value)}}})]),_c('div',{staticClass:"form-group"},[_c('label',[_vm._v("Email")]),_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.settings.email),expression:"settings.email"}],staticClass:"form-control",attrs:{"type":"email","name":"email"},domProps:{"value":(_vm.settings.email)},on:{"input":function($event){if($event.target.composing){ return; }_vm.$set(_vm.settings, "email", $event.target.value)}}})]),_c('div',{staticClass:"form-group"},[_c('label',[_vm._v("Веб-сайт")]),_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.settings.website),expression:"settings.website"}],staticClass:"form-control",attrs:{"type":"text","name":"website"},domProps:{"value":(_vm.settings.website)},on:{"input":function($event){if($event.target.composing){ return; }_vm.$set(_vm.settings, "website", $event.target.value)}}})]),_c('div',{staticClass:"form-group"},[_c('input',{staticClass:"btn btn-success",attrs:{"type":"submit","value":"Сохранить"}})])])]),_c('b-tab',{attrs:{"title":"Фотография"}},[_c('form',{on:{"submit":function($event){$event.preventDefault();return _vm.uploadPhoto($event)}}},[_c('div',{staticClass:"form-group"},[_c('div',{staticClass:"img-square-circle mb-3"},[_c('img',{attrs:{"src":_vm.settings.photo || "/images/200x200.png"}})]),_c('input',{staticClass:"form-control",attrs:{"type":"file","name":"photo"}})]),_c('div',{staticClass:"form-group"},[_c('input',{staticClass:"btn btn-success",attrs:{"type":"submit","value":"Загрузить"}})])])]),_c('b-tab',{attrs:{"title":"Внешний вид"}},[(_vm.settings.prefs)?_c('form',{on:{"submit":function($event){$event.preventDefault();return _vm.updateSettings($event)}}},[_c('div',{staticClass:"form-group"},[_c('label',[_vm._v("Тема")]),_c('select',{directives:[{name:"model",rawName:"v-model",value:(_vm.settings.prefs.theme),expression:"settings.prefs.theme"}],staticClass:"form-control",on:{"change":function($event){var $$selectedVal = Array.prototype.filter.call($event.target.options,function(o){return o.selected}).map(function(o){var val = "_value" in o ? o._value : o.value;return val}); _vm.$set(_vm.settings.prefs, "theme", $event.target.multiple ? $$selectedVal : $$selectedVal[0])}}},[_c('option',{attrs:{"value":"light"}},[_vm._v("Светлая")]),_c('option',{attrs:{"value":"dark"}},[_vm._v("Темная")])])]),_c('div',{staticClass:"form-group"},[_c('input',{staticClass:"btn btn-success",attrs:{"type":"submit","value":"Сохранить"}})])]):_vm._e()]),_c('b-tab',{attrs:{"title":"Смена пароля"}},[_c('form',[_c('b-form-group',[_c('label',[_vm._v("Старый пароль")]),_c('b-form-input',{attrs:{"type":"password","state":_vm.getFieldState('passwordChangeForm', 'old_password')},on:{"input":function($event){_vm.$v.passwordChangeForm.old_password.$touch()}},model:{value:(_vm.passwordChangeForm.old_password),callback:function ($$v) {_vm.$set(_vm.passwordChangeForm, "old_password", $$v)},expression:"passwordChangeForm.old_password"}}),_c('b-form-invalid-feedback',[_vm._v("Поле не может быть пустым")])],1),_c('b-form-group',[_c('label',[_vm._v("Новый пароль")]),_c('b-form-input',{attrs:{"type":"password","state":_vm.getFieldState('passwordChangeForm', 'new_password')},on:{"input":function($event){_vm.$v.passwordChangeForm.new_password.$touch()}},model:{value:(_vm.passwordChangeForm.new_password),callback:function ($$v) {_vm.$set(_vm.passwordChangeForm, "new_password", $$v)},expression:"passwordChangeForm.new_password"}}),_c('b-form-invalid-feedback',[_vm._v("Поле не может быть пустым")])],1),_c('b-form-group',[_c('label',[_vm._v("Новый пароль (еще раз)")]),_c('b-form-input',{attrs:{"type":"password","state":_vm.getFieldState('passwordChangeForm', 'new_password_repeat')},on:{"input":function($event){_vm.$v.passwordChangeForm.new_password_repeat.$touch()}},model:{value:(_vm.passwordChangeForm.new_password_repeat),callback:function ($$v) {_vm.$set(_vm.passwordChangeForm, "new_password_repeat", $$v)},expression:"passwordChangeForm.new_password_repeat"}}),(!_vm.$v.passwordChangeForm.new_password_repeat.required)?_c('b-form-invalid-feedback',[_vm._v("Поле не может быть пустым")]):_vm._e(),(!_vm.$v.passwordChangeForm.new_password_repeat.sameAs)?_c('b-form-invalid-feedback',[_vm._v("Проверочный пароль не совпадает")]):_vm._e()],1),_c('b-form-group',[_c('b-button',{attrs:{"variant":"success","disabled":_vm.$v.passwordChangeForm.$invalid},on:{"click":_vm.changePassword}},[_vm._v("Сохранить")])],1)],1)]),_c('b-tab',{attrs:{"title":"Языковые настройки"}})],1)],1)}
__vue__options__.staticRenderFns = []

},{"vuelidate/lib/validators":91}],21:[function(require,module,exports){
;(function(){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    name: "user",
    data: function data() {
        return {
            user: {},
            loadComplete: false
        };
    },
    computed: {
        photoOrPlaceholder: function photoOrPlaceholder() {
            return this.user.photo || "/images/200x200.png";
        }
    },
    methods: {
        reloadComponent: function reloadComponent(user) {
            var _this = this;

            this.loadComplete = false;
            this.$http.get("/rest/users/" + user).then(function (response) {
                _this.user = response.data;
                _this.loadComplete = true;
            });
        }
    },
    created: function created() {
        this.reloadComponent(this.$route.params.id);
    },
    beforeRouteUpdate: function beforeRouteUpdate(to, from, next) {
        this.reloadComponent(to.params.id);
        next();
    }
};
})()
if (module.exports.__esModule) module.exports = module.exports.default
var __vue__options__ = (typeof module.exports === "function"? module.exports.options: module.exports)
__vue__options__.render = function render () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"container"},[(!_vm.loadComplete)?_c('div',{staticClass:"spinner"},[_c('span',{staticClass:"fa fa-spinner fa-spin fa-3x"})]):_c('div',{staticClass:"col-lg-12"},[_c('div',{staticClass:"img-square-circle mb-3"},[_c('img',{attrs:{"src":_vm.photoOrPlaceholder}})]),(!_vm.user.name)?_c('h3',[_vm._v(_vm._s(_vm.user.login))]):_c('div',{staticClass:"text-center"},[_c('h3',[_vm._v(_vm._s(_vm.user.name))]),_c('h6',{staticClass:"text-muted"},[_vm._v("("+_vm._s(_vm.user.login)+")")])]),(_vm.user.about || _vm.user.website || _vm.user.email)?_c('div',{staticClass:"border rounded p-3 mt-3"},[(_vm.user.about)?_c('div',{staticClass:"d-block"},[_c('span',{staticClass:"fa fa-info fa-fw mr-2"}),_c('span',[_vm._v(_vm._s(_vm.user.about))])]):_vm._e(),(_vm.user.website)?_c('div',{staticClass:"d-block"},[_c('span',{staticClass:"fa fa-globe fa-fw mr-2"}),_c('a',{attrs:{"href":'http://' + _vm.user.website}},[_vm._v(_vm._s(_vm.user.website))])]):_vm._e(),(_vm.user.email)?_c('div',{staticClass:"d-block"},[_c('span',{staticClass:"fa fa-envelope fa-fw mr-2"}),_c('span',[_vm._v(_vm._s(_vm.user.email))])]):_vm._e()]):_vm._e()])])}
__vue__options__.staticRenderFns = []

},{}],22:[function(require,module,exports){
module.exports = { "default": require("core-js/library/fn/get-iterator"), __esModule: true };
},{"core-js/library/fn/get-iterator":24}],23:[function(require,module,exports){
module.exports = { "default": require("core-js/library/fn/object/assign"), __esModule: true };
},{"core-js/library/fn/object/assign":25}],24:[function(require,module,exports){
require('../modules/web.dom.iterable');
require('../modules/es6.string.iterator');
module.exports = require('../modules/core.get-iterator');

},{"../modules/core.get-iterator":76,"../modules/es6.string.iterator":79,"../modules/web.dom.iterable":80}],25:[function(require,module,exports){
require('../../modules/es6.object.assign');
module.exports = require('../../modules/_core').Object.assign;

},{"../../modules/_core":32,"../../modules/es6.object.assign":78}],26:[function(require,module,exports){
module.exports = function (it) {
  if (typeof it != 'function') throw TypeError(it + ' is not a function!');
  return it;
};

},{}],27:[function(require,module,exports){
module.exports = function () { /* empty */ };

},{}],28:[function(require,module,exports){
var isObject = require('./_is-object');
module.exports = function (it) {
  if (!isObject(it)) throw TypeError(it + ' is not an object!');
  return it;
};

},{"./_is-object":46}],29:[function(require,module,exports){
// false -> Array#indexOf
// true  -> Array#includes
var toIObject = require('./_to-iobject');
var toLength = require('./_to-length');
var toAbsoluteIndex = require('./_to-absolute-index');
module.exports = function (IS_INCLUDES) {
  return function ($this, el, fromIndex) {
    var O = toIObject($this);
    var length = toLength(O.length);
    var index = toAbsoluteIndex(fromIndex, length);
    var value;
    // Array#includes uses SameValueZero equality algorithm
    // eslint-disable-next-line no-self-compare
    if (IS_INCLUDES && el != el) while (length > index) {
      value = O[index++];
      // eslint-disable-next-line no-self-compare
      if (value != value) return true;
    // Array#indexOf ignores holes, Array#includes - not
    } else for (;length > index; index++) if (IS_INCLUDES || index in O) {
      if (O[index] === el) return IS_INCLUDES || index || 0;
    } return !IS_INCLUDES && -1;
  };
};

},{"./_to-absolute-index":67,"./_to-iobject":69,"./_to-length":70}],30:[function(require,module,exports){
// getting tag from 19.1.3.6 Object.prototype.toString()
var cof = require('./_cof');
var TAG = require('./_wks')('toStringTag');
// ES3 wrong here
var ARG = cof(function () { return arguments; }()) == 'Arguments';

// fallback for IE11 Script Access Denied error
var tryGet = function (it, key) {
  try {
    return it[key];
  } catch (e) { /* empty */ }
};

module.exports = function (it) {
  var O, T, B;
  return it === undefined ? 'Undefined' : it === null ? 'Null'
    // @@toStringTag case
    : typeof (T = tryGet(O = Object(it), TAG)) == 'string' ? T
    // builtinTag case
    : ARG ? cof(O)
    // ES3 arguments fallback
    : (B = cof(O)) == 'Object' && typeof O.callee == 'function' ? 'Arguments' : B;
};

},{"./_cof":31,"./_wks":74}],31:[function(require,module,exports){
var toString = {}.toString;

module.exports = function (it) {
  return toString.call(it).slice(8, -1);
};

},{}],32:[function(require,module,exports){
var core = module.exports = { version: '2.5.1' };
if (typeof __e == 'number') __e = core; // eslint-disable-line no-undef

},{}],33:[function(require,module,exports){
// optional / simple context binding
var aFunction = require('./_a-function');
module.exports = function (fn, that, length) {
  aFunction(fn);
  if (that === undefined) return fn;
  switch (length) {
    case 1: return function (a) {
      return fn.call(that, a);
    };
    case 2: return function (a, b) {
      return fn.call(that, a, b);
    };
    case 3: return function (a, b, c) {
      return fn.call(that, a, b, c);
    };
  }
  return function (/* ...args */) {
    return fn.apply(that, arguments);
  };
};

},{"./_a-function":26}],34:[function(require,module,exports){
// 7.2.1 RequireObjectCoercible(argument)
module.exports = function (it) {
  if (it == undefined) throw TypeError("Can't call method on  " + it);
  return it;
};

},{}],35:[function(require,module,exports){
// Thank's IE8 for his funny defineProperty
module.exports = !require('./_fails')(function () {
  return Object.defineProperty({}, 'a', { get: function () { return 7; } }).a != 7;
});

},{"./_fails":39}],36:[function(require,module,exports){
var isObject = require('./_is-object');
var document = require('./_global').document;
// typeof document.createElement is 'object' in old IE
var is = isObject(document) && isObject(document.createElement);
module.exports = function (it) {
  return is ? document.createElement(it) : {};
};

},{"./_global":40,"./_is-object":46}],37:[function(require,module,exports){
// IE 8- don't enum bug keys
module.exports = (
  'constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf'
).split(',');

},{}],38:[function(require,module,exports){
var global = require('./_global');
var core = require('./_core');
var ctx = require('./_ctx');
var hide = require('./_hide');
var PROTOTYPE = 'prototype';

var $export = function (type, name, source) {
  var IS_FORCED = type & $export.F;
  var IS_GLOBAL = type & $export.G;
  var IS_STATIC = type & $export.S;
  var IS_PROTO = type & $export.P;
  var IS_BIND = type & $export.B;
  var IS_WRAP = type & $export.W;
  var exports = IS_GLOBAL ? core : core[name] || (core[name] = {});
  var expProto = exports[PROTOTYPE];
  var target = IS_GLOBAL ? global : IS_STATIC ? global[name] : (global[name] || {})[PROTOTYPE];
  var key, own, out;
  if (IS_GLOBAL) source = name;
  for (key in source) {
    // contains in native
    own = !IS_FORCED && target && target[key] !== undefined;
    if (own && key in exports) continue;
    // export native or passed
    out = own ? target[key] : source[key];
    // prevent global pollution for namespaces
    exports[key] = IS_GLOBAL && typeof target[key] != 'function' ? source[key]
    // bind timers to global for call from export context
    : IS_BIND && own ? ctx(out, global)
    // wrap global constructors for prevent change them in library
    : IS_WRAP && target[key] == out ? (function (C) {
      var F = function (a, b, c) {
        if (this instanceof C) {
          switch (arguments.length) {
            case 0: return new C();
            case 1: return new C(a);
            case 2: return new C(a, b);
          } return new C(a, b, c);
        } return C.apply(this, arguments);
      };
      F[PROTOTYPE] = C[PROTOTYPE];
      return F;
    // make static versions for prototype methods
    })(out) : IS_PROTO && typeof out == 'function' ? ctx(Function.call, out) : out;
    // export proto methods to core.%CONSTRUCTOR%.methods.%NAME%
    if (IS_PROTO) {
      (exports.virtual || (exports.virtual = {}))[key] = out;
      // export proto methods to core.%CONSTRUCTOR%.prototype.%NAME%
      if (type & $export.R && expProto && !expProto[key]) hide(expProto, key, out);
    }
  }
};
// type bitmap
$export.F = 1;   // forced
$export.G = 2;   // global
$export.S = 4;   // static
$export.P = 8;   // proto
$export.B = 16;  // bind
$export.W = 32;  // wrap
$export.U = 64;  // safe
$export.R = 128; // real proto method for `library`
module.exports = $export;

},{"./_core":32,"./_ctx":33,"./_global":40,"./_hide":42}],39:[function(require,module,exports){
module.exports = function (exec) {
  try {
    return !!exec();
  } catch (e) {
    return true;
  }
};

},{}],40:[function(require,module,exports){
// https://github.com/zloirock/core-js/issues/86#issuecomment-115759028
var global = module.exports = typeof window != 'undefined' && window.Math == Math
  ? window : typeof self != 'undefined' && self.Math == Math ? self
  // eslint-disable-next-line no-new-func
  : Function('return this')();
if (typeof __g == 'number') __g = global; // eslint-disable-line no-undef

},{}],41:[function(require,module,exports){
var hasOwnProperty = {}.hasOwnProperty;
module.exports = function (it, key) {
  return hasOwnProperty.call(it, key);
};

},{}],42:[function(require,module,exports){
var dP = require('./_object-dp');
var createDesc = require('./_property-desc');
module.exports = require('./_descriptors') ? function (object, key, value) {
  return dP.f(object, key, createDesc(1, value));
} : function (object, key, value) {
  object[key] = value;
  return object;
};

},{"./_descriptors":35,"./_object-dp":54,"./_property-desc":61}],43:[function(require,module,exports){
var document = require('./_global').document;
module.exports = document && document.documentElement;

},{"./_global":40}],44:[function(require,module,exports){
module.exports = !require('./_descriptors') && !require('./_fails')(function () {
  return Object.defineProperty(require('./_dom-create')('div'), 'a', { get: function () { return 7; } }).a != 7;
});

},{"./_descriptors":35,"./_dom-create":36,"./_fails":39}],45:[function(require,module,exports){
// fallback for non-array-like ES3 and non-enumerable old V8 strings
var cof = require('./_cof');
// eslint-disable-next-line no-prototype-builtins
module.exports = Object('z').propertyIsEnumerable(0) ? Object : function (it) {
  return cof(it) == 'String' ? it.split('') : Object(it);
};

},{"./_cof":31}],46:[function(require,module,exports){
module.exports = function (it) {
  return typeof it === 'object' ? it !== null : typeof it === 'function';
};

},{}],47:[function(require,module,exports){
'use strict';
var create = require('./_object-create');
var descriptor = require('./_property-desc');
var setToStringTag = require('./_set-to-string-tag');
var IteratorPrototype = {};

// 25.1.2.1.1 %IteratorPrototype%[@@iterator]()
require('./_hide')(IteratorPrototype, require('./_wks')('iterator'), function () { return this; });

module.exports = function (Constructor, NAME, next) {
  Constructor.prototype = create(IteratorPrototype, { next: descriptor(1, next) });
  setToStringTag(Constructor, NAME + ' Iterator');
};

},{"./_hide":42,"./_object-create":53,"./_property-desc":61,"./_set-to-string-tag":63,"./_wks":74}],48:[function(require,module,exports){
'use strict';
var LIBRARY = require('./_library');
var $export = require('./_export');
var redefine = require('./_redefine');
var hide = require('./_hide');
var has = require('./_has');
var Iterators = require('./_iterators');
var $iterCreate = require('./_iter-create');
var setToStringTag = require('./_set-to-string-tag');
var getPrototypeOf = require('./_object-gpo');
var ITERATOR = require('./_wks')('iterator');
var BUGGY = !([].keys && 'next' in [].keys()); // Safari has buggy iterators w/o `next`
var FF_ITERATOR = '@@iterator';
var KEYS = 'keys';
var VALUES = 'values';

var returnThis = function () { return this; };

module.exports = function (Base, NAME, Constructor, next, DEFAULT, IS_SET, FORCED) {
  $iterCreate(Constructor, NAME, next);
  var getMethod = function (kind) {
    if (!BUGGY && kind in proto) return proto[kind];
    switch (kind) {
      case KEYS: return function keys() { return new Constructor(this, kind); };
      case VALUES: return function values() { return new Constructor(this, kind); };
    } return function entries() { return new Constructor(this, kind); };
  };
  var TAG = NAME + ' Iterator';
  var DEF_VALUES = DEFAULT == VALUES;
  var VALUES_BUG = false;
  var proto = Base.prototype;
  var $native = proto[ITERATOR] || proto[FF_ITERATOR] || DEFAULT && proto[DEFAULT];
  var $default = $native || getMethod(DEFAULT);
  var $entries = DEFAULT ? !DEF_VALUES ? $default : getMethod('entries') : undefined;
  var $anyNative = NAME == 'Array' ? proto.entries || $native : $native;
  var methods, key, IteratorPrototype;
  // Fix native
  if ($anyNative) {
    IteratorPrototype = getPrototypeOf($anyNative.call(new Base()));
    if (IteratorPrototype !== Object.prototype && IteratorPrototype.next) {
      // Set @@toStringTag to native iterators
      setToStringTag(IteratorPrototype, TAG, true);
      // fix for some old engines
      if (!LIBRARY && !has(IteratorPrototype, ITERATOR)) hide(IteratorPrototype, ITERATOR, returnThis);
    }
  }
  // fix Array#{values, @@iterator}.name in V8 / FF
  if (DEF_VALUES && $native && $native.name !== VALUES) {
    VALUES_BUG = true;
    $default = function values() { return $native.call(this); };
  }
  // Define iterator
  if ((!LIBRARY || FORCED) && (BUGGY || VALUES_BUG || !proto[ITERATOR])) {
    hide(proto, ITERATOR, $default);
  }
  // Plug for library
  Iterators[NAME] = $default;
  Iterators[TAG] = returnThis;
  if (DEFAULT) {
    methods = {
      values: DEF_VALUES ? $default : getMethod(VALUES),
      keys: IS_SET ? $default : getMethod(KEYS),
      entries: $entries
    };
    if (FORCED) for (key in methods) {
      if (!(key in proto)) redefine(proto, key, methods[key]);
    } else $export($export.P + $export.F * (BUGGY || VALUES_BUG), NAME, methods);
  }
  return methods;
};

},{"./_export":38,"./_has":41,"./_hide":42,"./_iter-create":47,"./_iterators":50,"./_library":51,"./_object-gpo":57,"./_redefine":62,"./_set-to-string-tag":63,"./_wks":74}],49:[function(require,module,exports){
module.exports = function (done, value) {
  return { value: value, done: !!done };
};

},{}],50:[function(require,module,exports){
module.exports = {};

},{}],51:[function(require,module,exports){
module.exports = true;

},{}],52:[function(require,module,exports){
'use strict';
// 19.1.2.1 Object.assign(target, source, ...)
var getKeys = require('./_object-keys');
var gOPS = require('./_object-gops');
var pIE = require('./_object-pie');
var toObject = require('./_to-object');
var IObject = require('./_iobject');
var $assign = Object.assign;

// should work with symbols and should have deterministic property order (V8 bug)
module.exports = !$assign || require('./_fails')(function () {
  var A = {};
  var B = {};
  // eslint-disable-next-line no-undef
  var S = Symbol();
  var K = 'abcdefghijklmnopqrst';
  A[S] = 7;
  K.split('').forEach(function (k) { B[k] = k; });
  return $assign({}, A)[S] != 7 || Object.keys($assign({}, B)).join('') != K;
}) ? function assign(target, source) { // eslint-disable-line no-unused-vars
  var T = toObject(target);
  var aLen = arguments.length;
  var index = 1;
  var getSymbols = gOPS.f;
  var isEnum = pIE.f;
  while (aLen > index) {
    var S = IObject(arguments[index++]);
    var keys = getSymbols ? getKeys(S).concat(getSymbols(S)) : getKeys(S);
    var length = keys.length;
    var j = 0;
    var key;
    while (length > j) if (isEnum.call(S, key = keys[j++])) T[key] = S[key];
  } return T;
} : $assign;

},{"./_fails":39,"./_iobject":45,"./_object-gops":56,"./_object-keys":59,"./_object-pie":60,"./_to-object":71}],53:[function(require,module,exports){
// 19.1.2.2 / 15.2.3.5 Object.create(O [, Properties])
var anObject = require('./_an-object');
var dPs = require('./_object-dps');
var enumBugKeys = require('./_enum-bug-keys');
var IE_PROTO = require('./_shared-key')('IE_PROTO');
var Empty = function () { /* empty */ };
var PROTOTYPE = 'prototype';

// Create object with fake `null` prototype: use iframe Object with cleared prototype
var createDict = function () {
  // Thrash, waste and sodomy: IE GC bug
  var iframe = require('./_dom-create')('iframe');
  var i = enumBugKeys.length;
  var lt = '<';
  var gt = '>';
  var iframeDocument;
  iframe.style.display = 'none';
  require('./_html').appendChild(iframe);
  iframe.src = 'javascript:'; // eslint-disable-line no-script-url
  // createDict = iframe.contentWindow.Object;
  // html.removeChild(iframe);
  iframeDocument = iframe.contentWindow.document;
  iframeDocument.open();
  iframeDocument.write(lt + 'script' + gt + 'document.F=Object' + lt + '/script' + gt);
  iframeDocument.close();
  createDict = iframeDocument.F;
  while (i--) delete createDict[PROTOTYPE][enumBugKeys[i]];
  return createDict();
};

module.exports = Object.create || function create(O, Properties) {
  var result;
  if (O !== null) {
    Empty[PROTOTYPE] = anObject(O);
    result = new Empty();
    Empty[PROTOTYPE] = null;
    // add "__proto__" for Object.getPrototypeOf polyfill
    result[IE_PROTO] = O;
  } else result = createDict();
  return Properties === undefined ? result : dPs(result, Properties);
};

},{"./_an-object":28,"./_dom-create":36,"./_enum-bug-keys":37,"./_html":43,"./_object-dps":55,"./_shared-key":64}],54:[function(require,module,exports){
var anObject = require('./_an-object');
var IE8_DOM_DEFINE = require('./_ie8-dom-define');
var toPrimitive = require('./_to-primitive');
var dP = Object.defineProperty;

exports.f = require('./_descriptors') ? Object.defineProperty : function defineProperty(O, P, Attributes) {
  anObject(O);
  P = toPrimitive(P, true);
  anObject(Attributes);
  if (IE8_DOM_DEFINE) try {
    return dP(O, P, Attributes);
  } catch (e) { /* empty */ }
  if ('get' in Attributes || 'set' in Attributes) throw TypeError('Accessors not supported!');
  if ('value' in Attributes) O[P] = Attributes.value;
  return O;
};

},{"./_an-object":28,"./_descriptors":35,"./_ie8-dom-define":44,"./_to-primitive":72}],55:[function(require,module,exports){
var dP = require('./_object-dp');
var anObject = require('./_an-object');
var getKeys = require('./_object-keys');

module.exports = require('./_descriptors') ? Object.defineProperties : function defineProperties(O, Properties) {
  anObject(O);
  var keys = getKeys(Properties);
  var length = keys.length;
  var i = 0;
  var P;
  while (length > i) dP.f(O, P = keys[i++], Properties[P]);
  return O;
};

},{"./_an-object":28,"./_descriptors":35,"./_object-dp":54,"./_object-keys":59}],56:[function(require,module,exports){
exports.f = Object.getOwnPropertySymbols;

},{}],57:[function(require,module,exports){
// 19.1.2.9 / 15.2.3.2 Object.getPrototypeOf(O)
var has = require('./_has');
var toObject = require('./_to-object');
var IE_PROTO = require('./_shared-key')('IE_PROTO');
var ObjectProto = Object.prototype;

module.exports = Object.getPrototypeOf || function (O) {
  O = toObject(O);
  if (has(O, IE_PROTO)) return O[IE_PROTO];
  if (typeof O.constructor == 'function' && O instanceof O.constructor) {
    return O.constructor.prototype;
  } return O instanceof Object ? ObjectProto : null;
};

},{"./_has":41,"./_shared-key":64,"./_to-object":71}],58:[function(require,module,exports){
var has = require('./_has');
var toIObject = require('./_to-iobject');
var arrayIndexOf = require('./_array-includes')(false);
var IE_PROTO = require('./_shared-key')('IE_PROTO');

module.exports = function (object, names) {
  var O = toIObject(object);
  var i = 0;
  var result = [];
  var key;
  for (key in O) if (key != IE_PROTO) has(O, key) && result.push(key);
  // Don't enum bug & hidden keys
  while (names.length > i) if (has(O, key = names[i++])) {
    ~arrayIndexOf(result, key) || result.push(key);
  }
  return result;
};

},{"./_array-includes":29,"./_has":41,"./_shared-key":64,"./_to-iobject":69}],59:[function(require,module,exports){
// 19.1.2.14 / 15.2.3.14 Object.keys(O)
var $keys = require('./_object-keys-internal');
var enumBugKeys = require('./_enum-bug-keys');

module.exports = Object.keys || function keys(O) {
  return $keys(O, enumBugKeys);
};

},{"./_enum-bug-keys":37,"./_object-keys-internal":58}],60:[function(require,module,exports){
exports.f = {}.propertyIsEnumerable;

},{}],61:[function(require,module,exports){
module.exports = function (bitmap, value) {
  return {
    enumerable: !(bitmap & 1),
    configurable: !(bitmap & 2),
    writable: !(bitmap & 4),
    value: value
  };
};

},{}],62:[function(require,module,exports){
module.exports = require('./_hide');

},{"./_hide":42}],63:[function(require,module,exports){
var def = require('./_object-dp').f;
var has = require('./_has');
var TAG = require('./_wks')('toStringTag');

module.exports = function (it, tag, stat) {
  if (it && !has(it = stat ? it : it.prototype, TAG)) def(it, TAG, { configurable: true, value: tag });
};

},{"./_has":41,"./_object-dp":54,"./_wks":74}],64:[function(require,module,exports){
var shared = require('./_shared')('keys');
var uid = require('./_uid');
module.exports = function (key) {
  return shared[key] || (shared[key] = uid(key));
};

},{"./_shared":65,"./_uid":73}],65:[function(require,module,exports){
var global = require('./_global');
var SHARED = '__core-js_shared__';
var store = global[SHARED] || (global[SHARED] = {});
module.exports = function (key) {
  return store[key] || (store[key] = {});
};

},{"./_global":40}],66:[function(require,module,exports){
var toInteger = require('./_to-integer');
var defined = require('./_defined');
// true  -> String#at
// false -> String#codePointAt
module.exports = function (TO_STRING) {
  return function (that, pos) {
    var s = String(defined(that));
    var i = toInteger(pos);
    var l = s.length;
    var a, b;
    if (i < 0 || i >= l) return TO_STRING ? '' : undefined;
    a = s.charCodeAt(i);
    return a < 0xd800 || a > 0xdbff || i + 1 === l || (b = s.charCodeAt(i + 1)) < 0xdc00 || b > 0xdfff
      ? TO_STRING ? s.charAt(i) : a
      : TO_STRING ? s.slice(i, i + 2) : (a - 0xd800 << 10) + (b - 0xdc00) + 0x10000;
  };
};

},{"./_defined":34,"./_to-integer":68}],67:[function(require,module,exports){
var toInteger = require('./_to-integer');
var max = Math.max;
var min = Math.min;
module.exports = function (index, length) {
  index = toInteger(index);
  return index < 0 ? max(index + length, 0) : min(index, length);
};

},{"./_to-integer":68}],68:[function(require,module,exports){
// 7.1.4 ToInteger
var ceil = Math.ceil;
var floor = Math.floor;
module.exports = function (it) {
  return isNaN(it = +it) ? 0 : (it > 0 ? floor : ceil)(it);
};

},{}],69:[function(require,module,exports){
// to indexed object, toObject with fallback for non-array-like ES3 strings
var IObject = require('./_iobject');
var defined = require('./_defined');
module.exports = function (it) {
  return IObject(defined(it));
};

},{"./_defined":34,"./_iobject":45}],70:[function(require,module,exports){
// 7.1.15 ToLength
var toInteger = require('./_to-integer');
var min = Math.min;
module.exports = function (it) {
  return it > 0 ? min(toInteger(it), 0x1fffffffffffff) : 0; // pow(2, 53) - 1 == 9007199254740991
};

},{"./_to-integer":68}],71:[function(require,module,exports){
// 7.1.13 ToObject(argument)
var defined = require('./_defined');
module.exports = function (it) {
  return Object(defined(it));
};

},{"./_defined":34}],72:[function(require,module,exports){
// 7.1.1 ToPrimitive(input [, PreferredType])
var isObject = require('./_is-object');
// instead of the ES6 spec version, we didn't implement @@toPrimitive case
// and the second argument - flag - preferred type is a string
module.exports = function (it, S) {
  if (!isObject(it)) return it;
  var fn, val;
  if (S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  if (typeof (fn = it.valueOf) == 'function' && !isObject(val = fn.call(it))) return val;
  if (!S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  throw TypeError("Can't convert object to primitive value");
};

},{"./_is-object":46}],73:[function(require,module,exports){
var id = 0;
var px = Math.random();
module.exports = function (key) {
  return 'Symbol('.concat(key === undefined ? '' : key, ')_', (++id + px).toString(36));
};

},{}],74:[function(require,module,exports){
var store = require('./_shared')('wks');
var uid = require('./_uid');
var Symbol = require('./_global').Symbol;
var USE_SYMBOL = typeof Symbol == 'function';

var $exports = module.exports = function (name) {
  return store[name] || (store[name] =
    USE_SYMBOL && Symbol[name] || (USE_SYMBOL ? Symbol : uid)('Symbol.' + name));
};

$exports.store = store;

},{"./_global":40,"./_shared":65,"./_uid":73}],75:[function(require,module,exports){
var classof = require('./_classof');
var ITERATOR = require('./_wks')('iterator');
var Iterators = require('./_iterators');
module.exports = require('./_core').getIteratorMethod = function (it) {
  if (it != undefined) return it[ITERATOR]
    || it['@@iterator']
    || Iterators[classof(it)];
};

},{"./_classof":30,"./_core":32,"./_iterators":50,"./_wks":74}],76:[function(require,module,exports){
var anObject = require('./_an-object');
var get = require('./core.get-iterator-method');
module.exports = require('./_core').getIterator = function (it) {
  var iterFn = get(it);
  if (typeof iterFn != 'function') throw TypeError(it + ' is not iterable!');
  return anObject(iterFn.call(it));
};

},{"./_an-object":28,"./_core":32,"./core.get-iterator-method":75}],77:[function(require,module,exports){
'use strict';
var addToUnscopables = require('./_add-to-unscopables');
var step = require('./_iter-step');
var Iterators = require('./_iterators');
var toIObject = require('./_to-iobject');

// 22.1.3.4 Array.prototype.entries()
// 22.1.3.13 Array.prototype.keys()
// 22.1.3.29 Array.prototype.values()
// 22.1.3.30 Array.prototype[@@iterator]()
module.exports = require('./_iter-define')(Array, 'Array', function (iterated, kind) {
  this._t = toIObject(iterated); // target
  this._i = 0;                   // next index
  this._k = kind;                // kind
// 22.1.5.2.1 %ArrayIteratorPrototype%.next()
}, function () {
  var O = this._t;
  var kind = this._k;
  var index = this._i++;
  if (!O || index >= O.length) {
    this._t = undefined;
    return step(1);
  }
  if (kind == 'keys') return step(0, index);
  if (kind == 'values') return step(0, O[index]);
  return step(0, [index, O[index]]);
}, 'values');

// argumentsList[@@iterator] is %ArrayProto_values% (9.4.4.6, 9.4.4.7)
Iterators.Arguments = Iterators.Array;

addToUnscopables('keys');
addToUnscopables('values');
addToUnscopables('entries');

},{"./_add-to-unscopables":27,"./_iter-define":48,"./_iter-step":49,"./_iterators":50,"./_to-iobject":69}],78:[function(require,module,exports){
// 19.1.3.1 Object.assign(target, source)
var $export = require('./_export');

$export($export.S + $export.F, 'Object', { assign: require('./_object-assign') });

},{"./_export":38,"./_object-assign":52}],79:[function(require,module,exports){
'use strict';
var $at = require('./_string-at')(true);

// 21.1.3.27 String.prototype[@@iterator]()
require('./_iter-define')(String, 'String', function (iterated) {
  this._t = String(iterated); // target
  this._i = 0;                // next index
// 21.1.5.2.1 %StringIteratorPrototype%.next()
}, function () {
  var O = this._t;
  var index = this._i;
  var point;
  if (index >= O.length) return { value: undefined, done: true };
  point = $at(O, index);
  this._i += point.length;
  return { value: point, done: false };
});

},{"./_iter-define":48,"./_string-at":66}],80:[function(require,module,exports){
require('./es6.array.iterator');
var global = require('./_global');
var hide = require('./_hide');
var Iterators = require('./_iterators');
var TO_STRING_TAG = require('./_wks')('toStringTag');

var DOMIterables = ('CSSRuleList,CSSStyleDeclaration,CSSValueList,ClientRectList,DOMRectList,DOMStringList,' +
  'DOMTokenList,DataTransferItemList,FileList,HTMLAllCollection,HTMLCollection,HTMLFormElement,HTMLSelectElement,' +
  'MediaList,MimeTypeArray,NamedNodeMap,NodeList,PaintRequestList,Plugin,PluginArray,SVGLengthList,SVGNumberList,' +
  'SVGPathSegList,SVGPointList,SVGStringList,SVGTransformList,SourceBufferList,StyleSheetList,TextTrackCueList,' +
  'TextTrackList,TouchList').split(',');

for (var i = 0; i < DOMIterables.length; i++) {
  var NAME = DOMIterables[i];
  var Collection = global[NAME];
  var proto = Collection && Collection.prototype;
  if (proto && !proto[TO_STRING_TAG]) hide(proto, TO_STRING_TAG, NAME);
  Iterators[NAME] = Iterators.Array;
}

},{"./_global":40,"./_hide":42,"./_iterators":50,"./_wks":74,"./es6.array.iterator":77}],81:[function(require,module,exports){
/**!
 * Sortable
 * @author	RubaXa   <trash@rubaxa.org>
 * @license MIT
 */

(function sortableModule(factory) {
	"use strict";

	if (typeof define === "function" && define.amd) {
		define(factory);
	}
	else if (typeof module != "undefined" && typeof module.exports != "undefined") {
		module.exports = factory();
	}
	else {
		/* jshint sub:true */
		window["Sortable"] = factory();
	}
})(function sortableFactory() {
	"use strict";

	if (typeof window === "undefined" || !window.document) {
		return function sortableError() {
			throw new Error("Sortable.js requires a window with a document");
		};
	}

	var dragEl,
		parentEl,
		ghostEl,
		cloneEl,
		rootEl,
		nextEl,
		lastDownEl,

		scrollEl,
		scrollParentEl,
		scrollCustomFn,

		lastEl,
		lastCSS,
		lastParentCSS,

		oldIndex,
		newIndex,

		activeGroup,
		putSortable,

		autoScroll = {},

		tapEvt,
		touchEvt,

		moved,

		/** @const */
		R_SPACE = /\s+/g,
		R_FLOAT = /left|right|inline/,

		expando = 'Sortable' + (new Date).getTime(),

		win = window,
		document = win.document,
		parseInt = win.parseInt,
		setTimeout = win.setTimeout,

		$ = win.jQuery || win.Zepto,
		Polymer = win.Polymer,

		captureMode = false,
		passiveMode = false,

		supportDraggable = ('draggable' in document.createElement('div')),
		supportCssPointerEvents = (function (el) {
			// false when IE11
			if (!!navigator.userAgent.match(/(?:Trident.*rv[ :]?11\.|msie)/i)) {
				return false;
			}
			el = document.createElement('x');
			el.style.cssText = 'pointer-events:auto';
			return el.style.pointerEvents === 'auto';
		})(),

		_silent = false,

		abs = Math.abs,
		min = Math.min,

		savedInputChecked = [],
		touchDragOverListeners = [],

		_autoScroll = _throttle(function (/**Event*/evt, /**Object*/options, /**HTMLElement*/rootEl) {
			// Bug: https://bugzilla.mozilla.org/show_bug.cgi?id=505521
			if (rootEl && options.scroll) {
				var _this = rootEl[expando],
					el,
					rect,
					sens = options.scrollSensitivity,
					speed = options.scrollSpeed,

					x = evt.clientX,
					y = evt.clientY,

					winWidth = window.innerWidth,
					winHeight = window.innerHeight,

					vx,
					vy,

					scrollOffsetX,
					scrollOffsetY
				;

				// Delect scrollEl
				if (scrollParentEl !== rootEl) {
					scrollEl = options.scroll;
					scrollParentEl = rootEl;
					scrollCustomFn = options.scrollFn;

					if (scrollEl === true) {
						scrollEl = rootEl;

						do {
							if ((scrollEl.offsetWidth < scrollEl.scrollWidth) ||
								(scrollEl.offsetHeight < scrollEl.scrollHeight)
							) {
								break;
							}
							/* jshint boss:true */
						} while (scrollEl = scrollEl.parentNode);
					}
				}

				if (scrollEl) {
					el = scrollEl;
					rect = scrollEl.getBoundingClientRect();
					vx = (abs(rect.right - x) <= sens) - (abs(rect.left - x) <= sens);
					vy = (abs(rect.bottom - y) <= sens) - (abs(rect.top - y) <= sens);
				}


				if (!(vx || vy)) {
					vx = (winWidth - x <= sens) - (x <= sens);
					vy = (winHeight - y <= sens) - (y <= sens);

					/* jshint expr:true */
					(vx || vy) && (el = win);
				}


				if (autoScroll.vx !== vx || autoScroll.vy !== vy || autoScroll.el !== el) {
					autoScroll.el = el;
					autoScroll.vx = vx;
					autoScroll.vy = vy;

					clearInterval(autoScroll.pid);

					if (el) {
						autoScroll.pid = setInterval(function () {
							scrollOffsetY = vy ? vy * speed : 0;
							scrollOffsetX = vx ? vx * speed : 0;

							if ('function' === typeof(scrollCustomFn)) {
								return scrollCustomFn.call(_this, scrollOffsetX, scrollOffsetY, evt);
							}

							if (el === win) {
								win.scrollTo(win.pageXOffset + scrollOffsetX, win.pageYOffset + scrollOffsetY);
							} else {
								el.scrollTop += scrollOffsetY;
								el.scrollLeft += scrollOffsetX;
							}
						}, 24);
					}
				}
			}
		}, 30),

		_prepareGroup = function (options) {
			function toFn(value, pull) {
				if (value === void 0 || value === true) {
					value = group.name;
				}

				if (typeof value === 'function') {
					return value;
				} else {
					return function (to, from) {
						var fromGroup = from.options.group.name;

						return pull
							? value
							: value && (value.join
								? value.indexOf(fromGroup) > -1
								: (fromGroup == value)
							);
					};
				}
			}

			var group = {};
			var originalGroup = options.group;

			if (!originalGroup || typeof originalGroup != 'object') {
				originalGroup = {name: originalGroup};
			}

			group.name = originalGroup.name;
			group.checkPull = toFn(originalGroup.pull, true);
			group.checkPut = toFn(originalGroup.put);
			group.revertClone = originalGroup.revertClone;

			options.group = group;
		}
	;

	// Detect support a passive mode
	try {
		window.addEventListener('test', null, Object.defineProperty({}, 'passive', {
			get: function () {
				// `false`, because everything starts to work incorrectly and instead of d'n'd,
				// begins the page has scrolled.
				passiveMode = false;
				captureMode = {
					capture: false,
					passive: passiveMode
				};
			}
		}));
	} catch (err) {}

	/**
	 * @class  Sortable
	 * @param  {HTMLElement}  el
	 * @param  {Object}       [options]
	 */
	function Sortable(el, options) {
		if (!(el && el.nodeType && el.nodeType === 1)) {
			throw 'Sortable: `el` must be HTMLElement, and not ' + {}.toString.call(el);
		}

		this.el = el; // root element
		this.options = options = _extend({}, options);


		// Export instance
		el[expando] = this;

		// Default options
		var defaults = {
			group: Math.random(),
			sort: true,
			disabled: false,
			store: null,
			handle: null,
			scroll: true,
			scrollSensitivity: 30,
			scrollSpeed: 10,
			draggable: /[uo]l/i.test(el.nodeName) ? 'li' : '>*',
			ghostClass: 'sortable-ghost',
			chosenClass: 'sortable-chosen',
			dragClass: 'sortable-drag',
			ignore: 'a, img',
			filter: null,
			preventOnFilter: true,
			animation: 0,
			setData: function (dataTransfer, dragEl) {
				dataTransfer.setData('Text', dragEl.textContent);
			},
			dropBubble: false,
			dragoverBubble: false,
			dataIdAttr: 'data-id',
			delay: 0,
			forceFallback: false,
			fallbackClass: 'sortable-fallback',
			fallbackOnBody: false,
			fallbackTolerance: 0,
			fallbackOffset: {x: 0, y: 0},
			supportPointer: Sortable.supportPointer !== false
		};


		// Set default options
		for (var name in defaults) {
			!(name in options) && (options[name] = defaults[name]);
		}

		_prepareGroup(options);

		// Bind all private methods
		for (var fn in this) {
			if (fn.charAt(0) === '_' && typeof this[fn] === 'function') {
				this[fn] = this[fn].bind(this);
			}
		}

		// Setup drag mode
		this.nativeDraggable = options.forceFallback ? false : supportDraggable;

		// Bind events
		_on(el, 'mousedown', this._onTapStart);
		_on(el, 'touchstart', this._onTapStart);
		options.supportPointer && _on(el, 'pointerdown', this._onTapStart);

		if (this.nativeDraggable) {
			_on(el, 'dragover', this);
			_on(el, 'dragenter', this);
		}

		touchDragOverListeners.push(this._onDragOver);

		// Restore sorting
		options.store && this.sort(options.store.get(this));
	}


	Sortable.prototype = /** @lends Sortable.prototype */ {
		constructor: Sortable,

		_onTapStart: function (/** Event|TouchEvent */evt) {
			var _this = this,
				el = this.el,
				options = this.options,
				preventOnFilter = options.preventOnFilter,
				type = evt.type,
				touch = evt.touches && evt.touches[0],
				target = (touch || evt).target,
				originalTarget = evt.target.shadowRoot && (evt.path && evt.path[0]) || target,
				filter = options.filter,
				startIndex;

			_saveInputCheckedState(el);


			// Don't trigger start event when an element is been dragged, otherwise the evt.oldindex always wrong when set option.group.
			if (dragEl) {
				return;
			}

			if (/mousedown|pointerdown/.test(type) && evt.button !== 0 || options.disabled) {
				return; // only left button or enabled
			}

			// cancel dnd if original target is content editable
			if (originalTarget.isContentEditable) {
				return;
			}

			target = _closest(target, options.draggable, el);

			if (!target) {
				return;
			}

			if (lastDownEl === target) {
				// Ignoring duplicate `down`
				return;
			}

			// Get the index of the dragged element within its parent
			startIndex = _index(target, options.draggable);

			// Check filter
			if (typeof filter === 'function') {
				if (filter.call(this, evt, target, this)) {
					_dispatchEvent(_this, originalTarget, 'filter', target, el, el, startIndex);
					preventOnFilter && evt.preventDefault();
					return; // cancel dnd
				}
			}
			else if (filter) {
				filter = filter.split(',').some(function (criteria) {
					criteria = _closest(originalTarget, criteria.trim(), el);

					if (criteria) {
						_dispatchEvent(_this, criteria, 'filter', target, el, el, startIndex);
						return true;
					}
				});

				if (filter) {
					preventOnFilter && evt.preventDefault();
					return; // cancel dnd
				}
			}

			if (options.handle && !_closest(originalTarget, options.handle, el)) {
				return;
			}

			// Prepare `dragstart`
			this._prepareDragStart(evt, touch, target, startIndex);
		},

		_prepareDragStart: function (/** Event */evt, /** Touch */touch, /** HTMLElement */target, /** Number */startIndex) {
			var _this = this,
				el = _this.el,
				options = _this.options,
				ownerDocument = el.ownerDocument,
				dragStartFn;

			if (target && !dragEl && (target.parentNode === el)) {
				tapEvt = evt;

				rootEl = el;
				dragEl = target;
				parentEl = dragEl.parentNode;
				nextEl = dragEl.nextSibling;
				lastDownEl = target;
				activeGroup = options.group;
				oldIndex = startIndex;

				this._lastX = (touch || evt).clientX;
				this._lastY = (touch || evt).clientY;

				dragEl.style['will-change'] = 'all';

				dragStartFn = function () {
					// Delayed drag has been triggered
					// we can re-enable the events: touchmove/mousemove
					_this._disableDelayedDrag();

					// Make the element draggable
					dragEl.draggable = _this.nativeDraggable;

					// Chosen item
					_toggleClass(dragEl, options.chosenClass, true);

					// Bind the events: dragstart/dragend
					_this._triggerDragStart(evt, touch);

					// Drag start event
					_dispatchEvent(_this, rootEl, 'choose', dragEl, rootEl, rootEl, oldIndex);
				};

				// Disable "draggable"
				options.ignore.split(',').forEach(function (criteria) {
					_find(dragEl, criteria.trim(), _disableDraggable);
				});

				_on(ownerDocument, 'mouseup', _this._onDrop);
				_on(ownerDocument, 'touchend', _this._onDrop);
				_on(ownerDocument, 'touchcancel', _this._onDrop);
				_on(ownerDocument, 'selectstart', _this);
				options.supportPointer && _on(ownerDocument, 'pointercancel', _this._onDrop);

				if (options.delay) {
					// If the user moves the pointer or let go the click or touch
					// before the delay has been reached:
					// disable the delayed drag
					_on(ownerDocument, 'mouseup', _this._disableDelayedDrag);
					_on(ownerDocument, 'touchend', _this._disableDelayedDrag);
					_on(ownerDocument, 'touchcancel', _this._disableDelayedDrag);
					_on(ownerDocument, 'mousemove', _this._disableDelayedDrag);
					_on(ownerDocument, 'touchmove', _this._disableDelayedDrag);
					options.supportPointer && _on(ownerDocument, 'pointermove', _this._disableDelayedDrag);

					_this._dragStartTimer = setTimeout(dragStartFn, options.delay);
				} else {
					dragStartFn();
				}


			}
		},

		_disableDelayedDrag: function () {
			var ownerDocument = this.el.ownerDocument;

			clearTimeout(this._dragStartTimer);
			_off(ownerDocument, 'mouseup', this._disableDelayedDrag);
			_off(ownerDocument, 'touchend', this._disableDelayedDrag);
			_off(ownerDocument, 'touchcancel', this._disableDelayedDrag);
			_off(ownerDocument, 'mousemove', this._disableDelayedDrag);
			_off(ownerDocument, 'touchmove', this._disableDelayedDrag);
			_off(ownerDocument, 'pointermove', this._disableDelayedDrag);
		},

		_triggerDragStart: function (/** Event */evt, /** Touch */touch) {
			touch = touch || (evt.pointerType == 'touch' ? evt : null);

			if (touch) {
				// Touch device support
				tapEvt = {
					target: dragEl,
					clientX: touch.clientX,
					clientY: touch.clientY
				};

				this._onDragStart(tapEvt, 'touch');
			}
			else if (!this.nativeDraggable) {
				this._onDragStart(tapEvt, true);
			}
			else {
				_on(dragEl, 'dragend', this);
				_on(rootEl, 'dragstart', this._onDragStart);
			}

			try {
				if (document.selection) {
					// Timeout neccessary for IE9
					_nextTick(function () {
						document.selection.empty();
					});
				} else {
					window.getSelection().removeAllRanges();
				}
			} catch (err) {
			}
		},

		_dragStarted: function () {
			if (rootEl && dragEl) {
				var options = this.options;

				// Apply effect
				_toggleClass(dragEl, options.ghostClass, true);
				_toggleClass(dragEl, options.dragClass, false);

				Sortable.active = this;

				// Drag start event
				_dispatchEvent(this, rootEl, 'start', dragEl, rootEl, rootEl, oldIndex);
			} else {
				this._nulling();
			}
		},

		_emulateDragOver: function () {
			if (touchEvt) {
				if (this._lastX === touchEvt.clientX && this._lastY === touchEvt.clientY) {
					return;
				}

				this._lastX = touchEvt.clientX;
				this._lastY = touchEvt.clientY;

				if (!supportCssPointerEvents) {
					_css(ghostEl, 'display', 'none');
				}

				var target = document.elementFromPoint(touchEvt.clientX, touchEvt.clientY);
				var parent = target;
				var i = touchDragOverListeners.length;

				if (target && target.shadowRoot) {
					target = target.shadowRoot.elementFromPoint(touchEvt.clientX, touchEvt.clientY);
					parent = target;
				}

				if (parent) {
					do {
						if (parent[expando]) {
							while (i--) {
								touchDragOverListeners[i]({
									clientX: touchEvt.clientX,
									clientY: touchEvt.clientY,
									target: target,
									rootEl: parent
								});
							}

							break;
						}

						target = parent; // store last element
					}
					/* jshint boss:true */
					while (parent = parent.parentNode);
				}

				if (!supportCssPointerEvents) {
					_css(ghostEl, 'display', '');
				}
			}
		},


		_onTouchMove: function (/**TouchEvent*/evt) {
			if (tapEvt) {
				var	options = this.options,
					fallbackTolerance = options.fallbackTolerance,
					fallbackOffset = options.fallbackOffset,
					touch = evt.touches ? evt.touches[0] : evt,
					dx = (touch.clientX - tapEvt.clientX) + fallbackOffset.x,
					dy = (touch.clientY - tapEvt.clientY) + fallbackOffset.y,
					translate3d = evt.touches ? 'translate3d(' + dx + 'px,' + dy + 'px,0)' : 'translate(' + dx + 'px,' + dy + 'px)';

				// only set the status to dragging, when we are actually dragging
				if (!Sortable.active) {
					if (fallbackTolerance &&
						min(abs(touch.clientX - this._lastX), abs(touch.clientY - this._lastY)) < fallbackTolerance
					) {
						return;
					}

					this._dragStarted();
				}

				// as well as creating the ghost element on the document body
				this._appendGhost();

				moved = true;
				touchEvt = touch;

				_css(ghostEl, 'webkitTransform', translate3d);
				_css(ghostEl, 'mozTransform', translate3d);
				_css(ghostEl, 'msTransform', translate3d);
				_css(ghostEl, 'transform', translate3d);

				evt.preventDefault();
			}
		},

		_appendGhost: function () {
			if (!ghostEl) {
				var rect = dragEl.getBoundingClientRect(),
					css = _css(dragEl),
					options = this.options,
					ghostRect;

				ghostEl = dragEl.cloneNode(true);

				_toggleClass(ghostEl, options.ghostClass, false);
				_toggleClass(ghostEl, options.fallbackClass, true);
				_toggleClass(ghostEl, options.dragClass, true);

				_css(ghostEl, 'top', rect.top - parseInt(css.marginTop, 10));
				_css(ghostEl, 'left', rect.left - parseInt(css.marginLeft, 10));
				_css(ghostEl, 'width', rect.width);
				_css(ghostEl, 'height', rect.height);
				_css(ghostEl, 'opacity', '0.8');
				_css(ghostEl, 'position', 'fixed');
				_css(ghostEl, 'zIndex', '100000');
				_css(ghostEl, 'pointerEvents', 'none');

				options.fallbackOnBody && document.body.appendChild(ghostEl) || rootEl.appendChild(ghostEl);

				// Fixing dimensions.
				ghostRect = ghostEl.getBoundingClientRect();
				_css(ghostEl, 'width', rect.width * 2 - ghostRect.width);
				_css(ghostEl, 'height', rect.height * 2 - ghostRect.height);
			}
		},

		_onDragStart: function (/**Event*/evt, /**boolean*/useFallback) {
			var _this = this;
			var dataTransfer = evt.dataTransfer;
			var options = _this.options;

			_this._offUpEvents();

			if (activeGroup.checkPull(_this, _this, dragEl, evt)) {
				cloneEl = _clone(dragEl);

				cloneEl.draggable = false;
				cloneEl.style['will-change'] = '';

				_css(cloneEl, 'display', 'none');
				_toggleClass(cloneEl, _this.options.chosenClass, false);

				// #1143: IFrame support workaround
				_this._cloneId = _nextTick(function () {
					rootEl.insertBefore(cloneEl, dragEl);
					_dispatchEvent(_this, rootEl, 'clone', dragEl);
				});
			}

			_toggleClass(dragEl, options.dragClass, true);

			if (useFallback) {
				if (useFallback === 'touch') {
					// Bind touch events
					_on(document, 'touchmove', _this._onTouchMove);
					_on(document, 'touchend', _this._onDrop);
					_on(document, 'touchcancel', _this._onDrop);

					if (options.supportPointer) {
						_on(document, 'pointermove', _this._onTouchMove);
						_on(document, 'pointerup', _this._onDrop);
					}
				} else {
					// Old brwoser
					_on(document, 'mousemove', _this._onTouchMove);
					_on(document, 'mouseup', _this._onDrop);
				}

				_this._loopId = setInterval(_this._emulateDragOver, 50);
			}
			else {
				if (dataTransfer) {
					dataTransfer.effectAllowed = 'move';
					options.setData && options.setData.call(_this, dataTransfer, dragEl);
				}

				_on(document, 'drop', _this);

				// #1143: Бывает элемент с IFrame внутри блокирует `drop`,
				// поэтому если вызвался `mouseover`, значит надо отменять весь d'n'd.
				// Breaking Chrome 62+
				// _on(document, 'mouseover', _this);

				_this._dragStartId = _nextTick(_this._dragStarted);
			}
		},

		_onDragOver: function (/**Event*/evt) {
			var el = this.el,
				target,
				dragRect,
				targetRect,
				revert,
				options = this.options,
				group = options.group,
				activeSortable = Sortable.active,
				isOwner = (activeGroup === group),
				isMovingBetweenSortable = false,
				canSort = options.sort;

			if (evt.preventDefault !== void 0) {
				evt.preventDefault();
				!options.dragoverBubble && evt.stopPropagation();
			}

			if (dragEl.animated) {
				return;
			}

			moved = true;

			if (activeSortable && !options.disabled &&
				(isOwner
					? canSort || (revert = !rootEl.contains(dragEl)) // Reverting item into the original list
					: (
						putSortable === this ||
						(
							(activeSortable.lastPullMode = activeGroup.checkPull(this, activeSortable, dragEl, evt)) &&
							group.checkPut(this, activeSortable, dragEl, evt)
						)
					)
				) &&
				(evt.rootEl === void 0 || evt.rootEl === this.el) // touch fallback
			) {
				// Smart auto-scrolling
				_autoScroll(evt, options, this.el);

				if (_silent) {
					return;
				}

				target = _closest(evt.target, options.draggable, el);
				dragRect = dragEl.getBoundingClientRect();

				if (putSortable !== this) {
					putSortable = this;
					isMovingBetweenSortable = true;
				}

				if (revert) {
					_cloneHide(activeSortable, true);
					parentEl = rootEl; // actualization

					if (cloneEl || nextEl) {
						rootEl.insertBefore(dragEl, cloneEl || nextEl);
					}
					else if (!canSort) {
						rootEl.appendChild(dragEl);
					}

					return;
				}


				if ((el.children.length === 0) || (el.children[0] === ghostEl) ||
					(el === evt.target) && (_ghostIsLast(el, evt))
				) {
					//assign target only if condition is true
					if (el.children.length !== 0 && el.children[0] !== ghostEl && el === evt.target) {
						target = el.lastElementChild;
					}

					if (target) {
						if (target.animated) {
							return;
						}

						targetRect = target.getBoundingClientRect();
					}

					_cloneHide(activeSortable, isOwner);

					if (_onMove(rootEl, el, dragEl, dragRect, target, targetRect, evt) !== false) {
						if (!dragEl.contains(el)) {
							el.appendChild(dragEl);
							parentEl = el; // actualization
						}

						this._animate(dragRect, dragEl);
						target && this._animate(targetRect, target);
					}
				}
				else if (target && !target.animated && target !== dragEl && (target.parentNode[expando] !== void 0)) {
					if (lastEl !== target) {
						lastEl = target;
						lastCSS = _css(target);
						lastParentCSS = _css(target.parentNode);
					}

					targetRect = target.getBoundingClientRect();

					var width = targetRect.right - targetRect.left,
						height = targetRect.bottom - targetRect.top,
						floating = R_FLOAT.test(lastCSS.cssFloat + lastCSS.display)
							|| (lastParentCSS.display == 'flex' && lastParentCSS['flex-direction'].indexOf('row') === 0),
						isWide = (target.offsetWidth > dragEl.offsetWidth),
						isLong = (target.offsetHeight > dragEl.offsetHeight),
						halfway = (floating ? (evt.clientX - targetRect.left) / width : (evt.clientY - targetRect.top) / height) > 0.5,
						nextSibling = target.nextElementSibling,
						after = false
					;

					if (floating) {
						var elTop = dragEl.offsetTop,
							tgTop = target.offsetTop;

						if (elTop === tgTop) {
							after = (target.previousElementSibling === dragEl) && !isWide || halfway && isWide;
						}
						else if (target.previousElementSibling === dragEl || dragEl.previousElementSibling === target) {
							after = (evt.clientY - targetRect.top) / height > 0.5;
						} else {
							after = tgTop > elTop;
						}
						} else if (!isMovingBetweenSortable) {
						after = (nextSibling !== dragEl) && !isLong || halfway && isLong;
					}

					var moveVector = _onMove(rootEl, el, dragEl, dragRect, target, targetRect, evt, after);

					if (moveVector !== false) {
						if (moveVector === 1 || moveVector === -1) {
							after = (moveVector === 1);
						}

						_silent = true;
						setTimeout(_unsilent, 30);

						_cloneHide(activeSortable, isOwner);

						if (!dragEl.contains(el)) {
							if (after && !nextSibling) {
								el.appendChild(dragEl);
							} else {
								target.parentNode.insertBefore(dragEl, after ? nextSibling : target);
							}
						}

						parentEl = dragEl.parentNode; // actualization

						this._animate(dragRect, dragEl);
						this._animate(targetRect, target);
					}
				}
			}
		},

		_animate: function (prevRect, target) {
			var ms = this.options.animation;

			if (ms) {
				var currentRect = target.getBoundingClientRect();

				if (prevRect.nodeType === 1) {
					prevRect = prevRect.getBoundingClientRect();
				}

				_css(target, 'transition', 'none');
				_css(target, 'transform', 'translate3d('
					+ (prevRect.left - currentRect.left) + 'px,'
					+ (prevRect.top - currentRect.top) + 'px,0)'
				);

				target.offsetWidth; // repaint

				_css(target, 'transition', 'all ' + ms + 'ms');
				_css(target, 'transform', 'translate3d(0,0,0)');

				clearTimeout(target.animated);
				target.animated = setTimeout(function () {
					_css(target, 'transition', '');
					_css(target, 'transform', '');
					target.animated = false;
				}, ms);
			}
		},

		_offUpEvents: function () {
			var ownerDocument = this.el.ownerDocument;

			_off(document, 'touchmove', this._onTouchMove);
			_off(document, 'pointermove', this._onTouchMove);
			_off(ownerDocument, 'mouseup', this._onDrop);
			_off(ownerDocument, 'touchend', this._onDrop);
			_off(ownerDocument, 'pointerup', this._onDrop);
			_off(ownerDocument, 'touchcancel', this._onDrop);
			_off(ownerDocument, 'pointercancel', this._onDrop);
			_off(ownerDocument, 'selectstart', this);
		},

		_onDrop: function (/**Event*/evt) {
			var el = this.el,
				options = this.options;

			clearInterval(this._loopId);
			clearInterval(autoScroll.pid);
			clearTimeout(this._dragStartTimer);

			_cancelNextTick(this._cloneId);
			_cancelNextTick(this._dragStartId);

			// Unbind events
			_off(document, 'mouseover', this);
			_off(document, 'mousemove', this._onTouchMove);

			if (this.nativeDraggable) {
				_off(document, 'drop', this);
				_off(el, 'dragstart', this._onDragStart);
			}

			this._offUpEvents();

			if (evt) {
				if (moved) {
					evt.preventDefault();
					!options.dropBubble && evt.stopPropagation();
				}

				ghostEl && ghostEl.parentNode && ghostEl.parentNode.removeChild(ghostEl);

				if (rootEl === parentEl || Sortable.active.lastPullMode !== 'clone') {
					// Remove clone
					cloneEl && cloneEl.parentNode && cloneEl.parentNode.removeChild(cloneEl);
				}

				if (dragEl) {
					if (this.nativeDraggable) {
						_off(dragEl, 'dragend', this);
					}

					_disableDraggable(dragEl);
					dragEl.style['will-change'] = '';

					// Remove class's
					_toggleClass(dragEl, this.options.ghostClass, false);
					_toggleClass(dragEl, this.options.chosenClass, false);

					// Drag stop event
					_dispatchEvent(this, rootEl, 'unchoose', dragEl, parentEl, rootEl, oldIndex);

					if (rootEl !== parentEl) {
						newIndex = _index(dragEl, options.draggable);

						if (newIndex >= 0) {
							// Add event
							_dispatchEvent(null, parentEl, 'add', dragEl, parentEl, rootEl, oldIndex, newIndex);

							// Remove event
							_dispatchEvent(this, rootEl, 'remove', dragEl, parentEl, rootEl, oldIndex, newIndex);

							// drag from one list and drop into another
							_dispatchEvent(null, parentEl, 'sort', dragEl, parentEl, rootEl, oldIndex, newIndex);
							_dispatchEvent(this, rootEl, 'sort', dragEl, parentEl, rootEl, oldIndex, newIndex);
						}
					}
					else {
						if (dragEl.nextSibling !== nextEl) {
							// Get the index of the dragged element within its parent
							newIndex = _index(dragEl, options.draggable);

							if (newIndex >= 0) {
								// drag & drop within the same list
								_dispatchEvent(this, rootEl, 'update', dragEl, parentEl, rootEl, oldIndex, newIndex);
								_dispatchEvent(this, rootEl, 'sort', dragEl, parentEl, rootEl, oldIndex, newIndex);
							}
						}
					}

					if (Sortable.active) {
						/* jshint eqnull:true */
						if (newIndex == null || newIndex === -1) {
							newIndex = oldIndex;
						}

						_dispatchEvent(this, rootEl, 'end', dragEl, parentEl, rootEl, oldIndex, newIndex);

						// Save sorting
						this.save();
					}
				}

			}

			this._nulling();
		},

		_nulling: function() {
			rootEl =
			dragEl =
			parentEl =
			ghostEl =
			nextEl =
			cloneEl =
			lastDownEl =

			scrollEl =
			scrollParentEl =

			tapEvt =
			touchEvt =

			moved =
			newIndex =

			lastEl =
			lastCSS =

			putSortable =
			activeGroup =
			Sortable.active = null;

			savedInputChecked.forEach(function (el) {
				el.checked = true;
			});
			savedInputChecked.length = 0;
		},

		handleEvent: function (/**Event*/evt) {
			switch (evt.type) {
				case 'drop':
				case 'dragend':
					this._onDrop(evt);
					break;

				case 'dragover':
				case 'dragenter':
					if (dragEl) {
						this._onDragOver(evt);
						_globalDragOver(evt);
					}
					break;

				case 'mouseover':
					this._onDrop(evt);
					break;

				case 'selectstart':
					evt.preventDefault();
					break;
			}
		},


		/**
		 * Serializes the item into an array of string.
		 * @returns {String[]}
		 */
		toArray: function () {
			var order = [],
				el,
				children = this.el.children,
				i = 0,
				n = children.length,
				options = this.options;

			for (; i < n; i++) {
				el = children[i];
				if (_closest(el, options.draggable, this.el)) {
					order.push(el.getAttribute(options.dataIdAttr) || _generateId(el));
				}
			}

			return order;
		},


		/**
		 * Sorts the elements according to the array.
		 * @param  {String[]}  order  order of the items
		 */
		sort: function (order) {
			var items = {}, rootEl = this.el;

			this.toArray().forEach(function (id, i) {
				var el = rootEl.children[i];

				if (_closest(el, this.options.draggable, rootEl)) {
					items[id] = el;
				}
			}, this);

			order.forEach(function (id) {
				if (items[id]) {
					rootEl.removeChild(items[id]);
					rootEl.appendChild(items[id]);
				}
			});
		},


		/**
		 * Save the current sorting
		 */
		save: function () {
			var store = this.options.store;
			store && store.set(this);
		},


		/**
		 * For each element in the set, get the first element that matches the selector by testing the element itself and traversing up through its ancestors in the DOM tree.
		 * @param   {HTMLElement}  el
		 * @param   {String}       [selector]  default: `options.draggable`
		 * @returns {HTMLElement|null}
		 */
		closest: function (el, selector) {
			return _closest(el, selector || this.options.draggable, this.el);
		},


		/**
		 * Set/get option
		 * @param   {string} name
		 * @param   {*}      [value]
		 * @returns {*}
		 */
		option: function (name, value) {
			var options = this.options;

			if (value === void 0) {
				return options[name];
			} else {
				options[name] = value;

				if (name === 'group') {
					_prepareGroup(options);
				}
			}
		},


		/**
		 * Destroy
		 */
		destroy: function () {
			var el = this.el;

			el[expando] = null;

			_off(el, 'mousedown', this._onTapStart);
			_off(el, 'touchstart', this._onTapStart);
			_off(el, 'pointerdown', this._onTapStart);

			if (this.nativeDraggable) {
				_off(el, 'dragover', this);
				_off(el, 'dragenter', this);
			}

			// Remove draggable attributes
			Array.prototype.forEach.call(el.querySelectorAll('[draggable]'), function (el) {
				el.removeAttribute('draggable');
			});

			touchDragOverListeners.splice(touchDragOverListeners.indexOf(this._onDragOver), 1);

			this._onDrop();

			this.el = el = null;
		}
	};


	function _cloneHide(sortable, state) {
		if (sortable.lastPullMode !== 'clone') {
			state = true;
		}

		if (cloneEl && (cloneEl.state !== state)) {
			_css(cloneEl, 'display', state ? 'none' : '');

			if (!state) {
				if (cloneEl.state) {
					if (sortable.options.group.revertClone) {
						rootEl.insertBefore(cloneEl, nextEl);
						sortable._animate(dragEl, cloneEl);
					} else {
						rootEl.insertBefore(cloneEl, dragEl);
					}
				}
			}

			cloneEl.state = state;
		}
	}


	function _closest(/**HTMLElement*/el, /**String*/selector, /**HTMLElement*/ctx) {
		if (el) {
			ctx = ctx || document;

			do {
				if ((selector === '>*' && el.parentNode === ctx) || _matches(el, selector)) {
					return el;
				}
				/* jshint boss:true */
			} while (el = _getParentOrHost(el));
		}

		return null;
	}


	function _getParentOrHost(el) {
		var parent = el.host;

		return (parent && parent.nodeType) ? parent : el.parentNode;
	}


	function _globalDragOver(/**Event*/evt) {
		if (evt.dataTransfer) {
			evt.dataTransfer.dropEffect = 'move';
		}
		evt.preventDefault();
	}


	function _on(el, event, fn) {
		el.addEventListener(event, fn, captureMode);
	}


	function _off(el, event, fn) {
		el.removeEventListener(event, fn, captureMode);
	}


	function _toggleClass(el, name, state) {
		if (el) {
			if (el.classList) {
				el.classList[state ? 'add' : 'remove'](name);
			}
			else {
				var className = (' ' + el.className + ' ').replace(R_SPACE, ' ').replace(' ' + name + ' ', ' ');
				el.className = (className + (state ? ' ' + name : '')).replace(R_SPACE, ' ');
			}
		}
	}


	function _css(el, prop, val) {
		var style = el && el.style;

		if (style) {
			if (val === void 0) {
				if (document.defaultView && document.defaultView.getComputedStyle) {
					val = document.defaultView.getComputedStyle(el, '');
				}
				else if (el.currentStyle) {
					val = el.currentStyle;
				}

				return prop === void 0 ? val : val[prop];
			}
			else {
				if (!(prop in style)) {
					prop = '-webkit-' + prop;
				}

				style[prop] = val + (typeof val === 'string' ? '' : 'px');
			}
		}
	}


	function _find(ctx, tagName, iterator) {
		if (ctx) {
			var list = ctx.getElementsByTagName(tagName), i = 0, n = list.length;

			if (iterator) {
				for (; i < n; i++) {
					iterator(list[i], i);
				}
			}

			return list;
		}

		return [];
	}



	function _dispatchEvent(sortable, rootEl, name, targetEl, toEl, fromEl, startIndex, newIndex) {
		sortable = (sortable || rootEl[expando]);

		var evt = document.createEvent('Event'),
			options = sortable.options,
			onName = 'on' + name.charAt(0).toUpperCase() + name.substr(1);

		evt.initEvent(name, true, true);

		evt.to = toEl || rootEl;
		evt.from = fromEl || rootEl;
		evt.item = targetEl || rootEl;
		evt.clone = cloneEl;

		evt.oldIndex = startIndex;
		evt.newIndex = newIndex;

		rootEl.dispatchEvent(evt);

		if (options[onName]) {
			options[onName].call(sortable, evt);
		}
	}


	function _onMove(fromEl, toEl, dragEl, dragRect, targetEl, targetRect, originalEvt, willInsertAfter) {
		var evt,
			sortable = fromEl[expando],
			onMoveFn = sortable.options.onMove,
			retVal;

		evt = document.createEvent('Event');
		evt.initEvent('move', true, true);

		evt.to = toEl;
		evt.from = fromEl;
		evt.dragged = dragEl;
		evt.draggedRect = dragRect;
		evt.related = targetEl || toEl;
		evt.relatedRect = targetRect || toEl.getBoundingClientRect();
		evt.willInsertAfter = willInsertAfter;

		fromEl.dispatchEvent(evt);

		if (onMoveFn) {
			retVal = onMoveFn.call(sortable, evt, originalEvt);
		}

		return retVal;
	}


	function _disableDraggable(el) {
		el.draggable = false;
	}


	function _unsilent() {
		_silent = false;
	}


	/** @returns {HTMLElement|false} */
	function _ghostIsLast(el, evt) {
		var lastEl = el.lastElementChild,
			rect = lastEl.getBoundingClientRect();

		// 5 — min delta
		// abs — нельзя добавлять, а то глюки при наведении сверху
		return (evt.clientY - (rect.top + rect.height) > 5) ||
			(evt.clientX - (rect.left + rect.width) > 5);
	}


	/**
	 * Generate id
	 * @param   {HTMLElement} el
	 * @returns {String}
	 * @private
	 */
	function _generateId(el) {
		var str = el.tagName + el.className + el.src + el.href + el.textContent,
			i = str.length,
			sum = 0;

		while (i--) {
			sum += str.charCodeAt(i);
		}

		return sum.toString(36);
	}

	/**
	 * Returns the index of an element within its parent for a selected set of
	 * elements
	 * @param  {HTMLElement} el
	 * @param  {selector} selector
	 * @return {number}
	 */
	function _index(el, selector) {
		var index = 0;

		if (!el || !el.parentNode) {
			return -1;
		}

		while (el && (el = el.previousElementSibling)) {
			if ((el.nodeName.toUpperCase() !== 'TEMPLATE') && (selector === '>*' || _matches(el, selector))) {
				index++;
			}
		}

		return index;
	}

	function _matches(/**HTMLElement*/el, /**String*/selector) {
		if (el) {
			selector = selector.split('.');

			var tag = selector.shift().toUpperCase(),
				re = new RegExp('\\s(' + selector.join('|') + ')(?=\\s)', 'g');

			return (
				(tag === '' || el.nodeName.toUpperCase() == tag) &&
				(!selector.length || ((' ' + el.className + ' ').match(re) || []).length == selector.length)
			);
		}

		return false;
	}

	function _throttle(callback, ms) {
		var args, _this;

		return function () {
			if (args === void 0) {
				args = arguments;
				_this = this;

				setTimeout(function () {
					if (args.length === 1) {
						callback.call(_this, args[0]);
					} else {
						callback.apply(_this, args);
					}

					args = void 0;
				}, ms);
			}
		};
	}

	function _extend(dst, src) {
		if (dst && src) {
			for (var key in src) {
				if (src.hasOwnProperty(key)) {
					dst[key] = src[key];
				}
			}
		}

		return dst;
	}

	function _clone(el) {
		if (Polymer && Polymer.dom) {
			return Polymer.dom(el).cloneNode(true);
		}
		else if ($) {
			return $(el).clone(true)[0];
		}
		else {
			return el.cloneNode(true);
		}
	}

	function _saveInputCheckedState(root) {
		var inputs = root.getElementsByTagName('input');
		var idx = inputs.length;

		while (idx--) {
			var el = inputs[idx];
			el.checked && savedInputChecked.push(el);
		}
	}

	function _nextTick(fn) {
		return setTimeout(fn, 0);
	}

	function _cancelNextTick(id) {
		return clearTimeout(id);
	}

	// Fixed #973:
	_on(document, 'touchmove', function (evt) {
		if (Sortable.active) {
			evt.preventDefault();
		}
	});

	// Export utils
	Sortable.utils = {
		on: _on,
		off: _off,
		css: _css,
		find: _find,
		is: function (el, selector) {
			return !!_closest(el, selector, el);
		},
		extend: _extend,
		throttle: _throttle,
		closest: _closest,
		toggleClass: _toggleClass,
		clone: _clone,
		index: _index,
		nextTick: _nextTick,
		cancelNextTick: _cancelNextTick
	};


	/**
	 * Create sortable instance
	 * @param {HTMLElement}  el
	 * @param {Object}      [options]
	 */
	Sortable.create = function (el, options) {
		return new Sortable(el, options);
	};


	// Export
	Sortable.version = '1.7.0';
	return Sortable;
});

},{}],82:[function(require,module,exports){
/*!
 * vue-carousel-3d v0.1.20
 * (c) 2018 Vladimir Bujanovic
 * https://github.com/wlada/vue-carousel-3d#readme
 */
!function(t,e){"object"==typeof exports&&"object"==typeof module?module.exports=e():"function"==typeof define&&define.amd?define([],e):"object"==typeof exports?exports.Carousel3d=e():t.Carousel3d=e()}(this,function(){return function(t){function e(i){if(n[i])return n[i].exports;var r=n[i]={exports:{},id:i,loaded:!1};return t[i].call(r.exports,r,r.exports,e),r.loaded=!0,r.exports}var n={};return e.m=t,e.c=n,e.p="",e(0)}([function(t,e,n){"use strict";function i(t){return t&&t.__esModule?t:{default:t}}Object.defineProperty(e,"__esModule",{value:!0}),e.Slide=e.Carousel3d=void 0;var r=n(1),o=i(r),s=n(15),a=i(s),u=function(t){t.component("carousel3d",o.default),t.component("slide",a.default)};e.default={install:u},e.Carousel3d=o.default,e.Slide=a.default},function(t,e,n){n(2);var i=n(7)(n(8),n(57),"data-v-c06c963c",null);t.exports=i.exports},function(t,e,n){var i=n(3);"string"==typeof i&&(i=[[t.id,i,""]]),i.locals&&(t.exports=i.locals);n(5)("e749a8c4",i,!0)},function(t,e,n){e=t.exports=n(4)(),e.push([t.id,".carousel-3d-container[data-v-c06c963c]{min-height:1px;width:100%;position:relative;z-index:0;overflow:hidden;margin:20px auto;box-sizing:border-box}.carousel-3d-slider[data-v-c06c963c]{position:relative;margin:0 auto;transform-style:preserve-3d;-webkit-perspective:1000px;-moz-perspective:1000px;perspective:1000px}",""])},function(t,e){t.exports=function(){var t=[];return t.toString=function(){for(var t=[],e=0;e<this.length;e++){var n=this[e];n[2]?t.push("@media "+n[2]+"{"+n[1]+"}"):t.push(n[1])}return t.join("")},t.i=function(e,n){"string"==typeof e&&(e=[[null,e,""]]);for(var i={},r=0;r<this.length;r++){var o=this[r][0];"number"==typeof o&&(i[o]=!0)}for(r=0;r<e.length;r++){var s=e[r];"number"==typeof s[0]&&i[s[0]]||(n&&!s[2]?s[2]=n:n&&(s[2]="("+s[2]+") and ("+n+")"),t.push(s))}},t}},function(t,e,n){function i(t){for(var e=0;e<t.length;e++){var n=t[e],i=c[n.id];if(i){i.refs++;for(var r=0;r<i.parts.length;r++)i.parts[r](n.parts[r]);for(;r<n.parts.length;r++)i.parts.push(o(n.parts[r]));i.parts.length>n.parts.length&&(i.parts.length=n.parts.length)}else{for(var s=[],r=0;r<n.parts.length;r++)s.push(o(n.parts[r]));c[n.id]={id:n.id,refs:1,parts:s}}}}function r(){var t=document.createElement("style");return t.type="text/css",d.appendChild(t),t}function o(t){var e,n,i=document.querySelector('style[data-vue-ssr-id~="'+t.id+'"]');if(i){if(f)return v;i.parentNode.removeChild(i)}if(x){var o=p++;i=h||(h=r()),e=s.bind(null,i,o,!1),n=s.bind(null,i,o,!0)}else i=r(),e=a.bind(null,i),n=function(){i.parentNode.removeChild(i)};return e(t),function(i){if(i){if(i.css===t.css&&i.media===t.media&&i.sourceMap===t.sourceMap)return;e(t=i)}else n()}}function s(t,e,n,i){var r=n?"":i.css;if(t.styleSheet)t.styleSheet.cssText=m(e,r);else{var o=document.createTextNode(r),s=t.childNodes;s[e]&&t.removeChild(s[e]),s.length?t.insertBefore(o,s[e]):t.appendChild(o)}}function a(t,e){var n=e.css,i=e.media,r=e.sourceMap;if(i&&t.setAttribute("media",i),r&&(n+="\n/*# sourceURL="+r.sources[0]+" */",n+="\n/*# sourceMappingURL=data:application/json;base64,"+btoa(unescape(encodeURIComponent(JSON.stringify(r))))+" */"),t.styleSheet)t.styleSheet.cssText=n;else{for(;t.firstChild;)t.removeChild(t.firstChild);t.appendChild(document.createTextNode(n))}}var u="undefined"!=typeof document,l=n(6),c={},d=u&&(document.head||document.getElementsByTagName("head")[0]),h=null,p=0,f=!1,v=function(){},x="undefined"!=typeof navigator&&/msie [6-9]\b/.test(navigator.userAgent.toLowerCase());t.exports=function(t,e,n){f=n;var r=l(t,e);return i(r),function(e){for(var n=[],o=0;o<r.length;o++){var s=r[o],a=c[s.id];a.refs--,n.push(a)}e?(r=l(t,e),i(r)):r=[];for(var o=0;o<n.length;o++){var a=n[o];if(0===a.refs){for(var u=0;u<a.parts.length;u++)a.parts[u]();delete c[a.id]}}}};var m=function(){var t=[];return function(e,n){return t[e]=n,t.filter(Boolean).join("\n")}}()},function(t,e){t.exports=function(t,e){for(var n=[],i={},r=0;r<e.length;r++){var o=e[r],s=o[0],a=o[1],u=o[2],l=o[3],c={id:t+":"+r,css:a,media:u,sourceMap:l};i[s]?i[s].parts.push(c):n.push(i[s]={id:s,parts:[c]})}return n}},function(t,e){t.exports=function(t,e,n,i){var r,o=t=t||{},s=typeof t.default;"object"!==s&&"function"!==s||(r=t,o=t.default);var a="function"==typeof o?o.options:o;if(e&&(a.render=e.render,a.staticRenderFns=e.staticRenderFns),n&&(a._scopeId=n),i){var u=a.computed||(a.computed={});Object.keys(i).forEach(function(t){var e=i[t];u[t]=function(){return e}})}return{esModule:r,exports:o,options:a}}},function(t,e,n){"use strict";function i(t){return t&&t.__esModule?t:{default:t}}Object.defineProperty(e,"__esModule",{value:!0});var r=n(9),o=i(r),s=n(10),a=i(s),u=n(15),l=i(u),c=function(){};e.default={name:"carousel3d",components:{Controls:a.default,Slide:l.default},props:{count:{type:[Number,String],default:0},perspective:{type:[Number,String],default:35},display:{type:[Number,String],default:5},loop:{type:Boolean,default:!0},animationSpeed:{type:[Number,String],default:500},dir:{type:String,default:"rtl"},width:{type:[Number,String],default:360},height:{type:[Number,String],default:270},border:{type:[Number,String],default:1},space:{type:[Number,String],default:"auto"},startIndex:{type:[Number,String],default:0},clickable:{type:Boolean,default:!0},disable3d:{type:Boolean,default:!1},minSwipeDistance:{type:Number,default:10},inverseScaling:{type:[Number,String],default:300},controlsVisible:{type:Boolean,default:!1},controlsPrevHtml:{type:String,default:"&lsaquo;"},controlsNextHtml:{type:String,default:"&rsaquo;"},controlsWidth:{type:[String,Number],default:50},controlsHeight:{type:[String,Number],default:50},onLastSlide:{type:Function,default:c},onSlideChange:{type:Function,default:c},bias:{type:String,default:"left"},onMainSlideClick:{type:Function,default:c}},data:function(){return{viewport:0,currentIndex:0,total:0,lock:!1,dragOffset:0,dragStartX:0,mousedown:!1,zIndex:998}},mixins:[o.default],watch:{count:function(){this.computeData()}},computed:{isLastSlide:function(){return this.currentIndex===this.total-1},isFirstSlide:function(){return 0===this.currentIndex},isNextPossible:function(){return!(!this.loop&&this.isLastSlide)},isPrevPossible:function(){return!(!this.loop&&this.isFirstSlide)},slideWidth:function(){var t=this.viewport,e=parseInt(this.width)+2*parseInt(this.border,10);return t<e?t:e},slideHeight:function(){var t=parseInt(this.width,10)+2*parseInt(this.border,10),e=parseInt(parseInt(this.height)+2*this.border,10),n=this.calculateAspectRatio(t,e);return this.slideWidth/n},visible:function(){var t=this.display>this.total?this.total:this.display;return t},hasHiddenSlides:function(){return this.total>this.visible},leftIndices:function(){var t=(this.visible-1)/2;t="left"===this.bias.toLowerCase()?Math.ceil(t):Math.floor(t);for(var e=[],n=1;n<=t;n++)e.push("ltr"===this.dir?(this.currentIndex+n)%this.total:(this.currentIndex-n)%this.total);return e},rightIndices:function(){var t=(this.visible-1)/2;t="right"===this.bias.toLowerCase()?Math.ceil(t):Math.floor(t);for(var e=[],n=1;n<=t;n++)e.push("ltr"===this.dir?(this.currentIndex-n)%this.total:(this.currentIndex+n)%this.total);return e},leftOutIndex:function(){var t=(this.visible-1)/2;return t="left"===this.bias.toLowerCase()?Math.ceil(t):Math.floor(t),t++,"ltr"===this.dir?this.total-this.currentIndex-t<=0?-parseInt(this.total-this.currentIndex-t):this.currentIndex+t:this.currentIndex-t},rightOutIndex:function(){var t=(this.visible-1)/2;return t="right"===this.bias.toLowerCase()?Math.ceil(t):Math.floor(t),t++,"ltr"===this.dir?this.currentIndex-t:this.total-this.currentIndex-t<=0?-parseInt(this.total-this.currentIndex-t,10):this.currentIndex+t}},methods:{goNext:function(){this.isNextPossible&&(this.isLastSlide?this.goSlide(0):this.goSlide(this.currentIndex+1))},goPrev:function(){this.isPrevPossible&&(this.isFirstSlide?this.goSlide(this.total-1):this.goSlide(this.currentIndex-1))},goSlide:function(t){var e=this;this.currentIndex=t<0||t>this.total-1?0:t,this.lock=!0,this.isLastSlide&&(this.onLastSlide!==c&&console.warn("onLastSlide deprecated, please use @last-slide"),this.onLastSlide(this.currentIndex),this.$emit("last-slide",this.currentIndex)),this.$emit("before-slide-change",this.currentIndex),setTimeout(function(){return e.animationEnd()},this.animationSpeed)},goFar:function(t){var e=this,n=t===this.total-1&&this.isFirstSlide?-1:t-this.currentIndex;this.isLastSlide&&0===t&&(n=1);for(var i=n<0?-n:n,r=0,o=0;o<i;){o+=1;var s=1===i?0:r;setTimeout(function(){return n<0?e.goPrev(i):e.goNext(i)},s),r+=this.animationSpeed/i}},animationEnd:function(){this.lock=!1,this.onSlideChange!==c&&console.warn("onSlideChange deprecated, please use @after-slide-change"),this.onSlideChange(this.currentIndex),this.$emit("after-slide-change",this.currentIndex)},handleMouseup:function(){this.mousedown=!1,this.dragOffset=0},handleMousedown:function(t){t.touches||t.preventDefault(),this.mousedown=!0,this.dragStartX="ontouchstart"in window?t.touches[0].clientX:t.clientX},handleMousemove:function(t){if(this.mousedown){var e="ontouchstart"in window?t.touches[0].clientX:t.clientX,n=this.dragStartX-e;this.dragOffset=n,this.dragOffset>this.minSwipeDistance?(this.handleMouseup(),this.goNext()):this.dragOffset<-this.minSwipeDistance&&(this.handleMouseup(),this.goPrev())}},attachMutationObserver:function(){var t=this,e=window.MutationObserver||window.WebKitMutationObserver||window.MozMutationObserver;if(e){var n={attributes:!0,childList:!0,characterData:!0};this.mutationObserver=new e(function(){t.$nextTick(function(){t.computeData()})}),this.$el&&this.mutationObserver.observe(this.$el,n)}},detachMutationObserver:function(){this.mutationObserver&&this.mutationObserver.disconnect()},getSlideCount:function(){return void 0!==this.$slots.default?this.$slots.default.filter(function(t){return void 0!==t.tag}).length:0},calculateAspectRatio:function(t,e){return Math.min(t/e)},computeData:function(t){this.total=this.getSlideCount(),(t||this.currentIndex>=this.total)&&(this.currentIndex=parseInt(this.startIndex)>this.total-1?this.total-1:parseInt(this.startIndex)),this.viewport=this.$el.clientWidth},setSize:function(){this.$el.style.cssText+="height:"+this.slideHeight+"px;",this.$el.childNodes[0].style.cssText+="width:"+this.slideWidth+"px; height:"+this.slideHeight+"px;"}},mounted:function(){this.computeData(!0),this.attachMutationObserver(),this.$isServer||(window.addEventListener("resize",this.setSize),"ontouchstart"in window?(this.$el.addEventListener("touchstart",this.handleMousedown),this.$el.addEventListener("touchend",this.handleMouseup),this.$el.addEventListener("touchmove",this.handleMousemove)):(this.$el.addEventListener("mousedown",this.handleMousedown),this.$el.addEventListener("mouseup",this.handleMouseup),this.$el.addEventListener("mousemove",this.handleMousemove)))},beforeDestroy:function(){this.$isServer||(this.detachMutationObserver(),"ontouchstart"in window?this.$el.removeEventListener("touchmove",this.handleMousemove):this.$el.removeEventListener("mousemove",this.handleMousemove),window.removeEventListener("resize",this.setSize))}}},function(t,e){"use strict";Object.defineProperty(e,"__esModule",{value:!0});var n={props:{autoplay:{type:Boolean,default:!1},autoplayTimeout:{type:Number,default:2e3},autoplayHoverPause:{type:Boolean,default:!0}},data:function(){return{autoplayInterval:null}},destroyed:function(){this.pauseAutoplay(),this.$isServer||(this.$el.removeEventListener("mouseenter",this.pauseAutoplay),this.$el.removeEventListener("mouseleave",this.startAutoplay))},methods:{pauseAutoplay:function(){this.autoplayInterval&&(this.autoplayInterval=clearInterval(this.autoplayInterval))},startAutoplay:function(){var t=this;this.autoplay&&(this.autoplayInterval=setInterval(function(){"ltr"===t.dir?t.goPrev():t.goNext()},this.autoplayTimeout))}},mounted:function(){!this.$isServer&&this.autoplayHoverPause&&(this.$el.addEventListener("mouseenter",this.pauseAutoplay),this.$el.addEventListener("mouseleave",this.startAutoplay)),this.startAutoplay()}};e.default=n},function(t,e,n){n(11);var i=n(7)(n(13),n(14),"data-v-43e93932",null);t.exports=i.exports},function(t,e,n){var i=n(12);"string"==typeof i&&(i=[[t.id,i,""]]),i.locals&&(t.exports=i.locals);n(5)("06c66230",i,!0)},function(t,e,n){e=t.exports=n(4)(),e.push([t.id,".carousel-3d-controls[data-v-43e93932]{position:absolute;top:50%;height:0;margin-top:-30px;left:0;width:100%;z-index:9099}.next[data-v-43e93932],.prev[data-v-43e93932]{width:60px;position:absolute;z-index:9999;font-size:60px;height:60px;line-height:60px;color:#333;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;text-decoration:none;top:0}.next[data-v-43e93932]:hover,.prev[data-v-43e93932]:hover{cursor:pointer;opacity:.7}.prev[data-v-43e93932]{left:10px;text-align:left}.next[data-v-43e93932]{right:10px;text-align:right}.disabled[data-v-43e93932],.disabled[data-v-43e93932]:hover{opacity:.2;cursor:default}",""])},function(t,e){"use strict";Object.defineProperty(e,"__esModule",{value:!0}),e.default={name:"controls",props:{width:{type:[String,Number],default:50},height:{type:[String,Number],default:60},prevHtml:{type:String,default:"&lsaquo;"},nextHtml:{type:String,default:"&rsaquo;"}},data:function(){return{parent:this.$parent}}}},function(t,e){t.exports={render:function(){var t=this,e=t.$createElement,n=t._self._c||e;return n("div",{staticClass:"carousel-3d-controls"},[n("a",{staticClass:"prev",class:{disabled:!t.parent.isPrevPossible},style:"width: "+t.width+"px; height: "+t.height+"px; line-height: "+t.height+"px;",attrs:{href:"#"},on:{click:function(e){e.preventDefault(),t.parent.goPrev()}}},[n("span",{domProps:{innerHTML:t._s(t.prevHtml)}})]),t._v(" "),n("a",{staticClass:"next",class:{disabled:!t.parent.isNextPossible},style:"width: "+t.width+"px; height: "+t.height+"px; line-height: "+t.height+"px;",attrs:{href:"#"},on:{click:function(e){e.preventDefault(),t.parent.goNext()}}},[n("span",{domProps:{innerHTML:t._s(t.nextHtml)}})])])},staticRenderFns:[]}},function(t,e,n){n(16);var i=n(7)(n(18),n(56),null,null);t.exports=i.exports},function(t,e,n){var i=n(17);"string"==typeof i&&(i=[[t.id,i,""]]),i.locals&&(t.exports=i.locals);n(5)("1dbacf8a",i,!0)},function(t,e,n){e=t.exports=n(4)(),e.push([t.id,".carousel-3d-slide{position:absolute;opacity:0;visibility:hidden;overflow:hidden;top:0;border-radius:1px;border-color:#000;border-color:rgba(0,0,0,.4);border-style:solid;background-size:cover;background-color:#ccc;display:block;margin:0;box-sizing:border-box;text-align:left}.carousel-3d-slide img{width:100%}.carousel-3d-slide.current{opacity:1!important;visibility:visible!important;transform:none!important;z-index:999}",""])},function(t,e,n){"use strict";function i(t){return t&&t.__esModule?t:{default:t}}Object.defineProperty(e,"__esModule",{value:!0});var r=n(19),o=i(r);e.default={name:"slide",props:{index:{type:Number}},data:function(){return{parent:this.$parent,styles:{},zIndex:999}},computed:{isCurrent:function(){return this.index===this.parent.currentIndex},slideStyle:function(){var t={};if(!this.isCurrent){var e=this.getSideIndex(this.parent.rightIndices),n=this.getSideIndex(this.parent.leftIndices);(e>=0||n>=0)&&(t=e>=0?this.calculatePosition(e,!0,this.zIndex):this.calculatePosition(n,!1,this.zIndex),t.opacity=1,t.visibility="visible"),this.parent.hasHiddenSlides&&(this.matchIndex(this.parent.leftOutIndex)?t=this.calculatePosition(this.parent.leftIndices.length-1,!1,this.zIndex):this.matchIndex(this.parent.rightOutIndex)&&(t=this.calculatePosition(this.parent.rightIndices.length-1,!0,this.zIndex)))}return(0,o.default)(t,{"border-width":this.parent.border+"px",width:this.parent.slideWidth+"px",height:this.parent.slideHeight+"px",transition:" transform "+this.parent.animationSpeed+"ms,                opacity "+this.parent.animationSpeed+"ms,                visibility "+this.parent.animationSpeed+"ms"})}},methods:{getSideIndex:function(t){var e=this,n=-1;return t.forEach(function(t,i){e.matchIndex(t)&&(n=i)}),n},matchIndex:function(t){return t>=0?this.index===t:this.parent.total+t===this.index},calculatePosition:function(t,e,n){var i=this.parent.disable3d?0:parseInt(this.parent.inverseScaling)+100*(t+1),r=this.parent.disable3d?0:parseInt(this.parent.perspective),o="auto"===this.parent.space?parseInt((t+1)*(this.parent.width/1.5),10):parseInt((t+1)*this.parent.space,10),s=e?"translateX("+o+"px) translateZ(-"+i+"px) rotateY(-"+r+"deg)":"translateX(-"+o+"px) translateZ(-"+i+"px) rotateY("+r+"deg)",a="auto"===this.parent.space?0:parseInt((t+1)*this.parent.space);return{transform:s,top:a,zIndex:n-(Math.abs(t)+1)}},goTo:function(){this.isCurrent?this.parent.onMainSlideClick():this.parent.clickable===!0&&this.parent.goFar(this.index)}}}},function(t,e,n){t.exports={default:n(20),__esModule:!0}},function(t,e,n){n(21),t.exports=n(24).Object.assign},function(t,e,n){var i=n(22);i(i.S+i.F,"Object",{assign:n(37)})},function(t,e,n){var i=n(23),r=n(24),o=n(25),s=n(27),a="prototype",u=function(t,e,n){var l,c,d,h=t&u.F,p=t&u.G,f=t&u.S,v=t&u.P,x=t&u.B,m=t&u.W,g=p?r:r[e]||(r[e]={}),y=g[a],b=p?i:f?i[e]:(i[e]||{})[a];p&&(n=e);for(l in n)c=!h&&b&&void 0!==b[l],c&&l in g||(d=c?b[l]:n[l],g[l]=p&&"function"!=typeof b[l]?n[l]:x&&c?o(d,i):m&&b[l]==d?function(t){var e=function(e,n,i){if(this instanceof t){switch(arguments.length){case 0:return new t;case 1:return new t(e);case 2:return new t(e,n)}return new t(e,n,i)}return t.apply(this,arguments)};return e[a]=t[a],e}(d):v&&"function"==typeof d?o(Function.call,d):d,v&&((g.virtual||(g.virtual={}))[l]=d,t&u.R&&y&&!y[l]&&s(y,l,d)))};u.F=1,u.G=2,u.S=4,u.P=8,u.B=16,u.W=32,u.U=64,u.R=128,t.exports=u},function(t,e){var n=t.exports="undefined"!=typeof window&&window.Math==Math?window:"undefined"!=typeof self&&self.Math==Math?self:Function("return this")();"number"==typeof __g&&(__g=n)},function(t,e){var n=t.exports={version:"2.4.0"};"number"==typeof __e&&(__e=n)},function(t,e,n){var i=n(26);t.exports=function(t,e,n){if(i(t),void 0===e)return t;switch(n){case 1:return function(n){return t.call(e,n)};case 2:return function(n,i){return t.call(e,n,i)};case 3:return function(n,i,r){return t.call(e,n,i,r)}}return function(){return t.apply(e,arguments)}}},function(t,e){t.exports=function(t){if("function"!=typeof t)throw TypeError(t+" is not a function!");return t}},function(t,e,n){var i=n(28),r=n(36);t.exports=n(32)?function(t,e,n){return i.f(t,e,r(1,n))}:function(t,e,n){return t[e]=n,t}},function(t,e,n){var i=n(29),r=n(31),o=n(35),s=Object.defineProperty;e.f=n(32)?Object.defineProperty:function(t,e,n){if(i(t),e=o(e,!0),i(n),r)try{return s(t,e,n)}catch(t){}if("get"in n||"set"in n)throw TypeError("Accessors not supported!");return"value"in n&&(t[e]=n.value),t}},function(t,e,n){var i=n(30);t.exports=function(t){if(!i(t))throw TypeError(t+" is not an object!");return t}},function(t,e){t.exports=function(t){return"object"==typeof t?null!==t:"function"==typeof t}},function(t,e,n){t.exports=!n(32)&&!n(33)(function(){return 7!=Object.defineProperty(n(34)("div"),"a",{get:function(){return 7}}).a})},function(t,e,n){t.exports=!n(33)(function(){return 7!=Object.defineProperty({},"a",{get:function(){return 7}}).a})},function(t,e){t.exports=function(t){try{return!!t()}catch(t){return!0}}},function(t,e,n){var i=n(30),r=n(23).document,o=i(r)&&i(r.createElement);t.exports=function(t){return o?r.createElement(t):{}}},function(t,e,n){var i=n(30);t.exports=function(t,e){if(!i(t))return t;var n,r;if(e&&"function"==typeof(n=t.toString)&&!i(r=n.call(t)))return r;if("function"==typeof(n=t.valueOf)&&!i(r=n.call(t)))return r;if(!e&&"function"==typeof(n=t.toString)&&!i(r=n.call(t)))return r;throw TypeError("Can't convert object to primitive value")}},function(t,e){t.exports=function(t,e){return{enumerable:!(1&t),configurable:!(2&t),writable:!(4&t),value:e}}},function(t,e,n){"use strict";var i=n(38),r=n(53),o=n(54),s=n(55),a=n(42),u=Object.assign;t.exports=!u||n(33)(function(){var t={},e={},n=Symbol(),i="abcdefghijklmnopqrst";return t[n]=7,i.split("").forEach(function(t){e[t]=t}),7!=u({},t)[n]||Object.keys(u({},e)).join("")!=i})?function(t,e){for(var n=s(t),u=arguments.length,l=1,c=r.f,d=o.f;u>l;)for(var h,p=a(arguments[l++]),f=c?i(p).concat(c(p)):i(p),v=f.length,x=0;v>x;)d.call(p,h=f[x++])&&(n[h]=p[h]);return n}:u},function(t,e,n){var i=n(39),r=n(52);t.exports=Object.keys||function(t){return i(t,r)}},function(t,e,n){var i=n(40),r=n(41),o=n(45)(!1),s=n(49)("IE_PROTO");t.exports=function(t,e){var n,a=r(t),u=0,l=[];for(n in a)n!=s&&i(a,n)&&l.push(n);for(;e.length>u;)i(a,n=e[u++])&&(~o(l,n)||l.push(n));return l}},function(t,e){var n={}.hasOwnProperty;t.exports=function(t,e){return n.call(t,e)}},function(t,e,n){var i=n(42),r=n(44);t.exports=function(t){return i(r(t))}},function(t,e,n){var i=n(43);t.exports=Object("z").propertyIsEnumerable(0)?Object:function(t){return"String"==i(t)?t.split(""):Object(t)}},function(t,e){var n={}.toString;t.exports=function(t){return n.call(t).slice(8,-1)}},function(t,e){t.exports=function(t){if(void 0==t)throw TypeError("Can't call method on  "+t);return t}},function(t,e,n){var i=n(41),r=n(46),o=n(48);t.exports=function(t){return function(e,n,s){var a,u=i(e),l=r(u.length),c=o(s,l);if(t&&n!=n){for(;l>c;)if(a=u[c++],a!=a)return!0}else for(;l>c;c++)if((t||c in u)&&u[c]===n)return t||c||0;return!t&&-1}}},function(t,e,n){var i=n(47),r=Math.min;t.exports=function(t){return t>0?r(i(t),9007199254740991):0}},function(t,e){var n=Math.ceil,i=Math.floor;t.exports=function(t){return isNaN(t=+t)?0:(t>0?i:n)(t)}},function(t,e,n){var i=n(47),r=Math.max,o=Math.min;t.exports=function(t,e){return t=i(t),t<0?r(t+e,0):o(t,e)}},function(t,e,n){var i=n(50)("keys"),r=n(51);t.exports=function(t){return i[t]||(i[t]=r(t))}},function(t,e,n){var i=n(23),r="__core-js_shared__",o=i[r]||(i[r]={});t.exports=function(t){return o[t]||(o[t]={})}},function(t,e){var n=0,i=Math.random();t.exports=function(t){return"Symbol(".concat(void 0===t?"":t,")_",(++n+i).toString(36))}},function(t,e){t.exports="constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf".split(",")},function(t,e){e.f=Object.getOwnPropertySymbols},function(t,e){e.f={}.propertyIsEnumerable},function(t,e,n){var i=n(44);t.exports=function(t){return Object(i(t))}},function(t,e){t.exports={render:function(){var t=this,e=t.$createElement,n=t._self._c||e;return n("div",{staticClass:"carousel-3d-slide",class:{current:t.isCurrent},style:t.slideStyle,on:{click:function(e){t.goTo()}}},[t._t("default")],2)},staticRenderFns:[]}},function(t,e){t.exports={render:function(){var t=this,e=t.$createElement,n=t._self._c||e;return n("div",{staticClass:"carousel-3d-container",style:{height:this.slideHeight+"px"}},[n("div",{staticClass:"carousel-3d-slider",style:{width:this.slideWidth+"px",height:this.slideHeight+"px"}},[t._t("default")],2),t._v(" "),t.controlsVisible?n("controls",{attrs:{"next-html":t.controlsNextHtml,"prev-html":t.controlsPrevHtml,width:t.controlsWidth,height:t.controlsHeight}}):t._e()],1)},staticRenderFns:[]}}])});
},{}],83:[function(require,module,exports){
'use strict';

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

(function () {
  "use strict";

  if (!Array.from) {
    Array.from = function (object) {
      return [].slice.call(object);
    };
  }

  function buildAttribute(object, propName, value) {
    if (value == undefined) {
      return object;
    }
    object = object == null ? {} : object;
    object[propName] = value;
    return object;
  }

  function buildDraggable(Sortable) {
    function removeNode(node) {
      node.parentElement.removeChild(node);
    }

    function insertNodeAt(fatherNode, node, position) {
      var refNode = position === 0 ? fatherNode.children[0] : fatherNode.children[position - 1].nextSibling;
      fatherNode.insertBefore(node, refNode);
    }

    function computeVmIndex(vnodes, element) {
      return vnodes.map(function (elt) {
        return elt.elm;
      }).indexOf(element);
    }

    function _computeIndexes(slots, children, isTransition) {
      if (!slots) {
        return [];
      }

      var elmFromNodes = slots.map(function (elt) {
        return elt.elm;
      });
      var rawIndexes = [].concat(_toConsumableArray(children)).map(function (elt) {
        return elmFromNodes.indexOf(elt);
      });
      return isTransition ? rawIndexes.filter(function (ind) {
        return ind !== -1;
      }) : rawIndexes;
    }

    function emit(evtName, evtData) {
      var _this = this;

      this.$nextTick(function () {
        return _this.$emit(evtName.toLowerCase(), evtData);
      });
    }

    function delegateAndEmit(evtName) {
      var _this2 = this;

      return function (evtData) {
        if (_this2.realList !== null) {
          _this2['onDrag' + evtName](evtData);
        }
        emit.call(_this2, evtName, evtData);
      };
    }

    var eventsListened = ['Start', 'Add', 'Remove', 'Update', 'End'];
    var eventsToEmit = ['Choose', 'Sort', 'Filter', 'Clone'];
    var readonlyProperties = ['Move'].concat(eventsListened, eventsToEmit).map(function (evt) {
      return 'on' + evt;
    });
    var draggingElement = null;

    var props = {
      options: Object,
      list: {
        type: Array,
        required: false,
        default: null
      },
      value: {
        type: Array,
        required: false,
        default: null
      },
      noTransitionOnDrag: {
        type: Boolean,
        default: false
      },
      clone: {
        type: Function,
        default: function _default(original) {
          return original;
        }
      },
      element: {
        type: String,
        default: 'div'
      },
      move: {
        type: Function,
        default: null
      },
      componentData: {
        type: Object,
        required: false,
        default: null
      }
    };

    var draggableComponent = {
      name: 'draggable',

      props: props,

      data: function data() {
        return {
          transitionMode: false,
          noneFunctionalComponentMode: false,
          init: false
        };
      },
      render: function render(h) {
        var slots = this.$slots.default;
        if (slots && slots.length === 1) {
          var child = slots[0];
          if (child.componentOptions && child.componentOptions.tag === "transition-group") {
            this.transitionMode = true;
          }
        }
        var children = slots;
        var footer = this.$slots.footer;

        if (footer) {
          children = slots ? [].concat(_toConsumableArray(slots), _toConsumableArray(footer)) : [].concat(_toConsumableArray(footer));
        }
        var attributes = null;
        var update = function update(name, value) {
          attributes = buildAttribute(attributes, name, value);
        };
        update('attrs', this.$attrs);
        if (this.componentData) {
          var _componentData = this.componentData,
              on = _componentData.on,
              _props = _componentData.props;

          update('on', on);
          update('props', _props);
        }
        return h(this.element, attributes, children);
      },
      mounted: function mounted() {
        var _this3 = this;

        this.noneFunctionalComponentMode = this.element.toLowerCase() !== this.$el.nodeName.toLowerCase();
        if (this.noneFunctionalComponentMode && this.transitionMode) {
          throw new Error('Transition-group inside component is not supported. Please alter element value or remove transition-group. Current element value: ' + this.element);
        }
        var optionsAdded = {};
        eventsListened.forEach(function (elt) {
          optionsAdded['on' + elt] = delegateAndEmit.call(_this3, elt);
        });

        eventsToEmit.forEach(function (elt) {
          optionsAdded['on' + elt] = emit.bind(_this3, elt);
        });

        var options = _extends({}, this.options, optionsAdded, { onMove: function onMove(evt, originalEvent) {
            return _this3.onDragMove(evt, originalEvent);
          } });
        !('draggable' in options) && (options.draggable = '>*');
        this._sortable = new Sortable(this.rootContainer, options);
        this.computeIndexes();
      },
      beforeDestroy: function beforeDestroy() {
        this._sortable.destroy();
      },


      computed: {
        rootContainer: function rootContainer() {
          return this.transitionMode ? this.$el.children[0] : this.$el;
        },
        isCloning: function isCloning() {
          return !!this.options && !!this.options.group && this.options.group.pull === 'clone';
        },
        realList: function realList() {
          return !!this.list ? this.list : this.value;
        }
      },

      watch: {
        options: {
          handler: function handler(newOptionValue) {
            for (var property in newOptionValue) {
              if (readonlyProperties.indexOf(property) == -1) {
                this._sortable.option(property, newOptionValue[property]);
              }
            }
          },

          deep: true
        },

        realList: function realList() {
          this.computeIndexes();
        }
      },

      methods: {
        getChildrenNodes: function getChildrenNodes() {
          if (!this.init) {
            this.noneFunctionalComponentMode = this.noneFunctionalComponentMode && this.$children.length == 1;
            this.init = true;
          }

          if (this.noneFunctionalComponentMode) {
            return this.$children[0].$slots.default;
          }
          var rawNodes = this.$slots.default;
          return this.transitionMode ? rawNodes[0].child.$slots.default : rawNodes;
        },
        computeIndexes: function computeIndexes() {
          var _this4 = this;

          this.$nextTick(function () {
            _this4.visibleIndexes = _computeIndexes(_this4.getChildrenNodes(), _this4.rootContainer.children, _this4.transitionMode);
          });
        },
        getUnderlyingVm: function getUnderlyingVm(htmlElt) {
          var index = computeVmIndex(this.getChildrenNodes() || [], htmlElt);
          if (index === -1) {
            //Edge case during move callback: related element might be
            //an element different from collection
            return null;
          }
          var element = this.realList[index];
          return { index: index, element: element };
        },
        getUnderlyingPotencialDraggableComponent: function getUnderlyingPotencialDraggableComponent(_ref) {
          var __vue__ = _ref.__vue__;

          if (!__vue__ || !__vue__.$options || __vue__.$options._componentTag !== "transition-group") {
            return __vue__;
          }
          return __vue__.$parent;
        },
        emitChanges: function emitChanges(evt) {
          var _this5 = this;

          this.$nextTick(function () {
            _this5.$emit('change', evt);
          });
        },
        alterList: function alterList(onList) {
          if (!!this.list) {
            onList(this.list);
          } else {
            var newList = [].concat(_toConsumableArray(this.value));
            onList(newList);
            this.$emit('input', newList);
          }
        },
        spliceList: function spliceList() {
          var _arguments = arguments;

          var spliceList = function spliceList(list) {
            return list.splice.apply(list, _arguments);
          };
          this.alterList(spliceList);
        },
        updatePosition: function updatePosition(oldIndex, newIndex) {
          var updatePosition = function updatePosition(list) {
            return list.splice(newIndex, 0, list.splice(oldIndex, 1)[0]);
          };
          this.alterList(updatePosition);
        },
        getRelatedContextFromMoveEvent: function getRelatedContextFromMoveEvent(_ref2) {
          var to = _ref2.to,
              related = _ref2.related;

          var component = this.getUnderlyingPotencialDraggableComponent(to);
          if (!component) {
            return { component: component };
          }
          var list = component.realList;
          var context = { list: list, component: component };
          if (to !== related && list && component.getUnderlyingVm) {
            var destination = component.getUnderlyingVm(related);
            if (destination) {
              return _extends(destination, context);
            }
          }

          return context;
        },
        getVmIndex: function getVmIndex(domIndex) {
          var indexes = this.visibleIndexes;
          var numberIndexes = indexes.length;
          return domIndex > numberIndexes - 1 ? numberIndexes : indexes[domIndex];
        },
        getComponent: function getComponent() {
          return this.$slots.default[0].componentInstance;
        },
        resetTransitionData: function resetTransitionData(index) {
          if (!this.noTransitionOnDrag || !this.transitionMode) {
            return;
          }
          var nodes = this.getChildrenNodes();
          nodes[index].data = null;
          var transitionContainer = this.getComponent();
          transitionContainer.children = [];
          transitionContainer.kept = undefined;
        },
        onDragStart: function onDragStart(evt) {
          this.context = this.getUnderlyingVm(evt.item);
          evt.item._underlying_vm_ = this.clone(this.context.element);
          draggingElement = evt.item;
        },
        onDragAdd: function onDragAdd(evt) {
          var element = evt.item._underlying_vm_;
          if (element === undefined) {
            return;
          }
          removeNode(evt.item);
          var newIndex = this.getVmIndex(evt.newIndex);
          this.spliceList(newIndex, 0, element);
          this.computeIndexes();
          var added = { element: element, newIndex: newIndex };
          this.emitChanges({ added: added });
        },
        onDragRemove: function onDragRemove(evt) {
          insertNodeAt(this.rootContainer, evt.item, evt.oldIndex);
          if (this.isCloning) {
            removeNode(evt.clone);
            return;
          }
          var oldIndex = this.context.index;
          this.spliceList(oldIndex, 1);
          var removed = { element: this.context.element, oldIndex: oldIndex };
          this.resetTransitionData(oldIndex);
          this.emitChanges({ removed: removed });
        },
        onDragUpdate: function onDragUpdate(evt) {
          removeNode(evt.item);
          insertNodeAt(evt.from, evt.item, evt.oldIndex);
          var oldIndex = this.context.index;
          var newIndex = this.getVmIndex(evt.newIndex);
          this.updatePosition(oldIndex, newIndex);
          var moved = { element: this.context.element, oldIndex: oldIndex, newIndex: newIndex };
          this.emitChanges({ moved: moved });
        },
        computeFutureIndex: function computeFutureIndex(relatedContext, evt) {
          if (!relatedContext.element) {
            return 0;
          }
          var domChildren = [].concat(_toConsumableArray(evt.to.children)).filter(function (el) {
            return el.style['display'] !== 'none';
          });
          var currentDOMIndex = domChildren.indexOf(evt.related);
          var currentIndex = relatedContext.component.getVmIndex(currentDOMIndex);
          var draggedInList = domChildren.indexOf(draggingElement) != -1;
          return draggedInList || !evt.willInsertAfter ? currentIndex : currentIndex + 1;
        },
        onDragMove: function onDragMove(evt, originalEvent) {
          var onMove = this.move;
          if (!onMove || !this.realList) {
            return true;
          }

          var relatedContext = this.getRelatedContextFromMoveEvent(evt);
          var draggedContext = this.context;
          var futureIndex = this.computeFutureIndex(relatedContext, evt);
          _extends(draggedContext, { futureIndex: futureIndex });
          _extends(evt, { relatedContext: relatedContext, draggedContext: draggedContext });
          return onMove(evt, originalEvent);
        },
        onDragEnd: function onDragEnd(evt) {
          this.computeIndexes();
          draggingElement = null;
        }
      }
    };
    return draggableComponent;
  }

  if (typeof exports == "object") {
    var Sortable = require("sortablejs");
    module.exports = buildDraggable(Sortable);
  } else if (typeof define == "function" && define.amd) {
    define(['sortablejs'], function (Sortable) {
      return buildDraggable(Sortable);
    });
  } else if (window && window.Vue && window.Sortable) {
    var draggable = buildDraggable(window.Sortable);
    Vue.component('draggable', draggable);
  }
})();
},{"sortablejs":81}],84:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

exports.pushParams = pushParams;
exports.popParams = popParams;
exports.withParams = withParams;

var stack = [];

// exported for tests
var target = exports.target = null;
var _setTarget = exports._setTarget = function _setTarget(x) {
  exports.target = target = x;
};

function pushParams() {
  if (target !== null) {
    stack.push(target);
  }
  exports.target = target = {};
}

function popParams() {
  var lastTarget = target;
  var newTarget = exports.target = target = stack.pop() || null;
  if (newTarget) {
    if (!Array.isArray(newTarget.$sub)) {
      newTarget.$sub = [];
    }
    newTarget.$sub.push(lastTarget);
  }
  return lastTarget;
}

function addParams(params) {
  if (typeof params === 'object' && !Array.isArray(params)) {
    exports.target = target = _extends({}, target, params);
  } else {
    throw new Error('params must be an object');
  }
}

function withParamsDirect(params, validator) {
  return withParamsClosure(function (add) {
    return function () {
      add(params);

      for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }

      return validator.apply(this, args);
    };
  });
}

function withParamsClosure(closure) {
  var validator = closure(addParams);
  return function () {
    pushParams();
    try {
      for (var _len2 = arguments.length, args = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
        args[_key2] = arguments[_key2];
      }

      return validator.apply(this, args);
    } finally {
      popParams();
    }
  };
}

function withParams(paramsOrClosure, maybeValidator) {
  if (typeof paramsOrClosure === 'object' && maybeValidator !== undefined) {
    return withParamsDirect(paramsOrClosure, maybeValidator);
  }
  return withParamsClosure(paramsOrClosure);
}
},{}],85:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _common = require('./common');

exports.default = (0, _common.regex)('alpha', /^[a-zA-Z]*$/);
},{"./common":89}],86:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _common = require('./common');

exports.default = (0, _common.regex)('alphaNum', /^[a-zA-Z0-9]*$/);
},{"./common":89}],87:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _common = require('./common');

exports.default = function () {
  for (var _len = arguments.length, validators = Array(_len), _key = 0; _key < _len; _key++) {
    validators[_key] = arguments[_key];
  }

  return (0, _common.withParams)({ type: 'and' }, function () {
    var _this = this;

    for (var _len2 = arguments.length, args = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
      args[_key2] = arguments[_key2];
    }

    return validators.length > 0 && validators.reduce(function (valid, fn) {
      return valid && fn.apply(_this, args);
    }, true);
  });
};
},{"./common":89}],88:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _common = require('./common');

exports.default = function (min, max) {
  return (0, _common.withParams)({ type: 'between', min: min, max: max }, function (value) {
    return !(0, _common.req)(value) || (!/\s/.test(value) || value instanceof Date) && +min <= +value && +max >= +value;
  });
};
},{"./common":89}],89:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.regex = exports.ref = exports.len = exports.req = exports.withParams = undefined;

var _withParams = require('../withParams');

var _withParams2 = _interopRequireDefault(_withParams);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.withParams = _withParams2.default;

// "required" core, used in almost every validator to allow empty values

var req = exports.req = function req(value) {
  if (Array.isArray(value)) return !!value.length;
  if (value === undefined || value === null || value === false) {
    return false;
  }

  if (value instanceof Date) {
    // invalid date won't pass
    return !isNaN(value.getTime());
  }

  if (typeof value === 'object') {
    for (var _ in value) {
      return true;
    }return false;
  }

  return !!String(value).length;
};

// get length in type-agnostic way
var len = exports.len = function len(value) {
  if (Array.isArray(value)) return value.length;
  if (typeof value === 'object') {
    return Object.keys(value).length;
  }
  return String(value).length;
};

// resolve referenced value
var ref = exports.ref = function ref(reference, vm, parentVm) {
  return typeof reference === 'function' ? reference.call(vm, parentVm) : parentVm[reference];
};

// regex based validator template
var regex = exports.regex = function regex(type, expr) {
  return (0, _withParams2.default)({ type: type }, function (value) {
    return !req(value) || expr.test(value);
  });
};
},{"../withParams":105}],90:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _common = require('./common');

var emailRegex = /(^$|^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$)/;

exports.default = (0, _common.regex)('email', emailRegex);
},{"./common":89}],91:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.maxValue = exports.minValue = exports.and = exports.or = exports.url = exports.sameAs = exports.requiredUnless = exports.requiredIf = exports.required = exports.minLength = exports.maxLength = exports.macAddress = exports.ipAddress = exports.email = exports.between = exports.numeric = exports.alphaNum = exports.alpha = undefined;

var _alpha = require('./alpha');

var _alpha2 = _interopRequireDefault(_alpha);

var _alphaNum = require('./alphaNum');

var _alphaNum2 = _interopRequireDefault(_alphaNum);

var _numeric = require('./numeric');

var _numeric2 = _interopRequireDefault(_numeric);

var _between = require('./between');

var _between2 = _interopRequireDefault(_between);

var _email = require('./email');

var _email2 = _interopRequireDefault(_email);

var _ipAddress = require('./ipAddress');

var _ipAddress2 = _interopRequireDefault(_ipAddress);

var _macAddress = require('./macAddress');

var _macAddress2 = _interopRequireDefault(_macAddress);

var _maxLength = require('./maxLength');

var _maxLength2 = _interopRequireDefault(_maxLength);

var _minLength = require('./minLength');

var _minLength2 = _interopRequireDefault(_minLength);

var _required = require('./required');

var _required2 = _interopRequireDefault(_required);

var _requiredIf = require('./requiredIf');

var _requiredIf2 = _interopRequireDefault(_requiredIf);

var _requiredUnless = require('./requiredUnless');

var _requiredUnless2 = _interopRequireDefault(_requiredUnless);

var _sameAs = require('./sameAs');

var _sameAs2 = _interopRequireDefault(_sameAs);

var _url = require('./url');

var _url2 = _interopRequireDefault(_url);

var _or = require('./or');

var _or2 = _interopRequireDefault(_or);

var _and = require('./and');

var _and2 = _interopRequireDefault(_and);

var _minValue = require('./minValue');

var _minValue2 = _interopRequireDefault(_minValue);

var _maxValue = require('./maxValue');

var _maxValue2 = _interopRequireDefault(_maxValue);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.alpha = _alpha2.default;
exports.alphaNum = _alphaNum2.default;
exports.numeric = _numeric2.default;
exports.between = _between2.default;
exports.email = _email2.default;
exports.ipAddress = _ipAddress2.default;
exports.macAddress = _macAddress2.default;
exports.maxLength = _maxLength2.default;
exports.minLength = _minLength2.default;
exports.required = _required2.default;
exports.requiredIf = _requiredIf2.default;
exports.requiredUnless = _requiredUnless2.default;
exports.sameAs = _sameAs2.default;
exports.url = _url2.default;
exports.or = _or2.default;
exports.and = _and2.default;
exports.minValue = _minValue2.default;
exports.maxValue = _maxValue2.default;
},{"./alpha":85,"./alphaNum":86,"./and":87,"./between":88,"./email":90,"./ipAddress":92,"./macAddress":93,"./maxLength":94,"./maxValue":95,"./minLength":96,"./minValue":97,"./numeric":98,"./or":99,"./required":100,"./requiredIf":101,"./requiredUnless":102,"./sameAs":103,"./url":104}],92:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _common = require('./common');

exports.default = (0, _common.withParams)({ type: 'ipAddress' }, function (value) {
  if (!(0, _common.req)(value)) {
    return true;
  }

  if (typeof value !== 'string') {
    return false;
  }

  var nibbles = value.split('.');
  return nibbles.length === 4 && nibbles.every(nibbleValid);
});


var nibbleValid = function nibbleValid(nibble) {
  if (nibble.length > 3 || nibble.length === 0) {
    return false;
  }

  if (nibble[0] === '0' && nibble !== '0') {
    return false;
  }

  if (!nibble.match(/^\d+$/)) {
    return false;
  }

  var numeric = +nibble | 0;
  return numeric >= 0 && numeric <= 255;
};
},{"./common":89}],93:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _common = require('./common');

exports.default = function () {
  var separator = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : ':';
  return (0, _common.withParams)({ type: 'macAddress' }, function (value) {
    if (!(0, _common.req)(value)) {
      return true;
    }

    if (typeof value !== 'string') {
      return false;
    }

    var parts = typeof separator === 'string' && separator !== '' ? value.split(separator) : value.length === 12 || value.length === 16 ? value.match(/.{2}/g) : null;

    return parts !== null && (parts.length === 6 || parts.length === 8) && parts.every(hexValid);
  });
};

var hexValid = function hexValid(hex) {
  return hex.toLowerCase().match(/^[0-9a-f]{2}$/);
};
},{"./common":89}],94:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _common = require('./common');

exports.default = function (length) {
  return (0, _common.withParams)({ type: 'maxLength', max: length }, function (value) {
    return !(0, _common.req)(value) || (0, _common.len)(value) <= length;
  });
};
},{"./common":89}],95:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _common = require('./common');

exports.default = function (max) {
  return (0, _common.withParams)({ type: 'maxValue', max: max }, function (value) {
    return !(0, _common.req)(value) || (!/\s/.test(value) || value instanceof Date) && +value <= +max;
  });
};
},{"./common":89}],96:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _common = require('./common');

exports.default = function (length) {
  return (0, _common.withParams)({ type: 'minLength', min: length }, function (value) {
    return !(0, _common.req)(value) || (0, _common.len)(value) >= length;
  });
};
},{"./common":89}],97:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _common = require('./common');

exports.default = function (min) {
  return (0, _common.withParams)({ type: 'minValue', min: min }, function (value) {
    return !(0, _common.req)(value) || (!/\s/.test(value) || value instanceof Date) && +value >= +min;
  });
};
},{"./common":89}],98:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _common = require('./common');

exports.default = (0, _common.regex)('numeric', /^[0-9]*$/);
},{"./common":89}],99:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _common = require('./common');

exports.default = function () {
  for (var _len = arguments.length, validators = Array(_len), _key = 0; _key < _len; _key++) {
    validators[_key] = arguments[_key];
  }

  return (0, _common.withParams)({ type: 'or' }, function () {
    var _this = this;

    for (var _len2 = arguments.length, args = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
      args[_key2] = arguments[_key2];
    }

    return validators.length > 0 && validators.reduce(function (valid, fn) {
      return valid || fn.apply(_this, args);
    }, false);
  });
};
},{"./common":89}],100:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _common = require('./common');

exports.default = (0, _common.withParams)({ type: 'required' }, _common.req);
},{"./common":89}],101:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _common = require('./common');

exports.default = function (prop) {
  return (0, _common.withParams)({ type: 'requiredIf', prop: prop }, function (value, parentVm) {
    return (0, _common.ref)(prop, this, parentVm) ? (0, _common.req)(value) : true;
  });
};
},{"./common":89}],102:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _common = require('./common');

exports.default = function (prop) {
  return (0, _common.withParams)({ type: 'requiredUnless', prop: prop }, function (value, parentVm) {
    return !(0, _common.ref)(prop, this, parentVm) ? (0, _common.req)(value) : true;
  });
};
},{"./common":89}],103:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _common = require('./common');

exports.default = function (equalTo) {
  return (0, _common.withParams)({ type: 'sameAs', eq: equalTo }, function (value, parentVm) {
    return value === (0, _common.ref)(equalTo, this, parentVm);
  });
};
},{"./common":89}],104:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _common = require('./common');

var urlRegex = /^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:[/?#]\S*)?$/i;

exports.default = (0, _common.regex)('url', urlRegex);
},{"./common":89}],105:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
/* istanbul ignore next */
var withParams = 'lib' === 'web' ? require('./withParamsBrowser').withParams : require('./params').withParams;

exports.default = withParams;
},{"./params":84,"./withParamsBrowser":106}],106:[function(require,module,exports){
(function (global){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
// In browser, validators should be independent from Vuelidate.
// The only usecase those do need to be dependent is when you need $params.
// To make the dependency optional, try to grab Vuelidate from global object,
// fallback to stubbed WithParams on failure.

var root = typeof global !== 'undefined' ? global : typeof window !== 'undefined' ? window : {};

/* istanbul ignore next */
var fakeWithParams = function fakeWithParams(paramsOrClosure, maybeValidator) {
  if (typeof paramsOrClosure === 'object' && maybeValidator !== undefined) {
    return maybeValidator;
  }
  return paramsOrClosure(function () {});
};

var withParams = exports.withParams = root.vuelidate ? root.vuelidate.withParams : fakeWithParams;
}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}]},{},[1]);
