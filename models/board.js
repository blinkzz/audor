const path = require("path");
const fs = require("fs-extra");

let boardSchema = new mongoose.Schema({
    __v: {type: Number, select: false},
    name: String,
    background: String,
    prefs: {
        showProgressBarsOnCards: {type: Boolean, default: false},
        showLabelsOnCards: {type: Boolean, default: true},
        showImagesOnCards: {type: Boolean, default: true}
    },
    users: [{
        user: {type: ObjectId, ref: "User"},
        role: {type: ObjectId, ref: "Role"}
    }]
}, {
    usePushEach: true
});

boardSchema.methods.uploadBackground = async function (background) {
    if (this.background) [err, data] = await to(fs.unlink(__root + this.background));
    [err, data] = await to(fs.mkdirp(__root + "/content/backgrounds/"));

    let backgroundPath = `/content/backgrounds/${path.basename(background.path)}`;
    [err, data] = await to(fs.rename(background.path, __root + backgroundPath));

    this.background = backgroundPath;
};

boardSchema.methods.deleteBackground = async function () {
    [err, data] = await to(fs.unlink(__root + this.background));
    this.background = undefined;
};

let Board = mongoose.model('Board', boardSchema);

module.exports = Board;
