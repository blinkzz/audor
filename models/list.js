let listSchema = new mongoose.Schema({
    __v: {type: Number, select: false},
    name: String,
    owner: {type: ObjectId, ref: "User"},
    board: {type: ObjectId, ref: "Board"},
    order: Number
}, {
    toObject: {virtuals: true},
    toJSON: {virtuals: true}
});

listSchema.virtual("cards", {
    ref: "Card",
    localField: "_id",
    foreignField: "list"
});

let autoPopulate = function (next) {
    this.populate("cards.owner");
    next();
};

listSchema.pre('find', autoPopulate).pre('findOne', autoPopulate);

let List = mongoose.model("List", listSchema);

module.exports = List;
