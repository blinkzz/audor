let roleSchema = new mongoose.Schema({
    __v: {type: Number, select: false},
    name: {type: String, unique: true},
    custom: {type: Boolean, default: false},
    permissions: {
        card: {
            create: {type: Boolean, default: false},
            remove: {type: Boolean, default: false},
            update: {type: Boolean, default: false},
        },
        board: {
            remove: {type: Boolean, default: false},
            update: {type: Boolean, default: false},
        },
        list: {
            create: {type: Boolean, default: false},
            update: {type: Boolean, default: false},
            remove: {type: Boolean, default: false}
        }
    }
});

let Role = mongoose.model("Role", roleSchema);

let defaultRoles = [
    {
        name: "owner",
        permissions: {
            card: {
                create: true,
                remove: true,
                update: true
            },
            board: {
                remove: true,
                update: true,
            },
            list: {
                create: true,
                update: true,
                remove: true
            }
        }
    }, {
        name: "admin",
        permissions: {
            card: {
                create: true,
                remove: true,
                update: true
            },
            board: {
                update: true,
            },
            list: {
                create: true,
                update: true,
                remove: true
            }
        }
    }, {
        name: "user",
        permissions: {
            card: {
                create: true,
                remove: true,
                update: true
            },
            list: {
                create: true,
                update: true,
                remove: true
            }
        }
    }, {
        name: "guest",
    }
];

defaultRoles.forEach(async (defaultRole) => {
    let [err, data] = await to(Role.create(defaultRole));
});

module.exports = Role;