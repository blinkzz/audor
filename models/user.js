const crypto = require("crypto");
const path = require("path");
const fs = require("fs-extra");

let userSchema = new mongoose.Schema({
    __v: {type: Number, select: false},
    login: {type: String, unique: true},
    name: String,
    about: String,
    website: String,
    email: String,
    photo: String,
    password: String,
    admin: Boolean,
    prefs: {
        theme: {type: String, default: "light"}
    }
});

userSchema.methods.uploadPhoto = async function (photo) {
    if (!photo.size) return;
    if (this.photo) [err, data] = await to(fs.unlink(__root + this.photo));
    [err, data] = await to(fs.mkdirp(__root + "/content/users/" + this._id));

    let photoPath = `/content/users/${this._id}/${path.basename(photo.path)}`;
    [err, data] = await to(fs.rename(photo.path, __root + photoPath));

    this.photo = photoPath;
};

userSchema.methods.toJSON = function () {
    let obj = this.toObject();
    delete obj.password;
    return obj
};

userSchema.statics.getPasswordHash = function (password) {
    password = crypto.createHash("md5").update(password).digest("hex");
    password = crypto.createHash("sha1").update(password).digest("hex");
    return password;
};

let User = mongoose.model('User', userSchema);

module.exports = User;
