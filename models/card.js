const fs = require("fs-extra");

let cardSchema = new mongoose.Schema({
    __v: {type: Number, select: false},
    name: String,
    description: String,
    owner: {type: ObjectId, ref: "User"},
    board: {type: ObjectId, ref: "Board"},
    list: {type: ObjectId, ref: "List"},
    labels: [String],
    image: String,
    progressBars: [{}],
    order: Number
}, {
    toObject: {virtuals: true},
    toJSON: {virtuals: true}
});

cardSchema.methods.uploadImage = async function (image) {
    if (this.image) [err, data] = await to(fs.unlink(__dirname + "/.." + this.image));
    [err, data] = await to(fs.mkdirp(__root + "/content/cards/"));

    let imagePath = `/content/cards/${image.filename}`;
    [err, data] = await to(fs.rename(__dirname + "/../" + image.path, __dirname + "/../" + imagePath));
    this.image = imagePath;
};

cardSchema.methods.removeImage = async function () {
    if (this.image) [err, data] = await to(fs.unlink(__root + this.image));
    this.image = undefined;
    this.save();
};

cardSchema.virtual("createdAt").get(function () {
    return this._id.getTimestamp();
});

let autoPopulate = function (next) {
    this.populate("owner");
    next();
};

cardSchema.pre('find', autoPopulate).pre('findOne', autoPopulate);

let Card = mongoose.model("Card", cardSchema);

module.exports = Card;
